#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# ------------------------- Modules To Import
import sys
import os
from optparse import OptionParser

VERSION    = "3.0"

class Options:
    """
    A basic/generic class to manage command line parsing in a unified way.
    """

    def __init__(self, args = ["Options"], options = None, version=None, verbose = False):
        """
        @param args    : a list of arguments, organized as command line
        @param options : a dictionary of the options ("key": value)
                         these options (if any) will overwrite args parsing
        @param version : the version of the program to pass to optParser
        """
        if not hasattr(self, "clp"):
            # print "New optParser instance"
            self.clp     = self.optParser(version = version)
        self.__cmdLine_options() # command line parser instance
        self.args    = args
        self.input_options = options
        return 

    def __repr__(self):
        """
        @return: a string describing the options setup for the Blast instance
        """
        try:
            rs = self.version
            for option in self.options.__dict__.keys():
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""

    def optParser(self, version = None):
        """
        Create an optionParse instance to fill with options later.
        @return: command line options and arguments (optionParser object)
        """
        if not version:
            version = "%prog"+ " v%s"%VERSION
        self.version = version
        cmd = OptionParser(usage="usage: %prog [options]", version = version, conflict_handler="resolve")
        return cmd

    def __cmdLine_options(self):
        """
        Command line definition for some universal options.
        """

        # Some varys basic/univsersal options
        # self.clp.add_option("--sge", action="store_true",      dest="sge",      
        #                help="launch commands using SGE",                                          default = False)

        # self.clp.add_option("--sgemap", action="store_true",      dest="sgemap",      
        #                help="generate commands using SGE_TASK_ID",                                          default = False)

        self.clp.add_option("-v", action="store_true", dest="verbose",  
                       help="verbose mode",                                                       default = False)

        self.clp.add_option("--debug", action="store_true", dest="debug",    
                            help="debugging mode",                                                     default = False)

    def add_options(self, options = None):
        if not options:
            return
        if self.input_options:
            for option in options.keys():
                self.input_options[option] = options[option]
        else:
            self.input_options = options
        
    def parse(self):
        """
        effecetive command line arguments parsing 
        plus additional speccific options installation
        from self.input_options
        """

        (self.options, self.unused_args) = self.clp.parse_args(self.args)
        if self.input_options != None:
            for option in self.input_options.keys():
                setattr(self.options, option, self.input_options[option])


def main( args ):
    """
    Launch the main purpose:
    - Parse command line
    - Launch predictions
    """
    
    # Parse arguments
    job = Options(args = args)
    job.parse()
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)

    # process
    # ret = process( argsDict )

    return 


# GO !
if __name__ == "__main__":
    main(sys.argv)
# -------------------------
