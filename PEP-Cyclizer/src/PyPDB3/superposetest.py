#!/usr/bin/env python
"""
        def superimpose(self, pdb, atmList = ["N", "CA", "C", "O"], verbose = 0):
                if len(self) != len(pdb):
                        return
"""
import sys, os, fnmatch
import PyPDB.PyPDB as PDB
import PyBestFit.BestFit as BestFit

def fragLibLoad(path):
    """
    frg_lib = fragLibLoad("/home/tuffery/Work/prgs/Src/S4-dev/lib/27-2/cutoff1.9-freq0.025-ward")
    """
    pdbs = []
    files = fnmatch.filter(os.listdir(path), "*fittmpl.proto*")
    print files
    for file in files:
        x = PDB.PDB("%s/%s" % (path, file))
        if x:
            pdbs.append(x)
    return pdbs


def single_gap_fill(pdb, pos, left_align, frg_lib):
    """
    PDB: the PDB to fill
    pos: the position at which the fragment is glued (fragment first position, so the position to fill is pos+1 )
    left_align: if True, then we use 2 residues Nter side, 1 Cter side.
                if False, then we use 1 residue Nter side, 2 Cter side.
    frg_lib: the fragment library (list of PDBs).
    x = PDB.PDB("align-3DPT_A-query-tito.pdb")
    x_gaps =  gap_scan(x)
    frg_lib = fragLibLoad("/home/tuffery/Work/prgs/Src/S4-dev/lib/27-2/cutoff1.9-freq0.025-ward")
    glue_pdb, rmsd = single_gap_fill(x, x_gaps[0][0], False, frg_lib)
    glue_pdb.out('toto.pdb')
    """
    best_rmsd = 1000000.
    if left_align:
        pdb_seed = pdb[pos-1:pos+2]
    else:
        pdb_seed = pdb[pos:pos+3]
    for frg in frg_lib:
        if left_align:
            frg_seed = frg[0:2]+frg[3:len(frg)]
        else:
            frg_seed = frg[0:1]+frg[2:len(frg)]
        lrs, rmsd = superimpose(pdb_seed, frg_seed, frg)
        print rmsd, best_rmsd, rmsd < best_rmsd
        if float(rmsd) < float(best_rmsd):
            best_rmsd = float(rmsd)
            print "new best rmsd: ", best_rmsd
            rs = lrs
    if left_align:
        glue = rs[1:4]
    else:
        glue = rs[0:3]
    glue.renumber(int(pdb[pos].rNum()))
    pdbrs = pdb[0:pos] + glue + pdb[pos+2:len(pdb)]    
    return pdbrs, best_rmsd

def single_gaps_fill_all(pdb, frg_lib_path = "/home/tuffery/Work/prgs/Src/S4-dev/lib/27-2/cutoff1.9-freq0.025-ward"):
    """
    pdb: the PDB to fill
    frg_lib_path: the path to the fragment library (list of PDBs).
    glue_pdb = single_gaps_fill_all(x)
    glue_pdb.out('toto.pdb')
    """
    pdb_gaps =  gap_scan(pdb)
    frg_lib = fragLibLoad(frg_lib_path)
    gaps= list(reversed(pdb_gaps))
    glue_pdb = pdb
    for i, g in enumerate(gaps):
        glue_pdb, rmsd = single_gap_fill(glue_pdb, g[0], False, frg_lib)
        glue_pdb.out('toto-%d.pdb' % i)
    return glue_pdb
    
def gap_scan(PDB, ref_seq = None):
    """
    This assumes residues have been renumbered from 1 according to the ref_seq (1st amino acid of ref_seq has number 1)
    gaps occur when number differ by more than 1 between two consecutive positions.
    x = PDB.PDB("align-3DPT_A-query-tito.pdb")
    x_gaps =  gap_scan(x)
    """
    rs = []
    for pos in range(0,len(PDB)-1):
        if int(PDB[pos+1].rNum()) == int(PDB[pos].rNum()) + 2:
            print PDB[pos].rNum(), PDB[pos+1].rNum(), 
            rs.append([pos, int(PDB[pos+1].rNum())])
    return rs


def superimpose(x, y, z, aWhat = ["N", "CA", "C", "O"], verbose = 0):
    """
    x is reference, y is moving
    z undergoes transformation
    """
    # print "=================================="
    # print x
    # print "================="
    # print y
    
    if len(x) != len(y):
        sys.stderr.write("superimpose: Sorry, incompatible sizes %d vs %d\n" % (len(x), len(y)) )
        print "=================================="
        print x
        print "================="
        print y
        return None, 1000000.
    rl1 = []
    rl2 = []
    # This way, we ensure that we have the same number of atoms
    for res1, res2 in zip(x, y):
        al1 = []
        al2 = []
        for atm in ["N","CA","C","O"]:
            a1 = res1.findAtm(atm)
            a2 = res2.findAtm(atm)
            if a1 and a2:
                al1.append(a1)
                al2.append(a2)
        
        r1 = PDB.atmList(al1)
        r2 = PDB.atmList(al2)
        rl1 += r1
        rl2 += r2
    
    p1 = PDB.PDB(rl1)
    p2 = PDB.PDB(rl2)
    
    bf = BestFit.bestFit()
    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(p1,p2, outFit = False)
    rs = bf.PDBTM(z, TM) # p2 moves
    return rs, bfrmsd

if __name__ == "__main__":

    x = PDB.PDB(sys.argv[1], hetSkip = 1)
    y = PDB.PDB(sys.argv[2], hetSkip = 1)

    yfit, rmsd = superimpose(x, y)
    yfit.out("toto-2019.pdb")

    sys.exit(0)
    
    rl1 = []
    rl2 = []
    for res1, res2 in zip(x, y):
        al1 = []
        al2 = []
        print res1
        print "=============="
        print res2
        print "==================================="
        for atm in ["N","CA","C","O"]:
            a1 = res1.findAtm(atm)
            a2 = res2.findAtm(atm)
            if a1 and a2:
                al1.append(a1)
                al2.append(a2)

        r1 = PDB.atmList(al1)
        r2 = PDB.atmList(al2)
        print r1
        print "xxxxxxxxxxxxxx"
        print r2
        print "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        rl1 += r1
        rl2 += r2

    p1 = PDB.PDB(rl1)
    p2 = PDB.PDB(rl2)

    import BestFit
    bf = BestFit.bestFit()
    orirmsd, bfrmsd, TM, rs = bf.PDBBestFit(p1,p2, outFit = False)
    rs = bf.PDBTM(p2, TM) # p2 moves
