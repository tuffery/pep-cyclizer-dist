#!/usr/bin/env python

import sys
import os

if os.environ.has_key('PEPFOLDHOME'):
    PEPFOLDHOME = os.environ['PEPFOLDHOME']
else:
    PEPFOLDHOME = "/nfs/joule/tuffery/Padilla2010/PEP-FOLD"
if not PEPFOLDHOME:
    sys.exit('PEPFOLDHOME must be defined in your environment')
    
# Local
sys.path.append("%s/bin/" % PEPFOLDHOME)

import PostTreat

PostTreat.SPARTA(sys.argv[1], sys.argv[2], iRef = sys.argv[3], verbose = 0)


f = open(sys.argv[2])
lines = f.readlines()
f.close()

of = open("%s.Rdata" % sys.argv[2], "w")
isOpened = False
for l in lines:
    it = l.split()
    if not len(it):
        continue
    if it[0] == "FORMAT":
        isOpened = True
        continue
    if isOpened and len(it):
        of.write("%s" % l)
of.close()

