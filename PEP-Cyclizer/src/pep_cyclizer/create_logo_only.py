#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018

Adapted by P. Tuffery May 2019 to make it independent of PEP-Cylizer naming context, an dcompliant with python3

./create_logo_only.py --aa_constraint_fname /home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs/constraints_list.data  --input-sequences /home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs/all.fasta --logo_fname test.png

# gives a division by 0
export ROOT=/home/tuffery/Work/prgs/Src/GitLab/src/PEP-Cyclizer
export DATAROOT=$ROOT/test/otherConfs
export UII=$DATAROOT/uII-2
$ROOT/src/pep-cyclizer/create_logo_only.py --input-sequences $UII/pep_cyclizer_test/Gap3/BCSearch/uII-2_sequence_guess_gap3.fasta --logo_fname uII-2_sequence_guess_gap3.png --aa_constraint_fname $DATAROOT/constraints_list.data 


"""

###########################
###  L I B R A R I E S  ###
###########################

import sys, os
#import weblogolib as w  # --> works with cluster mode!
import weblogo as w    #--> works with PC mode!
import argparse
import math
from itertools import *
import numpy as np
import random
import aa_constraints

DFLT_WORK_PATH     = "./"
DFLT_MODE          = "machine"  #else "cluster"
PROT_DIR           = "pep_cyclizer_test"
DFLT_LBL           = "pep_cyclizer"
DFLT_LOGO_FNAME    = "pep_cyclizer.png"
DFLT_LOGO_FIRST_INDEX = 1
DFLT_LOGO_START    = None
DFLT_LOGO_END      = None
DFLT_SHOW_AXES     = False
DFLT_REVERSE_STACKS = False

# DFLT_MODEL_S_FNAME = "pep_cyclizer_candidate_sequences.txt"
AAseq = ["A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"]
AAs = "ACDEFGHIKLMNPQRSTVWY"

protein_alphabet = ('ACDEFGHIKLMNOPQRSTUVWYBJZX*-adefghiklmnopqrstuvwybjzx')

#########################################################################
#########################################################################
#########################################################################
## Plot the logo
def plot_logo(acc_seq,logo_file,aa_const, not_const, first_index = 1, logo_start = None, logo_end = None, show_axes = False, reverse_stacks = False):
    """
    acc_seq  : filename of aligned sequences (fasta)
    logo_file: filename of the output logo (png)
    aa_const : amino acid contraints (grouped over all positions)
    not_const: the other (un_constrained) amino acids
    """

    #cmd = "weblogo --format PNG --size medium --yaxis 1 --aspect-ratio 2 --stack-width 100 --show-xaxis NO --show-yaxis NO --color-scheme auto --sequence-type protein --reverse-stacks NO --units probability < %s > %s"%(acc_seq,logo_file)
    #os.system(cmd)  # weblogo must be installed ...
    
    color_scheme = w.ColorScheme()
    color_scheme.rules = [] 
    if len(aa_const) > 0:
        color_scheme.rules.append(w.SymbolColor(aa_const, "red", "constraint"))
        color_scheme.rules.append(w.SymbolColor(not_const, "light gray", "others"))
    else:
        color_scheme.rules.append(w.SymbolColor("GSTYC", "green", "polar"))
        color_scheme.rules.append(w.SymbolColor("NQ", "purple", "neutral"))
        color_scheme.rules.append(w.SymbolColor("KRH", "blue", "basic"))
        color_scheme.rules.append(w.SymbolColor("DE", "red", "acidic"))
        color_scheme.rules.append(w.SymbolColor("PAWFLIMV", "black", "hydrophobic"))
        color_scheme.rules.append(w.SymbolColor("-", "white", "gap"))
    
    fin = open(acc_seq)
    seqs = w.read_seq_data(fin, alphabet=protein_alphabet)
    data = w.LogoData.from_seqs(seqs)
    options = w.LogoOptions()
    options.show_yaxis = False 
    options.show_xaxis = False 
    options.show_yaxis = show_axes # True #
    options.show_xaxis = show_axes # True #
    options.yaxis_scale = 1
    options.color_scheme= color_scheme
    options.show_fineprint = False
    # options.aspect_ratio = 8
    # options.stack_width = 6
    options.reverse_stacks = False
    options.reverse_stacks = reverse_stacks # True #
    options.first_index = first_index # -7 #
    options.logo_start = logo_start # 1   #
    options.logo_end  = logo_end # 110  #
    options.unit_name = 'probability'
    mformat = w.LogoFormat(data, options)
    fout = open(logo_file, 'wb')
    A = w.png_print_formatter( data, mformat)
    fout.write(A + b"/n")
    return   


def read_sequences(seq_file):
    """
    Read fasta file. Return sequences as a list of strings.
    Sequences are expected to be aligned (constant number of aas)
    """
    rs = []
    f = open(seq_file)
    lines = f.readlines()
    f.close()
    aln_sze = None
    for l in lines:
        if l.startswith(">") : continue
        s = l.split()[0]
        if not aln_sze:
            aln_sze = len(s)
        else:
            if len(s) != aln_sze:
                sys.stderr.write( "Sorry: detected sequences of different sizes (%d/%d) for %s\n" % (aln_sze, len(s), s) )
        rs.append(s)
    return aln_sze, rs

    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""

# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    #    parser.add_argument("--seq_sze", dest="seq_sze", type = int, help="The size of the aligned sequences",
    #                       action="store", default=None)  

    parser.add_argument("--input-sequences", dest="input_sequences", help="The input sequences (FASTA format)",
                        action="store", default=None)  

    parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
                        action="store", default=None) 
    
    parser.add_argument("--true_seq", dest="true_seq", help="the exact target sequence",
                        action="store", default=None)

    parser.add_argument("--logo_fname", dest="logo_fname", help="the name of the logo file generated.",
                        action="store", default=DFLT_LOGO_FNAME)

    parser.add_argument("--logo_first_index", dest="logo_first_index", help="the first index of the logo.",
                        type=int, action="store", default=DFLT_LOGO_FIRST_INDEX)

    parser.add_argument("--logo_start", dest="logo_start", help="the first position of the logo.",
                        type=int, action="store", default=DFLT_LOGO_START)

    parser.add_argument("--logo_end", dest="logo_end", help="the last position of the logo.",
                        type=int, action="store", default=DFLT_LOGO_END)

    parser.add_argument("--show_axes", dest="show_axes", help="show x,y axes.",
                        action="store_true", default=DFLT_SHOW_AXES)

    parser.add_argument("--reverse_stacks", dest="reverse_stacks", help="Reverse stacks (gaps are up).",
                        action="store_true", default=DFLT_REVERSE_STACKS)

    # parser.add_argument("--model_s_fname", dest="model_s_fname", help="the name of the file with modelled sequences inferred from matching sequences.",
    #                     action="store", default=DFLT_MODEL_S_FNAME)

    return parser

def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")
    #print((print_options(options)))
    return options

# def run(input_sequences, true_seq, aa_constraint_fname, logo_fname, candidate_sequences_fname):
def run(input_sequences, true_seq, aa_constraint_fname, logo_fname, first_index = 1, logo_start = None, logo_end = None, show_axes = False, reverse_stacks = False):
    """
    input_sequences   : the aligned sequences to generate the logo from.
    true_seq          : case where experimental linker sequence is known (one string)
    aa_constraint_fname: file specifying constraints per positions.
    logo_fname        : the name of the logo file (was "label.png") and "label_candidate_sequences.txt"
    candidate_sequences_fname : the name of the sequences inferred from data (was "label_candidate_sequences.txt")
    first_index       : the index of the first amino acid of the sequence
    logo_start        : the index to start logo
    logo_end          : the index to stop logo
    return: none
    """
    
    aa_cons, not_cons = aa_constraints.summed_aa_constraints(true_seq, aa_constraint_fname)
    
    #if os.path.exists(acc_seq_fname):
    print("Logo will be built from %s" % input_sequences)
    print("aa_cons: %s" % aa_cons)
    print("aa_others: ", not_cons)
    print("Logo file written as: %s" % logo_fname)
    
    plot_logo(input_sequences, logo_fname, aa_cons, not_cons, first_index, logo_start, logo_end, show_axes, reverse_stacks )
       

if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    if not options.input_sequences:
        sys.stderr.write("Sorry: create_logo_core.py needs a valid filename of aligned sequences (FASTA) to generate logo\n")
        sys.exit(0)

    # run(options.input_sequences, options.true_seq, options.aa_constraint_fname, options.logo_fname, options.model_s_fname)
    run(options.input_sequences, options.true_seq, options.aa_constraint_fname, options.logo_fname, options.logo_first_index, options.logo_start, options.logo_end, options.show_axes, options.reverse_stacks)

