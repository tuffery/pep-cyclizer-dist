#include "common.h"
typedef struct {
  int pdbid; //PDB identifier of the database
  int segid; //segment identifier of the database
  Index offset; //offset within segment
  Index *ind; //alignment indices:
                       // ind to ind+alen for the first row (query)
                       // ind+alen to ind+2*alen for the second row (match)
  Index alen;  //alignment length
  Real bc;
  Real rmsd;
} BCAlignHit;

int BCAlign (
  const Coord *atoms,
  Index nr_atoms, //query atoms
  Index gap,
  Index al,
  Real minBC,
  Real minBC0,
  Real maxRMSD, //maximum RMSD, or -1 for no RMSD check
  const Coord *dbca, //CA database
  int seg_index[][3], //(dbca offset, segment resnr, segment length)
  int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
  BCAlignHit *hits, //must be pre-allocated with size maxhits
  int maxhits,
  Index *alstr, //string to hold the aligments, must be pre-allocated with size max_alstr
  int max_alstr, // must be at least 2*hit.alen summed over for all hits
  char *score_type,
  char *mode
);
