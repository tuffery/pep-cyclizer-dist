import sys, os
import numpy as np
from PyPDB import PyPDB as PDB
from itertools import groupby

Helix = ["H","G","I"]
Beta = ["B","E"]
SS = Helix + Beta
LL = [' ', 'T', 'S']

def read_dssp(temp_dir, dssp_name):
    dssp_file = open("%s/%s" %(temp_dir, dssp_name), "r").readlines()
    nbRes = len(dssp_file) - 28
    SSlog = np.zeros(nbRes,dtype="str")
    for i in range(0,nbRes):
        line = dssp_file[i+28].split("\n")[0]
        if line[16] in SS:
            SSlog[i] = 'S'
        else:
            SSlog[i] = 'L'
    os.system("rm %s/%s" %(temp_dir, dssp_name))
    return SSlog,nbRes

def find_loop(SSlog,nbRes):
    Loop = {}; i=0; nbLoop = -1; SSlen = []
    SSprofile = ["".join(grp) for num, grp in groupby(SSlog)]
    nbGrp = len(SSprofile)
    for ll in range(0,nbGrp):
        SSlen.append(len(SSprofile[ll]))

    while i<nbGrp:
        if SSprofile[i][0] == 'S' and SSlen[i] >= 4:
            BefLoop = i
            LoopLen = 0
            LoopFinish = 0
            j = i
            while LoopLen < 30 and LoopFinish == 0 and j<(nbGrp-1):
                j += 1
                if SSprofile[j][0] == 'L' :
                    LoopLen += SSlen[j]
                elif SSprofile[j][0] in SS and SSlen[j] < 4:
                    LoopLen += len(SSprofile[j])
                elif SSprofile[j][0] == 'S' and SSlen[j] >= 4:
                    LoopFinish = 1
                    if LoopLen > 4:
                        nbLoop += 1
                        Loop[nbLoop]={}
                        start_ind = BefLoop + 1
                        end_ind = j #- 1
                        Loop[nbLoop]['start'] = sum(SSlen[0:start_ind]) +1
                        Loop[nbLoop]['end'] = sum(SSlen[0:end_ind]) #- 1
                        i = j - 1
                    else:
                        i = j - 2
        i += 1

    return Loop

def shrink(SSlog, nbRes):
    start = 0; stop = nbRes; i=0
    while i<nbRes:
       if SSlog[i] == 'S':
          start = i
          i = nbRes
       i += 1
    i = nbRes-1
    while i>0 :
       if SSlog[i] == 'S':
          stop = i+1
          i = 0
       i -= 1
    return start, stop

temp_dir = sys.argv[1]
pdb_name = sys.argv[2]

dssp_name = pdb_name + ".dssp"
cmd = "dssp -i %s/%s -o %s/%s" %(temp_dir, pdb_name, temp_dir, dssp_name)
os.system(cmd)
SSlog,nbRes = read_dssp(temp_dir, dssp_name)
#Loop = find_loop(SSlog,nbRes)
Beg_res, End_res = shrink(SSlog, nbRes)
open_prot = PDB.PDB("%s/%s" %(temp_dir, pdb_name))
Modif_prot = open_prot[Beg_res: End_res]
Modif_prot.out("%s/shrinked_pep.pdb" %temp_dir)
