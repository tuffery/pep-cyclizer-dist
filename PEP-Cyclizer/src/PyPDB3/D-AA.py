#!/usr/bin/env python

import PyPDB.PyPDB as PDB
from PyPDB.Geo3DUtils import *
x = PDB.PDB("PeptideI-Model1.pdb")
x[0].__class__.__name__
x[0].atms.__class__.__name__

atms = world2Peptide(x[0][x[0].atms.CApos()].xyz(), x[0][x[0].atms.Npos()].xyz(), x[0][x[0].atms.Cpos()].xyz(), x[0].atms)
atms = zmirror(atms)
atms = peptide2World(x[0][x[0].atms.CApos()].xyz(), x[0][x[0].atms.Npos()].xyz(), x[0][x[0].atms.Cpos()].xyz(), x[0].atms)
D_aminoacid(x[0])

for i in range(0, len(x)):
    res = D_aminoacid(x[i])

