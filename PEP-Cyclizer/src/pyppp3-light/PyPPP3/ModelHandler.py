### !/usr/bin/env python3

import sys
import os, os.path
from .svm import *

class ModelHandler:
	def __init__(self, root, letters='ACDEFGHIKLMNPQRSTVWY', mapping = {}, verbose=False):
		self.models = {}
		self.pyms = {}
		self.scalings = {}
		self.letters = letters
		self.mapping = mapping
		if verbose:
			print("libsmv:", libsvm)
			print("ModelHandler letters:", letters)
			print("Mapping:", mapping)
		for aa in letters:
			filename = root + aa + '.libsvm.bin'
			# filename = "/home/tuffery/Work/prgs/Src/GitLab/src/PyPPP3/lib/SVM/2012/current2012.bin"
			if verbose:
				sys.stderr.write("loading model %s\n" % filename)
			if not os.path.isfile(filename):
				sys.stderr.write("Could not load SVM model: %s \nPlease check models files. (Have you compiled the models?) \nAborting" % filename)
				sys.exit(0)
			try:
				bin_model = libsvm.svm_load_model_binary(bytes(filename, encoding='utf-8'))
			except:
				sys.stderr.write("ModelHandler: failed to load %s\nHave you compiled the models? \n"%filename)
				sys.exit(0)
			self.models[aa] = bin_model
			try:
				self.pyms[aa] = toPyModel(bin_model)
			except:
				sys.stderr.write("Error(2): Failed to load \"%s\"\nHave you compiled the models? \n"%filename)
				sys.exit(0)
			scaling_file = root + aa + '.libsvm.scaling'
			f = open(scaling_file)
			self.scalings[aa] = [[float(v) for v in x.strip().split()] for x in f]
			f.close()
			# print "labels for %s: %s" % (aa, self.getPym(aa).get_labels())
	
	def __getitem__(self, key):
		if key in self.mapping:
			key = self.mapping[key]
		# print("key", key)
		# print("letters", self.letters)
		if not key in self.letters:
			raise KeyError("No model for this amino acid: %s" % key)
		return self.models[key], self.scalings[key]
	
	def getPym(self, key):
		if key in self.mapping:
			key = self.mapping[key]
		if not key in self.letters:
			raise KeyError("No model for this amino acid: %s" % key)
		return self.pyms[key]
