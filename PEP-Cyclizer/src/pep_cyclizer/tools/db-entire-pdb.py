"""
Creates backbone database to reconstruct BCXSearch hits
A backbone is created for every residue that has a CA
Missing backbone atoms are filled in at -999, -999, -999

Input:
- Database name (DBname).
- Directory where PDB has been downloaded. The script will search for */pdb*.ent.gz

Output:
 - <DBname>.npy file containing all backbone data
    shape: (..., 4, 3)
    dtype: float32
 - <DBname>.segindex file
   One line per contiguous backbone segment
   Fields per line:
     First field: <start coordinate index> (in <DBname>.npy)
     Second field: <residue number of the first residue>
     Second field:  <segment length>
 - <DBname>.pdbindex file
   One line per PDB file
   Fields per line:
     First field: PDB code
     Second field: <start segment index>  (in <DBname>.segindex)
     Third field: <number of segments>
- <DBname>-SEQ.npy file containing all sequences (one letter code)
    shape: (...)
    dtype: |S1 (numpy one-byte dtype)
Existing files are appended, not overwritten.

If two more arguments are added, they are interpreted as chunking arguments,
 selecting just a subset of the PDB files
  chunkstart: the first structure to process
  chunkstep: the step to the next structure.
  e.g. 3 8 selects PDB file 3, 11, 19, 27, ... all the way to the end

Sjoerd de Vries, MTi, 2016-2017
"""

bb_atoms = "N", "CA", "C", "O"

import sys, os
import numpy as np
import glob, gzip
from PyPDB import PyPDB
single_letter = PyPDB.dico_AA

dbname = sys.argv[1]
pdbdir = sys.argv[2]

pdbfiles = sorted(glob.glob(pdbdir + "/*/pdb*.ent.gz"))
if len(sys.argv) > 2:
    chunk_offset, chunk_step = int(sys.argv[3])-1, int(sys.argv[4])
    pdbfiles = pdbfiles[chunk_offset::chunk_step]
if not len(pdbfiles):
    raise Exception("no PDB files (*/.pdb*.ent.gz) in %s" % pdbdir)
pdbs = {}
for pdbfile in pdbfiles:
    pdbcode = os.path.split(pdbfile)[1][3:-7].upper()
    assert pdbcode not in pdbs, pdbcode
    pdbs[pdbcode] = pdbfile
print >> sys.stderr, "%d PDBs found in  %s" % (len(pdbs), pdbdir)

segindexfn = dbname + ".segindex"
pdbindexfn = dbname + ".pdbindex"

segindex = []
pdbindex = []
if os.path.exists(pdbindexfn):
    with open(pdbindexfn) as f:
        for l in f.readlines():
            if l.strip().startswith("#"): continue
            ll = l.split()
            if not len(ll): continue
            code, seg_index, nsegs  = ll[0], int(ll[1]), int(ll[2])
            pdbindex.append((code, seg_index, nsegs))
    with open(segindexfn) as f:
        for l in f.readlines():
            if l.strip().startswith("#"): continue
            ll = l.split()
            if not len(ll): continue
            dbca_offset, resnr, length  = int(ll[0]), int(ll[1]), int(ll[2])
            segindex.append((dbca_offset, resnr, length))
existing_codes = set([p[0] for p in pdbindex])
for pdbcode in existing_codes:
    if pdbcode not in pdbs:
        if len(sys.argv) > 2:
            raise Exception("Existing PDB %s not in chunk, something went wrong" % pdbcode)
        print >> sys.stderr, "WARNING: %s has been removed from the PDB" % pdbcode
    else:
        pdbs.pop(pdbcode)
print >> sys.stderr, "%d PDBs to be processed" % len(pdbs)

coorlen = 0
resmax = 100000
coors = np.empty((resmax,4,3),dtype="float32")
seq = np.empty(resmax,dtype="|S1")

struc_resmax = 100000 #maximum residues in 1 structure
newcoors = np.empty((struc_resmax,4,3),dtype="float32")
newseq = np.empty(struc_resmax,dtype="|S1")
def process_pdb(pdblines, pdbcode):
    newcoors[:,:] = -999
    pdb = PyPDB.PDB(pdblines)
    newsegs = []
    start = coorlen
    pos = 0
    last_resnr = None
    for model in range(pdb.nModel):
        pdb.setModel(model + 1)
        for l in pdb:
            has_ca = False
            resnr = int(l[0].resNum())
            icode = l[0].icode()
            if icode.isalpha():
                continue
            code = single_letter.get(l.name, "X")
            newcoors0 = {}
            has_ca = False
            for ll in l:
                if ll.alt() not in (" ", "A"): continue
                if len(ll.icode().strip()): continue
                if l.name == "MSE" and ll.header().strip() in ("ATOM", "HETATM"):
                    code = "M"
                else:
                    if ll.header().strip() != "ATOM":
                        continue
                aname = ll.atmName().strip()
                if aname == "CA":
                    has_ca = True
                if aname in bb_atoms:
                    newcoors0[aname] = ll.xyz()
            if not has_ca: continue
            newseq[pos] = code
            for bb_atomnr, bb_atom in enumerate(bb_atoms):
                if bb_atom in newcoors0:
                    newcoors[pos,bb_atomnr] = newcoors0[bb_atom]
            pos += 1
            if pos == struc_resmax:
                print >> sys.stderr, "maximum residue count %d exceeded for %s" % (struc_resmax, pdbcode)
                return None, None
            if resnr - 1 != last_resnr:
                if last_resnr is not None:
                    newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos))
                currseg_pos = pos
                currseg_resnr = resnr
            last_resnr = resnr
    if last_resnr is not None:
        newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos+1))


    return pos, newsegs

def writeout():
    segindexf = open(segindexfn, "w")
    pdbindexf = open(pdbindexfn, "w")

    #write out indices
    for index, resnr, nsegs in segindex:
        print >> segindexf, index, resnr, nsegs

    for code, index, nsegs in pdbindex:
        print >> pdbindexf, code, index, nsegs

    #dump coordinates
    np.save(dbname, coors[:coorlen])
    np.save(dbname+"-SEQ", seq[:coorlen])
    pdbindexf.close()
    segindexf.close()

for count, pdbcode in enumerate(pdbs):
    pdbfile = pdbs[pdbcode]
    pdblines = gzip.open(pdbfile).readlines()
    ncoors, newsegs = process_pdb(pdblines, pdbcode)
    if ncoors is None:
        continue
    nsegs = len(newsegs)
    if coorlen + ncoors > resmax:
        resmax = int(max(1.1*resmax, coorlen+ncoors))
        coors.resize((resmax, 4, 3))
        seq.resize(resmax)
    coors[coorlen:coorlen+ncoors] = newcoors[:ncoors]
    seq[coorlen:coorlen+ncoors] = newseq[:ncoors]
    coorlen += ncoors
    #print pdbcode, ncoors, nsegs, coorlen
    pdbindex.append((pdbcode, len(segindex), nsegs))
    segindex.extend(newsegs)
    if (count+1) % 500 == 0:
        print >> sys.stderr, "%d/%d PDBs processed" % (count+1, len(pdbs))
        writeout()

writeout()
