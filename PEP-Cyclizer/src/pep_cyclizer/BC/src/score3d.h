#include "BC.h"

typedef Real (*Score3dfunc)(const Coord *, const Coord *, int, int, int);
Real BC1(const Coord *X, const Coord *Y, int from1, int from2, int len);
Real BC2(const Coord *X, const Coord *Y, int from1, int from2, int len);
Real BC3(const Coord *X, const Coord *Y, int from1, int from2, int len);
