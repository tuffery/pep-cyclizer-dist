#!/usr/bin/env python3
import os,sys

# ------------------------- Globals
VERSION          = "3.0"
# -------------------------

# PPP_ROOT is the main variable to configure.
# It is the absolute path to the PyPPP3 directory (with lib, bin, PyPPP3 subdirs)
#
# DFLT_BLAST_BANK_ROOT and DFLT_SVM_LIB_ROOT_PATH paths can be tuned.
# Other parameters can in general be left unmodified.

# Superseeded by environment variable if defined.
if  'PPP_ROOT' in os.environ:
    PPP_ROOT = os.environ['PPP_ROOT']
else:
    print("PPP_ROOT is not defined in the environment variables")
    sys.exit(0)

if 'BANK_ROOT' in os.environ:
    BANK_ROOT = os.environ['BANK_ROOT']
else:
    print("BANK_ROOT is not defined in the environment variables")
    sys.exit(0)

# Root directory for blast banks (should contain subdirectories depending on versions)
DFLT_BLAST_BANK_ROOT = "%s/blast_db" % BANK_ROOT
# The root directory for svm models (should contain subdirectories depending on versions)
DFLT_SVM_LIB_ROOT_PATH   = "%s/SVM" % BANK_ROOT

# ===========================================================
# Definitions below this line should not move (unless you are a PyPPP3 expert)
# ===========================================================

DFLT_SOCKET = PPP_ROOT + "/tmp/.pyppp3.socket"
DFLT_WPATH         = "."
DFLT_LABEL         = "PPP"
DFLT_PPP_BIN_PATH  = "%s/bin" % PPP_ROOT

# ===================================
# BLAST SECTION
# ===================================

# BLAST BINARIES. PSIBLAST SUPPOSED IN DEFAULT PATH
# BLASTPGP MUST BE ADDED
DFLT_BLASTPGP_BIN_PATH    = DFLT_PPP_BIN_PATH
DFLT_MAKEMAT_PATH         = DFLT_PPP_BIN_PATH
DFLT_GETCLEANMATRIX_PATH  = DFLT_PPP_BIN_PATH
DFLT_PSIBLASTMOD_BIN_PATH = DFLT_PPP_BIN_PATH
DFLT_BLASTPGP_MAT_PATH    = "%s/lib/blastpgp_data/" % PPP_ROOT # BLOSUM80
DFLT_BLOSUM          = "BLOSUM80"    # "BLOSUM62"

# BLAST BANKS
DFLT_BLAST_DB_PATH   = "%s/2014-11-02" % DFLT_BLAST_BANK_ROOT
DFLT_BLAST_BANK      = "uniref30.db" # "uniref50.db"

DFLT_BLAST_2012_DB_PATH = "%s/2012-10-31" % DFLT_BLAST_BANK_ROOT
DFLT_BLAST_2006_DB_PATH = "%s/2006-04-27" % DFLT_BLAST_BANK_ROOT
DFLT_BLAST_2016_DB_PATH = "%s/2014-11-02" % DFLT_BLAST_BANK_ROOT

DFLT_BLAST_2012_DATABASE = "uniref90"
DFLT_BLAST_2006_DATABASE = "uniref90"
DFLT_BLAST_2016_DATABASE = "uniref30.db"

# ===================================
# LIBSVM SECTION
# ===================================

DFLT_SVM_2012_PATH  = "%s/2012/" % DFLT_SVM_LIB_ROOT_PATH
DFLT_SVM_2006_PATH  = "%s/2006/" % DFLT_SVM_LIB_ROOT_PATH
DFLT_SVM_2016_PATH  = "%s/2016/psibl_mod_uniref30/" % DFLT_SVM_LIB_ROOT_PATH
DFLT_SVM_MODEL      = ""

# pointer to libsmv dynamic library 
# DFLT_LIBSVM_PATH    = "%s/thirdparty/libsvm" % PPP_ROOT
DFLT_DL_LIBSVM_PATH   = "%s/lib/libsvm" % PPP_ROOT
DFLT_NCORES         = 8
DFLT_EVALUE         = 0.1

