#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018

Adapted by P. Tuffery May 2019 to make it independent of PEP-Cylizer naming context, and compliant with python3.

Given a series of sequences, derive a HMM model so as to propose candidate sequences.

./likely_sequences.py --aa_constraint_fname /home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs/constraints_list.data  --input-sequences /home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs/all.fasta --logo_fname test.png --model_s_fname test_candidate_sequences.txt

# gives a division by 0
export ROOT=/home/tuffery/Work/prgs/Src/GitLab/src/PEP-Cyclizer
export DATAROOT=$ROOT/test/otherConfs
export DATAROOT=/home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs
export conf=uII-1
export UII=$DATAROOT/$conf
$ROOT/src/pep-cyclizer/likely_sequences.py --input-sequences $UII/pep_cyclizer_test/Gap3/BCSearch/${conf}_sequence_guess_gap3.fasta --model_s_fname $UII/${conf}_candidate_linkers.txt --aa_constraint_fname $DATAROOT/constraints_list.data 


export DATAROOT=/home/tuffery/Work/2019/PEP-Cyclizer/toTest/26RFa
$ROOT/src/pep-cyclizer/likely_sequences.py --input-sequences $DATAROOT/All.fasta --model_s_fname 26RFa-All_candidate_linkers.txt --aa_constraint_fname $DATAROOT/constraints_list.data 


export DATAROOT=/home/tuffery/Work/2019/PEP-Cyclizer/toTest/uII/otherConfs
$ROOT/src/pep-cyclizer/likely_sequences.py --input-sequences $DATAROOT/All.fasta --model_s_fname uII-All_candidate_linkers.txt --aa_constraint_fname $DATAROOT/constraints_list.data 

path = os.path.dirname(a_module.__file__)
print(os.path.dirname(os.path.realpath(__file__)))
"""

###########################
###  L I B R A R I E S  ###
###########################

import sys, os
import argparse
import math
import numpy as np
import random
import aa_constraints

DFLT_MODEL_S_FNAME = "pep_cyclizer_candidate_sequences.txt"
AAseq = ["A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"]
AAs = "ACDEFGHIKLMNPQRSTVWY"

# Weights for Dirichlet applied to transition matrix.
TM_REF_WEIGHT = 1.0 #0.9
TM_OBS_WEIGHT = 0.0 #0.1

# Weights for Dirichlet applied to amino acid frequencies.
FR_REF_WEIGHT = 0.
FR_OBS_WEIGHT = 1.

# Minimal frequency assigned to constrained amino acids 
PSEUDO_FREQ = 0.8 #0.1

# Maximal number of sequences drawn (not considering redundancy)
OUT_NSEQ = 4000

# Maximal number of tries to generate OUT_NSEQ 
MAX_TRIES = 10000

# Default pseudo random seed to use.
DLFT_SEED = -1

def effectives(FASTA, n_AA_types = 20):
    """
    input : sequences (list of strings)
    return: np.array of aas per position
    """
    n_seq = len(FASTA)
    s_sze = len(FASTA[0]) # Assume all sequences have identical sizes
    Eff   = np.zeros((s_sze, n_AA_types))

    # effectives per alignment position
    for s_index in range(n_seq):
        for s_pos in range(s_sze):
            aa = FASTA[s_index][s_pos]
            aa_tpe = AAs.index(aa)
            Eff[s_pos][aa_tpe] += 1
    return Eff

def effectives_to_frequencies(Eff):
    """
    convert np.array of effectives into a new np.array of frequencies
    """
    (s_sze, n_AA_types)  = Eff.shape
    Freq   = np.copy(Eff)
    
    for pos in range(s_sze):
        summ = 0.
        for aa in range(n_AA_types):
            summ += Eff[pos][aa]
        Freq[pos] /= summ
    return Freq

def combine_frequencies(F1, w1, F2, w2):
    """
    convert frequencies with pseudo count like strategy.
    """
    (d1) = F1.shape
    oF = np.zeros(d1)
    oF = F1 * w1 + F2 * w2
    oF /= (w1 + w2)
    return oF


def constrained_frequencies(Freq, raw_constraints = None, pseudo_F = 0.01):
    # s = F.shape() # array dimension
    (npos, naas)  = Freq.shape
    F   = np.copy(Freq)
    if raw_constraints == None:
        return F

    # print("Freq before constraints:", Freq)
    for pos in raw_constraints.keys():
        c_pos = pos #- 1 # from 0, file is from 1
        c_aas = raw_constraints[pos]
        # print(c_pos, c_aas)
        summ = 0.
        for aa in range(naas):
            if (AAs[aa] in c_aas):
                F[c_pos][aa] += pseudo_F
            else:
                F[c_pos][aa] = 0.
            summ += F[c_pos][aa]
        F[c_pos] /= summ
    # print("F after:", F)
    
    return F

def constrained_frequencies_bank(Freq, raw_constraints = None, bank_Freq = None, pseudo_F = 0.01):
    # s = F.shape() # array dimension
    (npos, naas)  = Freq.shape
    F   = np.copy(Freq)
    if raw_constraints == None:
        return F

    # print("Freq before constraints:", Freq)
    for pos in raw_constraints.keys():
        c_pos = pos #- 1 # from 0, file is from 1
        c_aas = raw_constraints[pos]
        # print(c_pos, c_aas)
        summ = 0.
        for aa in range(naas):
            if (AAs[aa] in c_aas):
                F[c_pos][aa] += pseudo_F * bank_Freq[aa] # eq1: F[c_pos][aa] += bank_Freq[aa] # eq2: F[c_pos][aa] += pseudo_F * bank_Freq[aa] # eq3: F[c_pos][aa] += pseudo_F
            else:
                F[c_pos][aa] = 0.
            summ += F[c_pos][aa]
        F[c_pos] /= summ
    # print("F after:", F)
    
    return F

def uniform_transitions(n_AA_types = 20):
    """
    Uniform transition model
    """
    Trans = np.zeros((n_AA_types, n_AA_types))
    Trans.fill(1./n_AA_types)
    return Trans

def databank_transitions(fname, n_AA_types = 20):
    """
    Transition model inferred from loop regions in proteins.
    """
    Trans = np.zeros((n_AA_types, n_AA_types))
    f = open(fname)
    lines = f.readlines()
    f.close()
    for aa, l in enumerate(lines):
        it = l.split("\n")[0].split()
        if len(it) != n_AA_types:
            print("observed_transitions: incorrect number of values per line (%d when %d expected)" % (len(it),n_AA_types), file = sys.stderr)
            sys.exit(0)
        for i in range(len(it)):
            Trans[aa][i] = float(it[i])
    return Trans
    
def databank_frequencies_and_transitions(fname, n_AA_types = 20):
    """
    Transition model inferred from loop regions in proteins.
    """
    Trans = np.zeros((n_AA_types, n_AA_types))
    Freq = np.zeros(n_AA_types)
    f = open(fname)
    lines = f.readlines()
    f.close()

    # Freq
    it = lines[0].split("\n")[0].split()
    if len(it) != n_AA_types:
        print("observed_frequencies: incorrect number of values per line (%d when %d expected)" % (len(it),n_AA_types), file = sys.stderr)
        sys.exit(0)
    for i in range(len(it)):
        Freq[i] = float(it[i])

    # TM
    for aa, l in enumerate(lines[2:]):
        it = l.split("\n")[0].split()
        if len(it) != n_AA_types:
            print("observed_transitions: incorrect number of values per line (%d when %d expected)" % (len(it),n_AA_types), file = sys.stderr)
            sys.exit(0)
        for i in range(len(it)):
            Trans[aa][i] = float(it[i])
    return Freq, Trans
    
def observed_transitions(FASTA, n_AA_types = 20):
    """
    infer transitions from sequences
    since the inferrence comes over few sequences, pseudocounts to consider (see combine_transitions)
    """
    n_seq = len(FASTA)
    s_sze = len(FASTA[0]) # Assume all sequences have identical sizes
    Trans = np.zeros((n_AA_types, n_AA_types))

    # Transitions over observed sequences
    for s_index in range(n_seq):
        for s_pos in range(1, s_sze):
            aa = FASTA[s_index][s_pos]
            aa_tpe = AAs.index(aa)
            paa = FASTA[s_index][s_pos-1]
            paa_tpe = AAs.index(paa)
            Trans[paa_tpe][aa_tpe] += 1

    # Normalize
    for paa_tpe in range(n_AA_types):
        summ = 0.
        for aa_tpe in range(n_AA_types):
            summ += Trans[paa_tpe][aa_tpe]
        if summ == 0.:
            summ = 0.0001
        Trans[paa_tpe] /= summ

    return Trans

def combine_transitions(Trans1, w1, Trans2, w2):
    """
    Combine transitions with pseudo count like strategy.
    """
    (d1, d2) = Trans1.shape
    Trans = np.zeros((d1, d2))
    for state in range(d1):
        Trans[state] = Trans1[state] * w1 + Trans2[state] * w2
        Trans[state] /= (w1 + w2)
    return Trans

def forward_func(F, PP, Pinit=None):
    """
    F : the observed frequencies of states at each position (numpy array)
    PP: the transitions from state to state at each position
    """
    # print("Forward:")
    # print("F:",F)
    # print("PP:",PP)
    # print("Pinit:",Pinit)
    
    (n_pos, n_states)  = F.shape
    # print("n_pos:",n_pos,"n_states:",n_states)
    
    fwd = np.zeros((n_pos, n_states))
    summ = 0.
    for state in range(n_states):
        # fwd[0][state] = F[0][state]*Pinit[state] # remove Pinit left for debug
        fwd[0][state] = F[0][state] # remove Pinit left for debug
        summ += fwd[0][state]
    fwd[0] /= summ
    
    # forward
    for pos in range(1,n_pos):
        for s in range(n_states):
            summ = 0.0
            for s2 in range(n_states):
                # summ += (fwd[pos-1][s2] * PP[pos-1][s2][s])
                summ += (fwd[pos-1][s2] * PP[s2][s]) # use a n_state * n_state matrix, position independant
            fwd[pos][s] = summ*F[pos][s]

    # normalize. UNCOMMENT when using backtrack_func
    for pos in range(n_pos):
        summ = 0.0
        for s in range(n_states):
            summ += fwd[pos][s]
        fwd[pos] /= summ

    # print("fwd:",fwd)
    return np.transpose(fwd)

def backtrack_func(fwd, PP, n_paths, max_tries = 2000, verbose = False):

    (n_states, n_pos) = fwd.shape
    tmp   = np.zeros(n_states)
    paths = np.zeros((n_pos,n_paths))

    # print("backtrack:")
    # print("fwd:",fwd)
    # print("states:", n_states, "pos: ", n_pos)
    # print("paths:", paths.shape)

    avoid_loops = 0
    cur_path = 0
    while (cur_path < n_paths) and (avoid_loops < max_tries) :
        
        pick = -1
        max_fwd = 0
        norme = 0

        # # Initial position (last pos)
        # r = random.random()
        # summ  = 0.
        # state = 0
        # while state < n_states :
        #     summ += fwd[theSze-1][state]
        #     if r <= summ :
        #         pick  = state
        #         state = n_states
        #     state+=1 
        # paths[n_pos-1][cur_path] = int(pick)

        # backtrack
        cur_pos = n_pos-1
        while cur_pos>=0 :

            if cur_pos == n_pos-1:
                np.copyto(tmp, fwd[:,cur_pos])
            else:
                norme=0
                for state in range(n_states):
                    # tmp[state] = fwd[state][cur_pos] * PP[cur_pos][state][int(paths[cur_pos+1][cur_path])]
                    # print("cur_pos:",cur_pos,"state:", state, "path to:", int(paths[cur_pos+1][cur_path]))
                    # print(fwd[state][cur_pos])
                    # print(PP[state][int(paths[cur_pos+1][cur_path])])
                    tmp[state] = fwd[state][cur_pos] * PP[state][int(paths[cur_pos+1][cur_path])]
                    norme += tmp[state]
                tmp /= norme
            # print("bck[",cur_pos,"]:",tmp)

            r = random.random()
            summ=0
            state=0
            pick= -1
            while state < n_states:
                summ += (tmp[state])
                if r <= summ :
                    pick = state
                    state = n_states
                state+=1

            is_break = False
            if pick == -1 :
                avoid_loops += 1
                cur_path -= 1
                is_break = True
                if avoid_loops > max_tries :
                    sys.stderr.write("failed to find a path\n")
                    return
                else:
                    sys.stderr.write("failed, retry to find a path\n")
            else:
                paths[cur_pos][cur_path] = pick
                
            cur_pos-=1

        if is_break:
            # print("avoid_loop:",avoid_loop)
            continue
        
        if cur_path>0:
            redundant = [np.array_equal(paths[:,cur_path],paths[:,row]) for row in range(0,cur_path)]
        else:
            redundant = ["False"]

        if not True in redundant:       
            cur_path+=1
        else:
            avoid_loops += 1

    # print(paths[:,0:cur_path])
    # print("paths:", cur_path,"\n",np.transpose(paths[:,0:cur_path]))
            
    return paths[:,0:cur_path]

def shrink_paths(paths):
    n_pos, n_path = paths.shape
    opaths = np.zeros((n_pos-2, n_path))
    for path in range(n_path):
        for pos in range(0, n_pos-2):
            opaths[pos][path] = paths[pos+1][path]
    return opaths

def paths_scores(paths, Freq, PP):
    # print("Freq:", Freq)
    # aa_p = aa_paths(paths)
    # print("paths:", aa_p)
    n_pos, n_path = paths.shape
    path_score = np.zeros(n_path)
    for path in range(n_path):
        path_score[path] = 0.
        for pos in range(0,(n_pos-1)):
            path_score[path] += np.log(Freq[pos][int(paths[pos][path])])
            path_score[path] += np.log(PP[int(paths[pos][path])][int(paths[pos+1][path])])
        path_score[path] += np.log(Freq[n_pos-1][int(paths[n_pos-1][path])])
        # path_score[path] *= (-1) # use reversed instead
        # print(aa_p[path], path_score[path])

    return path_score

def aa_paths(paths, Labels = AAs, shrink_ter = False):
    n_pos, n_path = paths.shape
    if shrink_ter:
        n_pos -= 2
    path_AA = np.zeros((n_path,n_pos), dtype='U')
    if shrink_ter:
        for path in range(n_path):
            for pos in range(n_pos):
                path_AA[path][pos] = Labels[int(paths[pos+1][path])]
    else:
        for path in range(n_path):
            for pos in range(n_pos):
                path_AA[path][pos] = Labels[int(paths[pos][path])]

    return path_AA


def read_sequences(seq_file):
    """
    Read fasta file. Return sequences as a list of strings.
    Sequences are expected to be aligned (constant number of aas)
    """
    rs = []
    f = open(seq_file)
    lines = f.readlines()
    f.close()
    aln_sze = None
    for l in lines:
        if l.startswith(">") : continue
        s = l.split()[0]
        if not aln_sze:
            aln_sze = len(s)
        else:
            if len(s) != aln_sze:
                sys.stderr.write( "Sorry: detected sequences of different sizes (%d/%d) for %s\n" % (aln_sze, len(s), s) )
        rs.append(s)
    return aln_sze, rs

def constrain_frequencies(F, aa_constraint_f, pseudo_F = 0.01):
    # s = F.shape() # array dimension
    (npos, naas)  = F.shape

    f = open(aa_constraint_f, 'r')
    lines = f.readlines()
    f.close()

    # print("F before:", F)
    for l in lines:
        it  = l.split(":")
        c_pos = int(it[0]) - 1 # from 0, file is from 1
        c_aas = "".join( it[1:])
        # print(c_pos, c_aas)
        summ = 0.
        for aa in range(naas):
            if (AAs[aa] in c_aas):
                F[c_pos][aa] += pseudo_F
            else:
                F[c_pos][aa] = pseudo_F
            summ += F[c_pos][aa]
        F[c_pos] /= summ
    # print("F after:", F)
    
    return F
        
def likely_sequences(acc_seq_f, aa_constraint_f, out_f, out_nseq = 1000, max_tries = 2000, tm_ref_w = TM_REF_WEIGHT, tm_obs_w = TM_OBS_WEIGHT, fr_ref_w = FR_REF_WEIGHT, fr_obs_w = FR_OBS_WEIGHT, min_pseudo_freq = PSEUDO_FREQ, use_sized_bank = False, Nter_aa = None, Cter_aa = None, verbose = False):
    """
    acc_seq_f       : filename of aligned sequences (fasta)
    aa_constraint_f : filename defining amino acid contraints per position of the aln.
    out_f           : the result file
    out_nseq        : the maximal number of non similar sequences drawn/estimated
    max_tries       : the maximal number of failures during backtracking
    tm_ref_w        : weight applied to a transition matrix inferred from collection of proteins. Note frequencies are renormalized.
    tm_obs_w        : weight applied to the transition matrix deduced from collected sequences using BCLoop. Note frequencies are renormalized.
    fr_ref_w        : weight applied to frequencies inferred from collection of proteins (bank). Note frequencies are renormalized.
    fr_obs_w        : weight applied to frequencies deduced from collected sequences using BCLoop. Note frequencies are renormalized.
    min_pseudo_freq : minimal frequency applied to constrained amino acids when none was observed. Note frequencies are renormalized.
    use_sized_bank  : use loop_sze_dependant banks, instead of "any" by default.
    Nter_aa         : the amino acid on the Nter extremity of the linker (Nter flank)
    Cter_aa         : the amino acid on the Cter extremity of the linker (Cter flank)
    verbose         : verbose mode (for debug)
    """

    rs = ["#Sequence Score\n"]
    #######################################################
    ### Parameters
    #######################################################
    # const_Labels = AAseq
    # const_AA     = [int(AAseq.index(const_Labels[j])) for j in range(len(const_Labels))]
    # if aa_constraint_f != None:
    #     # PT 2019: This supposes all positions have identical constraints!
    #     f = open(aa_constraint_f, 'r')
    #     lines = f.readlines()
    #     f.close()
    #     label_line   = (lines[0].split()[0]).split(':')
    #     const_Labels = label_line[1:len(label_line)]
    #     const_AA = [int(AAseq.index(const_Labels[j])) for j in range(len(const_Labels))]
    # MAXTRIES = 2000
    # k = 300

    #
    # Read the sequences (fasta) and calculate transitions (unconstrained)
    #

    # Observed data.
    
    loop_sze, seq_fasta = read_sequences(acc_seq_f)
    # print("Sequences:", seq_fasta)
    Eff = effectives(seq_fasta)  # per position aa effectives over the aligned sequences (returned by BCsearch).
    # print("Effectives:", Eff)

    # databank frequencies and transitions
    bFreq, bTrans = databank_frequencies_and_transitions(os.path.dirname(os.path.realpath(__file__))+"/lib/Loop_faa_tmat-any.txt")
    if use_sized_bank:
        if (loop_sze > 1) and (loop_sze <= 10): 
            bFreq, bTrans = databank_frequencies_and_transitions(os.path.dirname(os.path.realpath(__file__))+"/lib/Loop_faa_tmat-%d.txt" % loop_sze)
    np.set_printoptions(precision=3)
    np.set_printoptions(linewidth=150)
    if verbose:
        print("bank frequencies:", bFreq)
        print("bank transitions:", bTrans)

    # observed frequencies and transitions
    oFreq = effectives_to_frequencies(Eff)
    if aa_constraint_f != None:
        raw_constraints = aa_constraints.raw_constraints(aa_constraint_f)
        # print(raw_constraints)
        if verbose:
            print("min_pseudo_freq:", min_pseudo_freq)
        if use_sized_bank:
            cFreq = constrained_frequencies_bank(oFreq, raw_constraints, bFreq, pseudo_F = min_pseudo_freq)
        else:
            cFreq = constrained_frequencies(oFreq, raw_constraints, pseudo_F = min_pseudo_freq)
    else:
        cFreq = oFreq
    if verbose:
        print("bank_Freq:", bFreq)
        print("obs_Freq:", oFreq)
        print("cnst_Freq:", cFreq)
        
    # print("Frequencies:", Freq)
    oTrans = observed_transitions(seq_fasta)
    # uTrans = uniform_transitions() # should be inferred from observations on loops
    # print("Transitions:", Trans)

    # Pseudo counts

    # transitions
    # bTrans = databank_transitions(os.path.dirname(os.path.realpath(__file__))+"/lib/Loop_tmat.txt")
    dTrans = combine_transitions(bTrans, tm_ref_w, oTrans, tm_obs_w) # 0.3, 07 to be questionned.
    if verbose:
        print("obs_Trans:", oTrans)
        print("bank_Trans:", bTrans)
        print("Combined Transitions:", dTrans)

    
    dFreq = combine_frequencies(bFreq, fr_ref_w, cFreq, fr_obs_w)
    # dFreq = cFreq
    if verbose:
        print("bank_Freq:", bFreq)
        print("obs_Freq:", oFreq)
        print("cnst_Freq:", cFreq)
        print("Combined Frequencies:", dFreq)

    #
    # Run the forward
    #
    if (Nter_aa != None) and (Cter_aa != None):
        # take into account the flanking amino acids
        pinit = np.zeros(20)
        pend = np.zeros(20)
        pinit[AAs.index(Nter_aa)] = 1.
        pend[AAs.index(Cter_aa)] = 1.
        ddFreq = np.zeros((loop_sze+2, 20))
        ddFreq[0]          = pinit
        ddFreq[loop_sze+1] = pend
        for pos in range(1,loop_sze+1):
            ddFreq[pos] = dFreq[pos-1]
        ffwd = forward_func(ddFreq, dTrans)
    else:
        # Do not take into account the flanking amino acids
        ffwd = forward_func(dFreq, dTrans, dFreq[0])
        ddFreq = dFreq
        
    if verbose:
        print("Forward2:", ffwd)
        print("shape:", ffwd.shape)

    #
    # Run the backtrack
    #
    paths = backtrack_func(ffwd, dTrans, n_paths = out_nseq, max_tries = max_tries, verbose = verbose)
    # print("Paths:", paths)
    paths_AA = aa_paths(paths)
    # print("ini_paths_AA:", paths_AA)
    # if (Nter_aa != None) and (Cter_aa != None):
    #     paths_AA = aa_paths(paths, shrink_ter = True)
    #     paths = shrink_paths(paths)
    # print("paths_AA:",paths_AA)

    
    #
    # scores
    #
    scores = paths_scores(paths, ddFreq, dTrans)
    # print("paths_scores:",scores)

    # FW_BT_path, path_score = forw_back(positions, Labels, Pinit, Freq, Trans, MAXTRIES, k)
    # sys.exit(0)
    n = len(scores)
    sort_ind = reversed(np.argsort(scores))
    for ind in sort_ind:
        keys = paths_AA[ind]
        rs.append( "%s %0.2f\n" % ("".join(keys), scores[ind]*1.) )
    
    # print("".join(rs))
    # write to disk
    fbt_f = open(out_f, "w")
    fbt_f.write( "".join(rs) )
    fbt_f.close()
    return
    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    #    parser.add_argument("--seq_sze", dest="seq_sze", type = int, help="The size of the aligned sequences",
    #                       action="store", default=None)  

    parser.add_argument("--input-sequences", dest="input_sequences", help="The input sequences (FASTA format)",
                        action="store", default=None)  

    parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
                        action="store", default=None) 
    
    # parser.add_argument("--true_seq", dest="true_seq", help="the exact target sequence",
    #                     action="store", default=None)

    parser.add_argument("--model_s_fname", dest="model_s_fname", help="the name of the file with modelled sequences inferred from matching sequences.",
                        action="store", default=DFLT_MODEL_S_FNAME)

    parser.add_argument("--out_nseq", dest="out_nseq", help="The maximal number of sequences drawn/estimated using fbt (%d)" % OUT_NSEQ,
                        action="store", type=int, default=OUT_NSEQ) 
    
    parser.add_argument("--max_tries", dest="max_tries", help="The maximal number of failed backtracking (%d)" % MAX_TRIES,
                        action="store", type=int, default=MAX_TRIES) 
     
    parser.add_argument("--tm_ref_w", dest="tm_ref_w", help="Dirichlet like weight for reference transition matrix (%f)" % TM_REF_WEIGHT,
                        action="store", type=float, default=TM_REF_WEIGHT)
    
    parser.add_argument("--tm_obs_w", dest="tm_obs_w", help="Dirichlet like weight for observed transition matrix (%f)" % TM_OBS_WEIGHT,
                        action="store", type=float, default=TM_OBS_WEIGHT) 
    
    parser.add_argument("--fr_ref_w", dest="fr_ref_w", help="Dirichlet like weight for reference frequencies(%f)" % FR_REF_WEIGHT,
                        action="store", type=float, default=FR_REF_WEIGHT)
    
    parser.add_argument("--fr_obs_w", dest="fr_obs_w", help="Dirichlet like weight for observed frequencies (%f)" % FR_OBS_WEIGHT,
                        action="store", type=float, default=FR_OBS_WEIGHT) 
    
    parser.add_argument("--min_pseudo_freq", dest="min_pseudo_freq", help="Minimal observed frequency for constrained amino acids (%f)" % PSEUDO_FREQ,
                        action="store", type=float, default=PSEUDO_FREQ) 

    parser.add_argument("--use_sized_bank", dest="use_sized_bank", help="use databank size dependent transitions and frequencies",
                        action="store_true", default=True) #False) Y.K. using the sized bank

    parser.add_argument("--cter_aa", dest="cter_aa", help="cter amino acid of the linker (corresponds to Nter amino acid of open peptide)",
                        action="store", default=None) 

    parser.add_argument("--nter_aa", dest="nter_aa", help="nter amino acid of the linker (corresponds to Cter amino acid of open peptide)",
                        action="store", default=None)
    
    parser.add_argument("--seed", dest="seed", help="Random seed value (int, negative value means system time, i.e. random) (%d)" % DLFT_SEED,
                        action="store", type=int, default=DLFT_SEED) 

    parser.add_argument("-v", "--verbose", dest="verbose", help="verbose mode active",
                        action="store_true", default=False) 

    return parser

def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.seed >= 0:
        random.seed(options.seed)
    else:
        random.seed()

    #print((print_options(options)))
    return options

def run(input_sequences, aa_constraint_fname, candidate_sequences_fname):
    """
    input_sequences   : the aligned sequences to generate the logo from.
    true_seq          : case where experimental linker sequence is known (one string)
    aa_constraint_fname: file specifying constraints per positions.
    candidate_sequences_fname : the name of the sequences inferred from data (was "label_candidate_sequences.txt")
    return: none
    """
    
    # true_seq = None
    # aa_cons, not_cons = aa_constraints.summed_aa_constraints(true_seq, aa_constraint_fname)
    
    # if os.path.exists(acc_seq_fname):
    # print("Sequence model adapted from %s" % input_sequences)
    # print("aa_cons: %s" % aa_cons)
    # print("aa_others: ", not_cons)
    # print("Candidate sequences written to: %s" % candidate_sequences_fname)
    
    likely_sequences(input_sequences, aa_constraint_fname, candidate_sequences_fname)

if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    if not options.input_sequences:
        sys.stderr.write("Sorry: likely_sequences.py needs a valid filename of aligned sequences (FASTA) to infer candidate sequences\n")
        sys.exit(0)
        
    likely_sequences(options.input_sequences, options.aa_constraint_fname, options.model_s_fname, options.out_nseq, options.max_tries, options.tm_ref_w, options.tm_obs_w, options.fr_ref_w, options.fr_obs_w, options.min_pseudo_freq, options.use_sized_bank, options.nter_aa, options.cter_aa, options.verbose)

