#!/usr/bin/env python3
"""
author: Yasaman Karami
12 December 2018
MTi

Modified by P. Tuffery, 2019: compliance to python3, including PyPDB, more understandable parameter names, and less magic file naming.
/home/tuffery/Work/prgs/Src/GitLab/src/PEP-Cyclizer/src/pep-cyclizer/Prep_input.py --target uII-1.pdb --linker_sze 3
"""

###########################
###  L I B R A R I E S  ###
###########################
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse

# DFLT_WORK_PATH     = "./"
DFLT_WORK_PATH     = None
DFLT_FLANK_SZE     = 4
DFLT_MODE          = "machine"
DFLT_BANK_PATH     = "/scratch/banks/bc_banks"
DFLT_PDB_CLS_PATH  = DFLT_BANK_PATH + "/PDB_clusters/"
# PROT_DIR           = "pep_cyclizer_candidate_search"
WORKING_SUBDIR     = "pep_cyclizer_candidate_search"
AAs = "ARNDCEQGHILKMFPSTWYV"
AA3 = ("ALA","ARG","ASN","ASP","CYS","GLU","GLN","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL")
#########################################################################
#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    if not name_dir.endswith('/'):
        name_dir += '/'
    return name_dir

def write_error_file(target_dir):
    error_file = open("%s/error.log" %target_dir, "w")
    error_file.write("True\n")
    return

def get_ca(pdb):
    BB = ["CA"]
    coors = []
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue
            if atom.atmName().strip() in BB:
                coors.append(atom.xyz())
    return np.array(coors, dtype = "double")

## Remove homologs
def remove_homolog(path_to_pdb_clusters, model_dir, pdbID):
    """
    Identify homologs of protName in PDB, based on name and pdbclusters70
    """
    sys.stderr.write("Will discard homologs for: %s\n" %  pdbID)
    PDBcode = []
    flag = 0
    line = open(path_to_pdb_clusters + "bc-70.out","r").readlines()
    for ll in range(0,len(line)):
        cluster = line[ll].split(" ")[0:-1]
        pdbClust = []
        for k in range(0,len(cluster)):
            pdbClust.append(str.lower(cluster[k]).split("_")[0])
        if(pdbID in pdbClust):
            flag = 1
            break
        
    if flag == 0:
        sys.stderr.write("Detected no homologs\n")
        return PDBcode # pdbId not found
    
    for i in range(0,len(pdbClust)):
        code = "pdb" + pdbClust[i]
        if code not in PDBcode:
            PDBcode.append(code)
    sys.stderr.write("Writing list of discarded homologs to: %s\n" % (model_dir + "homologs.list"))
    
    clusterID = open(model_dir + "homologs.list","w")
    for i in range(0,len(PDBcode)):
        clusterID.write(PDBcode[i] + "\n")
    clusterID.close()

def clean_input_pdb(model_dir, input_PDB, target_dir):
    '''
    check input pdb and prepare it for cyclization:
    read the first model, first chain, ignore water molecules and 
    convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    take care of alternate coordinates
    '''
    ### check if the PDB file is valid
    if len(input_PDB) == 0:
        write_error_file(target_dir)
        sys.stderr.write("Error! The input PDB file is not valid!\n")
        sys.exit(0)

    ### uncomments this part for the web-server Y.K.
    #if len(input_PDB) < 8:
    #    sys.stderr.write("Error! The input PDB file is too small, the peptide must have a minimum size of 8 residues!\n")
    #    sys.exit(0)

    ### read the first model and the first chain
    nb_models = input_PDB.nModels()
    if nb_models > 1:
        sys.stderr.write("Only the first model from the input PDB is considered!\n")
        input_PDB.setModel(1)

    all_chains = input_PDB.chnList()
    if len(all_chains) > 1:
        sys.stderr.write("Only the first chain from the input PDB is considered!\n")
        GappedPDB = input_PDB.nChn(all_chains[0])
    else:
        GappedPDB = input_PDB

    ### clean the input PDB and ignore amide and carboxyle groups at the terminals
    GappedPDB.clean()
    refe_seq = GappedPDB.aaseq()
    start_ind = 0; end_ind = len(GappedPDB)
    if refe_seq[0] == 'X':
        start_ind = 1
    if refe_seq[len(GappedPDB) - 1] == 'X':
        end_ind = len(GappedPDB) - 1
    mod_GappedPDB = GappedPDB[start_ind : end_ind]
    refe_seq = mod_GappedPDB.aaseq()

    ### check for modified amino acids
    if 'X' in refe_seq:
        write_error_file(target_dir)
        sys.stderr.write("Error! PDB to cyclize can only contain standard amino acids!\n")
        sys.exit(0)

    ### check for insertion codes
    mask_list = []
    for ll in range(len(mod_GappedPDB)):
        if mod_GappedPDB[ll].riCode() != ' ':
           mask_list.append(ll)

    if len(mask_list) > 0:
        start = 0; i = 0
        while mask_list[i] == start:
            start += 1
            i += 1
        sub_pdb = mod_GappedPDB[start : mask_list[i]]
        while i < (len(mask_list)-1):
            i += 1
            if (mask_list[i-1]+1) == mask_list[i]: continue
            sub_pdb += mod_GappedPDB[(mask_list[i-1]+1) : mask_list[i]]
        if mask_list[i] < (len(mod_GappedPDB)-1):
            sub_pdb += mod_GappedPDB[(mask_list[i]+1) : len(mod_GappedPDB)]
    else:
        sub_pdb = mod_GappedPDB

    ### check for alternate atoms
    '''
    flag_alt = sum(sub_pdb.hasAltAtms())
    if flag_alt > 0:
        for rr in sub_pdb:
            if sum(rr.hasAltAtms()) > 0:
                del_list = []
                for aa in rr:
                    if rr.alt() not in (" ", "A"):
                        del_list.append(rr.atmName())
                    else:
                        rr.alt(" ")
                rr.delete(del_list)    
    '''
    sub_pdb.out(model_dir + "modified_open_peptide.pdb", altCare = 1)
    new_pdb = PDB.PDB(model_dir + "modified_open_peptide.pdb")
    nb_BB, list_BB = sub_pdb.BBatmMiss()
    if nb_BB > 0:
        write_error_file(target_dir)
        sys.stderr.write("Error! Detected missing backbone atoms in your input PDB: %s\n" % list_BB)
        sys.exit(0)

    return new_pdb

def check_aa_constraints(targetAddr, aa_type_file, Loop_size):
    aa_pos = np.zeros(Loop_size, dtype='int32')
    aa_type_cons = open(targetAddr + aa_type_file , 'r').readlines()
    if len(aa_type_cons) != Loop_size:
        write_error_file(targetAddr)
        print("Error! There is no match between the linker size (%d) and amino acid constraints (%d)!" %(Loop_size, len(aa_type_cons)))
        sys.exit(0)
    for ll in range(len(aa_type_cons)):
        lines = (aa_type_cons[ll].split("\n")[0]).split(":")
        ## check if the line is empty or field with other characters
        if len(lines) <= 1 :
            write_error_file(targetAddr)
            print("Format Error! The amino acid constraints at line %d are wrong!" %(ll+1))
            sys.exit(0)
        linker_pos = int(lines[0])
        if aa_pos[linker_pos - 1] > 0:
            write_error_file(targetAddr)
            print("Format Error! The constraints for linker position (%d) are defiend more than once!"  %(linker_pos))
            sys.exit(0)
        aa_pos[linker_pos - 1] = 1
        if linker_pos <= 0 or linker_pos >10 :
            write_error_file(targetAddr)
            print("Format Error! The linker position (%d), is not within the specified linker size (%d)!" %(linker_pos, Loop_size))
            sys.exit(0)
        for jj in range(1,len(lines)):
            if lines[jj] not in AAs:
                write_error_file(targetAddr)
                print("Error! Please provide standard amino acids for the constraints! \"%s\" is not valid." %lines[jj])
                sys.exit(0)
    return

def linker_check_size_aa_constraints(targetAddr, aa_type_file):
    aa_type_cons = open(targetAddr + aa_type_file , 'r').readlines()
    Loop_size = len(aa_type_cons)
    aa_pos = np.zeros(Loop_size, dtype='int32')
    for ll in range(len(aa_type_cons)):
        lines = (aa_type_cons[ll].split("\n")[0]).split(":")
        ## check if the line is empty or field with other characters
        if len(lines) <= 1 :
            write_error_file(targetAddr)
            print("Format Error! The amino acid constraints at line %d are wrong!" %(ll+1))
            sys.exit(0)
        linker_pos = int(lines[0])
        if aa_pos[linker_pos - 1] > 0:
            write_error_file(targetAddr)
            print("Format Error! The constraints for linker position (%d) are defiend more than once!"  %(linker_pos))
            sys.exit(0)
        aa_pos[linker_pos - 1] = 1
        if linker_pos <= 0 or linker_pos >10 :
            write_error_file(targetAddr)
            print("Format Error! The linker position (%d), is not within the specified linker size (maximum 10 residues long)!" %(linker_pos))
            sys.exit(0)
        for jj in range(1,len(lines)):
            if lines[jj] not in AAs:
                write_error_file(targetAddr)
                print("Error! Please provide standard amino acids for the constraints! \"%s\" is not valid." %lines[jj])
                sys.exit(0)
    return Loop_size

def generate_aa_constraints(targetAddr, Loop_size):
    AA_file = open("%s/aa_const_pred.txt" %targetAddr, "w")
    for ll in range(Loop_size):
        AA_file.write("%d:A:G\n" %(ll+1))
    AA_file.close()
    return

def predict_linker_size(target_dir, work_path, modified_pdb, inp_linker_size, AA_constraint):
    ### if the constraint file is provided
    if AA_constraint is not None:
        if not os.path.exists(target_dir + AA_constraint):
            write_error_file(target_dir)
            print("Error the constraints file provided (%s) is wrong!" %AA_constraint)
            sys.exit(0)
        if inp_linker_size is not None:
            linker_size = inp_linker_size
            check_aa_constraints(target_dir, AA_constraint, linker_size)
        else:
            linker_size = linker_check_size_aa_constraints(target_dir, AA_constraint)
    ### if there is no constarint file
    else:
        if inp_linker_size is None:
            refe_pdb = PDB.PDB("%s/%s" %(work_path, modified_pdb))
            pep_len = len(refe_pdb)
            res0 = get_ca(refe_pdb[0:1])
            res1 = get_ca(refe_pdb[(pep_len-1):pep_len])
            dist = np.linalg.norm(res0-res1)
            linker_size = int((1.24 * dist) - 3.77)
        else:
            linker_size = inp_linker_size
        generate_aa_constraints(target_dir, linker_size)
    write_file = open("%s/linker_size_pred.txt" %target_dir, "w")
    write_file.write("%d\n" %int(linker_size))
    return linker_size

## prepare input files
def prep_files(target_dir, input_PDB_name, is_clean, work_dir, flank_size, inp_linker_size, linker_seq, AA_constraint):
    """
    Prepare  the input PDB: linker is between Cter and Nter, so PDB must be Cter + void + Nter, which requires PDB re-ordering. A PDB named \"GappedPDB.pdb\" is generated. flank1.pdb and flank2.pdb are also generated. They consists in only the flanks for the linker candidate search. 
  
    input_PDB_name: the name of the PDB to cyclize
    is_clean: is the cleaned version of the input_PDB already ready.
    work_dir: the working directory
    flank_size: the size of the flanks in use for candidate loop search
    inp_linker_size: the size of the linker to cyclize 
    linker_seq: the sequence of the linker 
    """

    ### read the first model, first chain, ignore water molecules and convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    if is_clean:
        GappedPDB = PDB.PDB(work_dir + "modified_open_peptide.pdb")
    else:
        input_PDB = PDB.PDB(input_PDB_name)
        GappedPDB = clean_input_pdb(work_dir, input_PDB, target_dir)

    ### predict minimum linker size, if the size is not given
    if linker_seq is None:
        linker_size = predict_linker_size(target_dir, work_dir, "modified_open_peptide.pdb", inp_linker_size, AA_constraint)
    else:
        linker_size = inp_linker_size

    gap_dir = mk_dir(work_dir + "Gap" + str(linker_size))
    # Split as two parts to fake loop
    ProtLen = len(GappedPDB)
    Divis = int(ProtLen/2)
    FirstHalf = GappedPDB[Divis:ProtLen]
    FirstHalf.out(work_dir + "FirstHalf.pdb")
    SecondHalf = GappedPDB[0:Divis]
    SecondHalf.out(work_dir + "SecondHalf.pdb")
    
    gaped_seq = GappedPDB.aaseq()
    ProtLen = len(GappedPDB)
    # Preparing the flanks
    flank1 = GappedPDB[(ProtLen - flank_size):ProtLen]
    flank2 = GappedPDB[0:flank_size]
    flank1.out(gap_dir + "flank1.pdb")
    flank2.out(gap_dir + "flank2.pdb")
    GappedPDB.out(gap_dir + "GappedPDB.pdb")

    half_len = int(ProtLen/2)
    if linker_seq is not None:
        ProtSeq = gaped_seq[half_len:ProtLen] + linker_seq + gaped_seq[0:half_len]
    else:
        ProtSeq = gaped_seq
    # PyPPP input fst
    PPP_name = "target_seq"
    fst_name = work_dir + PPP_name + ".fst"
    fst_file = open(fst_name, "w")
    fst_file.write(">Gap" + str(linker_size) + "\n")
    fst_file.write(ProtSeq + "\n")

    return
    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.
    @return: command line options and arguments (optionParser object)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail (absolute path is better)",
                        action="store", required=True, default = None)    

    parser.add_argument("--cyclize_ready", dest="cyclize_ready", help="Is the PDB already prepared for cyclization (Nter, Cter amino acids define loop)",
                        action="store_true", default = False)    

    parser.add_argument("--wpath", dest="work_path", help="Working directory (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)
    
    parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None)

    parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Loop size",
                        action="store", default = None) 

    parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
                        action="store", default=None)
    
    parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters (%s)" % DFLT_PDB_CLS_PATH,
                        action="store", default=DFLT_PDB_CLS_PATH) 

    parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
                        action="store", default=None) 
    return parser


def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()
    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path == None:
        options.work_path = os.getcwd() + "/"

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/" 
    #print( "%s\n" % print_options(options) )
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """
    options = argParse()
    linker_search_dir = mk_dir(options.work_path + WORKING_SUBDIR) # Impose sub tree

    if options.pdb_id != None:
        remove_homolog(options.pdb_cls_path, linker_search_dir, options.pdb_id)

    prep_files(options.work_path, options.target, options.cyclize_ready, linker_search_dir, options.flank_sze, options.linker_sze, options.linker_seq, options.aa_constraint_fname)

