#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence
cyclic_structure_guess_gap2_model1.pdb pdb1q5p-A-0-200-209 0.821039 0.310683 1.527722 PG
cyclic_structure_guess_gap2_model2.pdb pdb1jt8-A-1-27-36 0.809289 0.384702 2.475709 GA
cyclic_structure_guess_gap2_model3.pdb pdb1y8m-A-20-29-38 0.836907 2.281522 2.489819 GG
cyclic_structure_guess_gap2_model4.pdb pdb1zza-A-4-51-60 0.819231 2.168779 2.497587 DG
