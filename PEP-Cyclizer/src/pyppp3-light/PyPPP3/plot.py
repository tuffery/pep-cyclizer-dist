#!/usr/bin/env python3
import cairo
import sys
import math

def str2rgb(s):
    r = float(int(s[1:3], 16))/255
    g = float(int(s[3:5], 16))/255
    b = float(int(s[5:7], 16))/255
    return [r, g, b]

def plot_profile(probfile, sequence, title):
    COLUMN_WIDTH = 15
    WIDTH = COLUMN_WIDTH * len(sequence)
    HEIGHT = 400
    PADDING = 30

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
    context = cairo.Context(surface)

    alphabet = list("ABCDEFGHIJKLMNOPQRSTUVWXYZa")
    alpha = list("aAVWZBCDEOSRQIFUPHGYJKLMNXT")
    reordered = [alphabet.index(x) for x in alpha]
    
    colors_str = ["#FF0000", "#E20000", "#C40000", "#A80000", "#8B0000", "#0000CD",
                  "#0000D3", "#0000D9", "#0000DF", "#0000E6", "#0000EC", "#0000F2",
                  "#0000F8", "#0000FF", "#0000F8", "#0000F2", "#0000EC", "#0000E6",
                  "#0000DF", "#0000D9", "#0000D3", "#0000CD", "#006400", "#008A00",
                  "#00B100", "#00D800", "#00FF00"]

    colors = list(map(str2rgb, colors_str))

    f = open(probfile, 'r')
    tab = [[float(v) for v in line.strip().split()] for line in f]

    if len(tab) != len(sequence)-3:
        sys.stderr.write("the sequence (length %d) does not match the probability file (length %d)\n" % (len(sequence)-3,
                                                                                                         len(tab)))
        sys.exit(1)
    f.close()

    # white background
    context.set_source_rgb(1, 1, 1)
    context.rectangle(0, 0, WIDTH, HEIGHT)
    context.fill()

    # title
    context.select_font_face("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
    context.set_font_size(14)
    xbearing, ybearing, width, h, xadvance, yadvance  = context.text_extents(title)
    title_height = h+20
    context.set_source_rgb(0, 0, 0)
    context.move_to(WIDTH/2 - width + xbearing, h+10)
    context.show_text(title)
    context.set_font_size(10)

    x0 = 2*PADDING
    y0 = HEIGHT-PADDING
    unit_width = math.ceil((WIDTH - 2*PADDING) / len(tab))
    height = float(HEIGHT - 2*PADDING-title_height)

    # y-axis
    context.set_source_rgb(0, 0, 0)
    context.move_to(x0 - PADDING/2, y0)
    context.line_to(x0 - PADDING/2, y0 - height)
    context.stroke()
    for step in (0.0, 0.2, 0.4, 0.6, 0.8, 1.0):
        x = x0-PADDING/2
        y = y0 -step*height
        context.move_to(x, y)
        context.line_to(x-5, y)
        context.stroke()
        label = "%.1f" % step
        xbearing, ybearing, width, h, xadvance, yadvance  = context.text_extents(label)
        context.move_to(x - 10 - (width + xbearing), y+h+ybearing/2)
        context.show_text(label)

    # x-axis
    for r in range(len(tab)):
        label = sequence[r]
        xbearing, ybearing, width, h, xadvance, yadvance  = context.text_extents(label)
        x = x0 + r*unit_width + unit_width/2 - (width/2 + xbearing)
        y = y0 + 10
        context.move_to(x, y)
        context.show_text(label)
        y = y0 - height - 10
        context.move_to(x, y)
        context.show_text(label)

    # draw the boxes
    context.set_line_width(0.5)
    for r in range(len(tab)):
        row = tab[r]
        x = x0 + r*unit_width
        y = y0
        i = 0
        for c in reordered:
            col = row[c]
            red, green, blue = colors[i]
            context.set_source_rgb(red, green, blue)
            context.rectangle(x, y, unit_width, -height*col)
            context.fill()
            context.set_source_rgb(0, 0, 0)
            context.rectangle(x, y, unit_width, -height*col)
            context.stroke()
            y -= height*col
            i += 1

    surface.write_to_png("%s.svmi8.27.prob.probplot.27.png" % title)
