#
# R --slave --args [LIST FILE] [WORKING PATH] < doOneE1071.8.R
# 
verbose = 1

library(e1071)

set = "400"
set = "All.0"
# Biased
#ext = ".iter1.svmi8"
# Unbiased
ext = ".wght.svmi8"
# 24 letters model
#ext = ".hmm24-2.svmi8"
#mpath = "/nfs/joule/tuffery/Padilla2010/PEP-FOLD/lib/SVM/"
#mfile = paste(mpath,"LS-",set,ext,".model", sep = "")
mpath=Sys.getenv("SVMLIBPATH")
mfile = paste(mpath,"/LS-",set,ext,".model", sep = "")
if( verbose ){
  cat(" Will load the SVM Model:",mfile,"\n")
}
load(mfile)

# 
# Get file name of data to treat.
# 
args = commandArgs()
listfname = args[[4]]
wPath = args[[5]]

names = read.table(listfname)
t = as.character(as.vector(names[,1]))
# t = levels(names[,1])
if( verbose ){
  cat(t)
}

for (i in 1:length(t)) {

  fname = paste(wPath, "/", t[i], ".svmi8", sep="")

  s = sprintf( " %5d/%5d %s\n", i, length(t), fname)
  if( verbose ){
    cat(s)
  }

  OVSPred = paste(fname,".27.pred",sep = "")
  OVSProb = paste(fname,".27.prob",sep = "")

  # prediction
  TData=read.table(fname,skip=0)
  x=TData[,1:160]
  pred=predict(saFromAAmodel, x, prob = TRUE)
  Prob=attr(pred, "probabilities")
  # 27 letters model
  nn=as.character(0:26)
  # 24 letters model
  #nn=as.character(0:23)
  Prob=Prob[,nn]

  # sorties
  Out=cbind(x,as.double(as.vector(pred)))
  write.table(Out,file=OVSPred,quote=FALSE, col=FALSE, row=FALSE)
  write.table(Prob,file=OVSProb,quote=FALSE, col=FALSE, row=FALSE)

}
