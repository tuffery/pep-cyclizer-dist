overview

PEP-Cyclizer is a tool to assist the design of head-to-tail peptide cyclization, a well known strategy to enhance peptide resistance to enzymatic degradation and thus peptide bioavailability.

PEP-Cyclizer adresses two complementary features

The search for candidate sequences compatible with the cyclization of the peptide.
The generation of 3D models of a cyclic peptide starting from the 3D structure of the un-cylized peptide and the sequence of the cyclized peptide.

limitations

Un-cylized peptide size
Although cyclic peptides as short as 4 amino acids exist, PEP-Cyclizer paradigma is more oriented towards the cyclization of bioactive peptide formerly characterized, and consequently often peptides of larger sizes. In practice, PEP-Cyclizer will not start from un-cyclized peptides of size less than 8 amino-acids, due to the requirement to perform the PDB mining (see farther).

Amino acid types
PEP-Cyclizer only accepts un-cyclized peptides made of the 20 usual amino acids. It will not process un-cylized peptides with D- amino-acids, or unusual L- amino acids.
When guessing for candidate sequences, it will only propose sequences made of the 20 usual L- amino acids.
          

Peptide properties

It is possible to undergo the head-to-tail cyclization of peptides containing disulfde bonds. However PEP-Cylizer may not account correctly for other kinds of peptide internal cyclizations prior to head-to tail cyclization. For instance, the design of bicyclic peptides is out of the scope of PEP-Cyclizer.
          
PEP-Cyclizer requires the 3D structure of a peptide - the template, and will provide information on the linker, i.e. the additional amino-acids required to perform the head-to-tail cyclization.

Un-cyclized peptide structure
<p>This field is to specify the coordinates of the template peptide. The input file must be in PDB format. When multiple models are provided, only the first one will be considered. It must contain only the 20 standard amino acids. The size of the input template can be as long as 50 amino acids, but must be longer than 8 amino acids. Note that residue numbers will be changed for the ouptut, the first amino-acid will assigned number 1. 

Linker sequence
This field is to specify the sequence of a candidate linker. Its size may vary between 2 and 10 amino acids. Its sequence must be provided using the 1 letter amino acid code. Filling this field, PEP-Cyclizer will generate 3D models for the head-to-tail cyclized peptide.

Linker size
This field is to specify the size of a candidate linker. Its size may vary between 2 and 10 amino acids. Filling this field (default), PEP-Cyclizer will infer information from the PDB fragments of the specified linker size matching the head-to-tail closure condition.

Constraints for amino acid types at each position
It is often undesirable to perform head-to-tail cyclization with amino acids likely to interfere with peptide bioactivity. For this reason, only a limited subset of amino acids is most often used. This field makes possible to specify constraints on the amino acid types at each position. Information for each position in the linker sequence MUST be provided. the ':' symbol is used as a delimiter. 1:F:G:P indicates that at linker position 1, only PHE, GLY and PRO are preferred. 1: indicates that at linker position 1, no preference is specified, meaning all 20 standard amino acids are allowed. By default the form is filled using 1:A:G, i.e. indicating a preference for ALA and GLY.

