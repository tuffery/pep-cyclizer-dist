#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence
cyclic_structure_guess_gap7_model1.pdb pdb4kb4-A-0-23-37 0.904954 1.278707 0.881631 IRTGAAN
cyclic_structure_guess_gap7_model2.pdb pdb2rfq-A-0-258-272 0.826610 0.357612 0.914689 RAAFAGE
cyclic_structure_guess_gap7_model3.pdb pdb3on2-A-0-112-126 0.885479 1.172288 0.949436 HAPGAAS
cyclic_structure_guess_gap7_model4.pdb pdb1wp4-A-0-198-212 0.904295 0.712062 0.954132 ASSGRSN
cyclic_structure_guess_gap7_model5.pdb pdb4pxz-A-0-80-94 0.875929 0.738081 0.989303 DAKLGTG
cyclic_structure_guess_gap7_model6.pdb pdb2ong-A-0-226-240 0.898960 0.097278 0.992105 NEGGVDG
cyclic_structure_guess_gap7_model7.pdb pdb2onh-B-0-226-240 0.898926 0.095669 1.020218 NEGGVDG
cyclic_structure_guess_gap7_model8.pdb pdb2q1f-A-0-302-316 0.802150 0.127075 1.030347 NNSEAGS
cyclic_structure_guess_gap7_model9.pdb pdb4b4f-B-0-280-294 0.865506 1.036262 1.101557 GGNGGTE
cyclic_structure_guess_gap7_model10.pdb pdb1wqf-A-0-23-37 0.814895 0.320151 1.108946 IRTGRAN
cyclic_structure_guess_gap7_model11.pdb pdb4avn-A-0-280-294 0.864891 1.150169 1.118885 GGNGGTE
cyclic_structure_guess_gap7_model12.pdb pdb4kc6-A-0-23-37 0.849361 1.172274 1.122598 IRTGRAN
cyclic_structure_guess_gap7_model13.pdb pdb4b4f-A-0-280-294 0.866410 1.064522 1.123327 GGNGGTE
cyclic_structure_guess_gap7_model14.pdb pdb3oc5-A-0-119-133 0.868419 1.980742 1.125455 NEGKLST
cyclic_structure_guess_gap7_model15.pdb pdb1is1-A-0-23-37 0.825157 0.981346 1.159863 VRTGRAH
cyclic_structure_guess_gap7_model16.pdb pdb2mar-A-15-42-56 0.801807 0.519343 1.174425 TLGGDYK
cyclic_structure_guess_gap7_model17.pdb pdb1j5s-B-0-68-82 0.810631 1.789119 1.186914 ITGSRSN
cyclic_structure_guess_gap7_model18.pdb pdb2da7-A-19-57-71 0.872745 0.980074 1.202841 YSNSRSG
cyclic_structure_guess_gap7_model19.pdb pdb2onh-A-0-226-240 0.837207 0.744327 1.206723 NEGGVDG
cyclic_structure_guess_gap7_model20.pdb pdb2zr1-C-0-137-151 0.867954 1.084208 1.207691 RSGASDD
