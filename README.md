# This repository is for the generation of a docker image of PEP-Cyclizer (tested for Linux distributions)
#### The following instructions suppose you have docker installed on your machine.
#### You also need biopython installed in the environment (bash or conda) used to run PEP-Cyclizer (pip install biopython --user)

#### To get PEP-Cylizer work, you need to build 2 images:
  1. pep-cyclizer
  2. gromacs_py 

## 1. Pull the code from gitlab

```
git clone https://gitlab.rpbs.univ-paris-diderot.fr/tuffery/pep-cyclizer-dist.git
cd pep-cyclizer-dist
export ROOT_DIR=$PWD
```


## 2. Download the banks required for pyppp-light somewhere on your machine (best is ROOT_DIR/banks):

```
mkdir ${ROOT_DIR}/banks
cd ${ROOT_DIR}/banks
wget -O ppplib.tgz https://owncloud.rpbs.univ-paris-diderot.fr/owncloud/index.php/s/ne5uUmwxSkLIy8e/download
tar xzf ppplib.tgz
mv ppplib/* ${ROOT_DIR}/banks/
```

## 3. Download the banks corresponding to the PDB subset somewhere on your machine (e.g. ROOT_DIR/banks):

```
cd ${ROOT_DIR}/banks/
wget -O PDB_clusters.tgz https://owncloud.rpbs.univ-paris-diderot.fr/owncloud/index.php/s/0E9COlSi7Mip0Nb/download
wget -O pdbChainID.tgz https://owncloud.rpbs.univ-paris-diderot.fr/owncloud/index.php/s/p5NyxCCyQJ2LgDS/download
wget -O PyPPP_profiles.tgz https://owncloud.rpbs.univ-paris-diderot.fr/owncloud/index.php/s/Wf0UU59a5rmCHxl/download
tar xzf PDB_clusters.tgz
tar xzf pdbChainID.tgz
tar xzf PyPPP_profiles.tgz
```

## 4. Generate the docker image

```
cd ${ROOT_DIR}/PEP-Cyclizer
docker build -t pep-cyclizer .
cd ${ROOT_DIR}/gromacs_py
docker build -t gromacs:2020.4.gromacs-py .
```

## 5. Run the docker image (Biopython has to be locally installed prior to running PEP-Cyclizer docker)

```
# install biopython package
```

### Structure prediction

```
cd $ROOT_DIR/demo/structure_guess
export LOCAL_PATH_TO_BANKS=${ROOT_DIR}/banks
$ROOT_DIR/Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --linker_seq 1m2c_gap6.fasta --bank_local_path ${LOCAL_PATH_TO_BANKS} --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py
```

#### The expected results can be found in the $ROOT_DIR/demo/structure_guess/expected_results directory.

### Sequence prediction

```
cd $ROOT_DIR/demo/sequence_guess
export LOCAL_PATH_TO_BANKS=${ROOT_DIR}/banks
$ROOT_DIR/Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --aa_constraint_fname aa_cons.txt --bank_local_path ${LOCAL_PATH_TO_BANKS} --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py
```

#### The expected results can be found in the $ROOT_DIR/demo/sequence_guess/expected_results directory.

### Special case: sequence prediction with no linker size
#### If the linker size is not known, PEP-Cyclizer guesses the linker size based on the distance between the N-terminal and C-terminal residues of the open peptide. See the manuscript for more details.

```
cd $ROOT_DIR/demo/linker_size_guess
export LOCAL_PATH_TO_BANKS=${ROOT_DIR}/banks
$ROOT_DIR/Run-PEP-Cyclizer-Docker.py --run_docker --target uII-2.pdb --bank_local_path ${LOCAL_PATH_TO_BANKS} --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py
```

#### The expected results can be found in the $ROOT_DIR/demo/linker_size_guess/expected_results directory.
