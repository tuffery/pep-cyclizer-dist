#include "common.h"
#include "rmsd.h"
#include "RMSDSearch.h"
#include <stdio.h>
#include <memory.h>


int RMSDSearch (const Coord *atoms, int nr_atoms, //fragment to search
              float maxRMSD,
              const Coord *dbca, //CA database
              int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
              char seg_chain[], //(chain)
              int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
              RMSDSearchHit *hits, //must be pre-allocated with size maxhits
              int maxhits
            )
{
  Coord Xc[nr_atoms];
  const Coord *Y;
  Coord Yc[nr_atoms];
  center(atoms, nr_atoms, Xc);
  int pdb, seg;
  int nhits = 0;
  for (pdb = 0; pdb < nr_pdbindex; pdb++) {
    int seg_offset = pdb_index[pdb][0];
    int nsegs = pdb_index[pdb][1];
    for (seg = seg_offset; seg < seg_offset + nsegs; seg++) {
      int dbca_offset = seg_index[seg][0];
      int seglen = seg_index[seg][2];
      const Coord *dbca_seg = &dbca[dbca_offset-1];
      for (int n = 0; n < seglen - nr_atoms + 1; n++) {
        Y = dbca_seg + n;

        //Is the match any good?
        center(Y, nr_atoms, Yc);
        Real rms = -1;
        rmsd(Xc,Yc,0,0,nr_atoms, &rms);
        if (rms>maxRMSD) continue;

        if (nhits == maxhits) {
          fprintf(stderr, "MAXHITS reached!"); return maxhits;
        }

        RMSDSearchHit *hit = &hits[nhits];
        hit->pdbid = pdb;
        hit->segid = seg;
        hit->segment_offset = n;
        hit->rmsd = rms;
        nhits++;
      }
    }
  }
  return nhits;
}
