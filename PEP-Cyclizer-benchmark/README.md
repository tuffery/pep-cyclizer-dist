# In this directory you can find all the test cases that were studied in the PEP-Cyclizer paper.
## There are 4 sub-directories:
 1. linker_structure_prediction (PEP-Cyclizer structure guess)
 2. linker_sequence_design (PEP-Cyclizer sequence guess)
 3. Urotensin_II (uII test case with linker size of 3)
 4. Nrf2_1x2rB (NrF2 case with linker size of 3)


## For each case, there is an input folder containing the required files to run the corresponding prediction:
- pdb and sequence for the linker structure preditions
- pdb and amino acid constraint for the linker sequence predictions

## There is an output folder for each case, that represents the results that were obtained and reported in the paper:
- a summary file reporting the results
- The top 30 (in case of sequence prediction) or top 20 (in case of linker structure prediction) predicted models
- the logo for linker sequence prediction
- the likelyhood of the predicted linker sequences


#### For the linker structure prediction (linker_structure_prediction), the results are reported for all the NMR models of the input open peptide.


## Here are two examples on how to run PEP-Cyclizer on the benchamrk (given you had followed the installation steps provided in the main PEP-Cyclizer gitlab page):

### Structure prediction 1m2c with linker size 6 (GGAAGG)

```
export ROOT_DIR=$PWD
cd pep-cyclizer-dist/PEP-Cyclizer-benchmark/linker_structure_prediction/1m2c_GGAAGG/Model1/input
export LOCAL_PATH_TO_BANKS=${ROOT_DIR}/banks
$ROOT_DIR/Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --linker_seq 1m2c_gap6.fasta --bank_local_path ${LOCAL_PATH_TO_BANKS} --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py
```

The expected results can be found in the $ROOT_DIR/PEP-Cyclizer-benchmark/linker_structure_prediction/1m2c_GGAAGG/Model1/output directory.

### Sequence prediction

```
export ROOT_DIR=$PWD
cd $ROOT_DIR/PEP-Cyclizer-benchmark/linker_sequence_design/1m2c_GGAAGG/input
export LOCAL_PATH_TO_BANKS=${ROOT_DIR}/banks
$ROOT_DIR/Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --aa_constraint_fname aa_cons.txt --bank_local_path ${LOCAL_PATH_TO_BANKS} --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py
```

The expected results can be found in the $ROOT_DIR/PEP-Cyclizer-benchmark/linker_sequence_design/1m2c_GGAAGG/output directory.
