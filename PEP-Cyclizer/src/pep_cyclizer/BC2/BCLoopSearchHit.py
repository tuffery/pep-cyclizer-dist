import numpy as np
from . import Real
from .rmsd import get_rmsd, apply_matrix

missing = np.array((-999.0,-999.0, -999.0))

class BCLoopSearchHit(object):
    valid = None #some hits are not valid, e.g. they span between two chains.
    def __init__(self, library, flank1, looplength, flank2, hit0, clash_body_tree):
        self.pdbcode = library.pdb_index[hit0.pdbid][0]
        self.flank1 = flank1
        self.flank2 = flank2
        self.score = hit0.bc
        self.rmsd = hit0.rmsd
        self.chain = hit0.segment_chain.decode('UTF-8')
        self.model = hit0.segment_model
        self.rigidity = hit0.rigidity
        seg_index_line = self.seg_index_line = hit0.segid
        segment_offset = self.segment_offset = hit0.segment_offset
        seg = library.seg_index[hit0.segid]
        self.match_start = seg[1] + segment_offset
        seglength = seg[2]
        #seg_remainder: how much of the segment remains after the start of flank 1
        seg_remainder = seglength - segment_offset
        assert len(library.dbbb.shape) == 3, library.dbbb.shape #backbone library, not CA library
        seg_bb = library.dbbb[seg[0]-1:seg[0]-1+seglength]  ##TODO: -1 ("feature" of dbca
        seg_tmp = library.seq[seg[0]-1:seg[0]-1+seglength]  ##TODO: -1 ("feature" of dbca))
        seg_seq = np.array([i.decode('UTF-8') for i in seg_tmp])
        totlength = len(flank1) + looplength + len(flank2)
        self.bb = np.empty((totlength, 4, 3), dtype=Real)
        self.seq = np.empty(totlength, dtype=seg_seq.dtype)
        self.resnrs = np.empty(totlength, dtype="int")
        if seg_remainder >= totlength:
            self.gapped = False
            self.match_end = self.match_start + totlength - 1
            self.aln = "X" * looplength
            self.bb[:] = seg_bb[segment_offset:segment_offset + totlength]
            self.seq[:] = seg_seq[segment_offset:segment_offset + totlength]
            self.resnrs[:] = np.arange(totlength) + 1
        else:
            self.gapped = True
            assert seg_remainder > len(flank1)
            self.aln = "X" * (seg_remainder - len(flank1) )
            self.bb[:seg_remainder] = seg_bb[segment_offset:segment_offset + seg_remainder]
            self.seq[:seg_remainder] = seg_seq[segment_offset:segment_offset + seg_remainder]
            self.resnrs[:seg_remainder] = np.arange(seg_remainder) + 1
            pos = seg_remainder
            currseg = seg_index_line
            #tot_remainder: how much of the loop remains
            tot_remainder = totlength - seg_remainder - len(flank2)
            assert tot_remainder > 0
            seg_last_res = seg[1] + seg[2] - 1
            resnr_pos = seg_remainder + 1
            while tot_remainder > 0:
                currseg += 1
                seg2 = library.seg_index[currseg]
                #Detect if the segments are from different chains
                #We know this is the case if the numbering decreases
                seg2_first_res = seg2[1]
                if seg2_first_res <= seg_last_res:
                    self.valid = False
                    return
                skip = seg2[1] - seg_last_res - 1
                assert skip >= 0, (seg2[1], seg_last_res, seg, seg2, currseg)
                self.aln += "-" * skip
                resnr_pos += skip
                tot_remainder -= skip
                assert tot_remainder >= 0
                seg2length = seg2[2]
                seg_bb = library.dbbb[seg2[0]-1:seg2[0]-1+seg2length]  ##TODO: -1 ("feature" of dbca)
                seg_seq = library.seq[seg2[0]-1:seg2[0]-1+seg2length]  ##TODO: -1 ("feature" of dbca)
                read = min(seg2length, tot_remainder)
                self.aln += "X" * read
                self.bb[pos:pos+read] = seg_bb[:read]
                self.seq[pos:pos+read] = seg_seq[:read]
                self.resnrs[pos:pos+read] = np.arange(resnr_pos, resnr_pos+read)
                resnr_pos += read
                seg_last_res = seg2[1] + seg2[2] - 1
                self.match_end = seg2[1] + read
                tot_remainder -= read
                pos += read
            self.bb[pos:pos+len(flank2)] = seg_bb[read:read+len(flank2)]
            self.seq[pos:pos+len(flank2)] = seg_seq[read:read+len(flank2)]
            self.resnrs[pos:pos+len(flank2)] = np.arange(resnr_pos, resnr_pos+len(flank2))
            pos += len(flank2)
            self.bb = self.bb[:pos]
            self.seq = self.seq[:pos]
            self.resnrs = self.resnrs[:pos]
            #print self.pdbcode, self.aln, seg, segment_offset, len(flank1), len(flank2), pos
        flankmask = np.zeros(len(self.bb),dtype=bool)
        flankmask[:len(flank1)] = 1
        flankmask[-len(flank2):] = 1
        hit_flanks = self.bb[flankmask]
        query_flanks = np.concatenate((flank1, flank2))
        rotmat, offset, rmsd = get_rmsd(query_flanks, hit_flanks[:,1])
        self.rmsd = rmsd #TODO: compare with hit0.rmsd
        pivot = np.sum(hit_flanks,axis=0) / len(hit_flanks)
        missing_mask = np.all(self.bb == -999, axis=2)
        self.bb = apply_matrix(self.bb, pivot, rotmat, offset)
        if np.sum(missing_mask):
            self.bb[missing_mask] = missing
        loopmask = ~flankmask
        self.loop_bb = self.bb[loopmask]
        self.ca = self.bb[:,1,:]
        self.loop_ca = self.loop_bb[:,1,:]
        self.valid = True
    
        self.clashes = -1
        if clash_body_tree is not None:
            clashes = clash_body_tree.query_ball_point(self.loop_bb[:,1,:], 3) #hard-coded as 3 A in BCScore3 binary
            self.clashes = sum([len(l) for l in clashes])

    def pdb(self, flanks=True):
        AA1 = "ACDEFGHIKLMNPQRSTVWY"
        AA1seq = "ACDEFGHIKLMNPQRSTVWYXXXSXWMCXWYMDPECXXYAMMSCMAHHHZB"
        AA3 = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR","5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG", "HSE", "HSD","HSP","GLX","ASX"]
        AA3STRICT = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"]
        txt = ""
        tmpl = "ATOM   %4d  %s  %s  %4d    %8.3f%8.3f%8.3f  1.00  0.00\n"  #Yasaman
        atomnames = "N ", "CA", "C ", "O "
        bb = self.bb.reshape(len(self.bb)*4, 3)
        resnrs = self.resnrs
        for atomnr, atom in enumerate(bb):
            resindex = atomnr//4
            resnr = resnrs[resindex]
            if atom[0] != -999 or atom[1] != -999 or atom[2] != -999:
                AA_single = self.seq[resindex]
                AA1_ind = AA1seq.index(AA_single)  #Yasaman
                AA_Triple = AA3[AA1_ind]  #Yasaman
                #AA_Triple = dico_AA[AA_single]
                txt += tmpl % (atomnr+1, atomnames[atomnr%4], AA_Triple, resnr, atom[0], atom[1],atom[2])
        return txt
