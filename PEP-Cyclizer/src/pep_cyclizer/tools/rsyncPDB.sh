#Downloads the entire PDB in PDB format in ./pdb
rsync -rlpt -v -z --delete \
rsync.ebi.ac.uk::pub/databases/pdb/data/structures/divided/pdb/ \
./pdb
