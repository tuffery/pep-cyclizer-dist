#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
from rmsd import *
import argparse
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
DFLT_ROOT          = "/src/pep-cyclizer"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PDB_CLS_PATH  = DFLT_BANK + "/PDB_clusters/"
DFLT_PDB_PATH      = DFLT_BANK + "/pdbChainID/"
DFLT_HOME          = DFLT_ROOT + "/CyclicLoop/"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_MODE          = "machine"  #else "cluster"
PROT_DIR           = "pep_cyclizer_test"
PROT_DIR           = "pep_cyclizer_candidate_search"

DFLT_LBL           = "cyclic"
#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def find_clash(refe_pdb, loop_start, loop_sze, clash_th = 3.):
    """
    inter_CA based clash detection. Uses KDtree approach.
    refe_pdb  : the pdb to analyse (PyPDB instance)
    loop_start: the index of first loop residue in refe_pdb (from 0)
    loop_sze  : the size of the loop (number of residues)
    clash_th  : the cut-off distance to detect clashes.
    """
    # Clash_th = 3
    refe = PDB.PDB(refe_pdb)
    refe_ca_array = get_ca(refe)

    clash_body      = refe_ca_array
    clash_body_tree = None
    if clash_body is not None:
        clash_body_tree = KDTree(np.array(clash_body))

    loop_ca       = get_ca(refe[loop_start: (loop_start + loop_sze)])
    loop_ca_array = np.array(loop_ca)

    LoopRes = []
    for i in range(0,loop_sze):
        LoopRes.append(loop_start + i) # indices

    FinalClashes = 0
    if clash_body_tree is not None:
        clashes = clash_body_tree.query_ball_point(loop_ca_array, clash_th)
        for l in clashes:
          for x in l:
            if x not in LoopRes:
              FinalClashes = FinalClashes + 1

    return FinalClashes

## Scoring    
def Scoring(Loop_addr, looplength):

    outputdir = Loop_addr + "Candidates/"
    RMSDfile = Loop_addr + "top_models.list"   #"PyPPP_accepted.txt"
    sys.stderr.write("Scoring: Loop_addr: %s\n" % Loop_addr)
    f = open(RMSDfile,"r")
    RMSDvallines = f.readlines()
    f.close()
    # RMSDvallines = open(RMSDfile,"r").readlines()
    score_file = open(Loop_addr + "Final_scores.txt","w")
    score_file.write("#pdb chain model start end fBC fRigidity fRMSD LoopSeq\n")
    nbCandidates = len(RMSDvallines)
    score = np.zeros((nbCandidates,2))
    seq = []; nbb = 0
    for j in range(0,len(RMSDvallines)):
        candid = RMSDvallines[j].split()
        pdb_code = candid[0]
        pdb_chain = candid[1]
        pdb_model = int(candid[2])
        pdb_start = int(candid[3])
        pdb_end = int(candid[4])
        name = pdb_code + "-" + pdb_chain + "-" + str(pdb_model) + "-" + str(pdb_start) + "-" + str(pdb_end)
        if os.path.isfile(outputdir + "refe_" + name + "_mini.pdb"):
            ### check for clashes
            hit_pdb = PDB.PDB(outputdir + "refe_" + name + "_mini.pdb")
            loopStart = len(hit_pdb) - looplength  # for cyclization, loop always at the end.          
            is_clash = find_clash(hit_pdb, loopStart, looplength)
            if is_clash == 0:
                score[nbb,0] = float(candid[5])
                score[nbb,1] = float(candid[7])
                nbb = nbb + 1
                seq.append(candid[8])
                score_file.write(RMSDvallines[j])
    seq = np.array(seq)
    score_array = score[0:nbb,:]
    score_file.close()
    return score_array, seq

def run_Scoring(model_dir, number, loopSeq, out_label):
    if loopSeq is None:
       nameing_pre = out_label + "_sequence_guess_gap" + str(number)
       nbSeq = 30 #100  #30  #increasing number of sequences
    else:
       nameing_pre = out_label + "_structure_guess_gap" + str(number)
       nbSeq = 20

    gap_dir = model_dir + "Gap" + str(number) + "/"
    loop_dir = gap_dir + "BCSearch/"
    ################ Scoring (DOPE, BCscore, Flank RMSD) ####################
    scores, seq = Scoring(loop_dir, number)
    out_dir = targetAddr
    table_file = open(out_dir + nameing_pre + "_summary.txt", "w")
    table_file.write("#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence\n")
    if len(seq) < nbSeq:
       nbSeq = len(seq)
    FinalArgu = np.argsort(scores[:,1])[0:nbSeq]
    FinalSeq = seq[FinalArgu]
    ScoreLine = open(loop_dir + "Final_scores.txt","r").readlines()
    acc_seq_file = loop_dir + nameing_pre + ".fasta"
    acc_seq = open(acc_seq_file, "w")

    refe = PDB.PDB(model_dir + "modified_open_peptide.pdb")
    refe_bb = get_bb(refe)

    for j in range(0,len(FinalArgu)):
        model_name = nameing_pre + "_model" + str(j+1) + ".pdb"
        linee = FinalArgu[j] + 1 #ignore the first line
        candid = ScoreLine[linee].split()
        name = candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4]
        table_file.write("%s %s %s %s %s %s\n"%(model_name, name, candid[5], candid[6], candid[7], FinalSeq[j]))
        acc_seq.write(">%s\n" %(name))
        acc_seq.write("%s\n" %(FinalSeq[j]))    
        ### align the candidates to the initial open peptide
        HIT_PDB = PDB.PDB(loop_dir + "Candidates/refe_" + name + "_mini.pdb")
        candid_all = get_all(HIT_PDB)
        candid_sub = HIT_PDB[0:len(refe)]
        candid_sub_bb = get_bb(candid_sub)
        rotmat, offset, rmsdAll = fit(refe_bb, candid_sub_bb)
        pivot = np.sum(candid_sub_bb,axis=0) / len(candid_sub_bb)
        new_all = apply_matrix(candid_all, pivot, rotmat, offset) 
        candid_new_pdb = update_all(HIT_PDB, new_all) 
        candid_new_pdb.out(out_dir + model_name)    
    acc_seq.close()
    table_file.close()


def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    
    parser.add_argument("--run_docker", dest="run_docker", help="in docker mode (\"True\") or not (\"False\")",
                        action="store_true", default=False)  

    parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Loop size",
                        action="store", default=None)  
    
    parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)
    
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    #print(print_options(options))    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr = options.work_path
    protName   = options.target
    mode       = options.run_docker
    LoopSize   = options.linker_sze
    loopSeq    = options.linker_seq
    out_label  = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    outputdir = model_dir + "Gap" + str(LoopSize) + "/BCSearch/"
    RMSDfile = outputdir + "top_models.list"
    if os.path.exists(RMSDfile):
        run_Scoring(model_dir, LoopSize, loopSeq, out_label)


