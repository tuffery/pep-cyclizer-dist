/*
* Adaptation of BCAlign.c by Sjoerd de Vries/Frederic Guyon, MTi, 2016
*
* BCAlign author: Frederic Guyon, MTi
* Citations:
*      Fast protein fragment similarity scoring using a Binet-Cauchy Kernel, Bioinformatics, Frederic Guyon  and Pierre Tuffery,  doi:10.1093/bioinformatics/btt618
*
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "BC.h"
#include "rmsd.h"
#include "score3d.h"
#include "align3d.h"
#include "BCAlign.h"



int BCAlignSegment (
  const Coord *Xc,  //query coordinates, centered
  Index n1,
  const Coord *Y,  //search segment coordinates, uncentered
  Index n2,
  Index gap,
  Index al,
  Real minBC,
  Real minBC0,
  Real maxRMSD,
  int pdbid, //PDB identifier that will be added to all hits
  int segid, //segment identifier that will be added to all hits
  char *mode, // mode="all": all alignments, "one": one alignment per position (may be not the best)
  BCAlignHit *hits, //must be pre-allocated with size maxhits
  int maxhits,
  Index *alstr, //string to hold the aligments, must be pre-allocated with size max_alstr
  Score3dfunc score3d,
  // Modified:
  int *alstr_remaining // must be at least 2 * hit->alen, summed over for all hits
)
{
  Coord Yc[n2];
  Real Sc[n2];
  Index Xindex[n2], Scindex[n2];
  Coord cy;
  Index from, xfrom, yfrom, yfrom_prec, len;
  Index ind1[n1], ind2[n1];
  Real bc, bcnew,  bcopt;
  int nhits=0;
  Coord Xal[n1], Yal[n1];
  int i,j,k,kk,l;
  memset(Sc, -100, sizeof(Sc));
  memset(Xindex, 0, sizeof(Xindex));
  memset(Scindex, 0, sizeof(Scindex));
  //al=n1-3*gap;
  //if (al<=5) al=n1; // protection against stupid params
  for (from=0; from<n2-al+1; from++) {
    bcopt=-1;
    for (i=0; i<n1-al+1; i++) {
      bc=score3d(Xc,Y,i,from,al);
      if (bc>bcopt) {
        Sc[from]=bc; bcopt=bc;
        Xindex[from]=i;
      }
    }
  }

  len=maxSlidingWindow(Sc, n2, n1+2*gap, Scindex);
  bc=Sc[0]*al;
  yfrom_prec=-1;
  for (k=0; k<len; k++) {
    int alen = al;
    from=Scindex[k];
    xfrom=Xindex[from];
    if (Sc[from]>=minBC0) {
      for (i=0;i<al;i++) {ind1[i]=-1; ind2[i]=-1;}
      for (i=0;i<al;i++) {
        ind1[i]=xfrom+i;
        ind2[i]=xfrom+gap+i;
      }

      yfrom=from-xfrom-gap;
      if (yfrom<0) continue;
      if (yfrom + al > n2) continue;
      if (strcmp(mode, "all")!=0 && yfrom<=yfrom_prec) continue;
      int ysize = n1+2*gap;
      if (yfrom + ysize > n2) ysize = n2 - yfrom;
      yfrom_prec=yfrom;
      getCenter(Y+yfrom,al,&cy);
      for (j=0;j<3;j++)
        for (i=0; i<ysize;i++)
          Yc[i][j]=Y[yfrom+i][j]-cy[j];

      bcnew=align3d(Xc, n1, Yc, ysize, &alen, ind1, ind2, score3d);
      bcnew=bcnew*alen/n1;

      //BC score check
      if (bcnew<minBC) continue;

      //RMSD check
      Real rms = -1;
      if (maxRMSD > -1) {
        for (kk=0; kk<alen; kk++) {
          for (l=0;l<3;l++) Xal[kk][l]=Xc[ind1[kk]][l];
          for (l=0;l<3;l++) Yal[kk][l]=Yc[ind2[kk]][l];
        }
        rmsd(Xal,Yal,0,0,alen, &rms);
        if (rms>maxRMSD) continue;
      }

      //maxhits check
      if (nhits == maxhits) {
        fprintf(stderr, "MAXHITS reached!\n"); return maxhits;
      }
      if (*alstr_remaining < 2 * alen) {
        *alstr_remaining = 0;
        fprintf(stderr, "maximum alignment size reached!\n"); return nhits;
      }


      BCAlignHit *hit = &hits[nhits];
      hit->pdbid = pdbid;
      hit->segid = segid;
      hit->offset = yfrom;
      hit->ind = alstr;
      memcpy(alstr, ind1, alen*sizeof(Index) );
      memcpy(alstr+alen, ind2, alen*sizeof(Index) );
      alstr += 2*alen;
      *alstr_remaining -= 2* alen;
      hit->alen = alen;
      hit->bc = bcnew;
      hit->rmsd = rms;
      //printf("pdbid=%d, segid=%d, yfrom=%d, alen=%d, bcnew=%lf\n",pdbid, segid, yfrom, alen, bcnew);
      nhits++;
    }
  }
  return nhits;
}

int BCAlign (
  const Coord *atoms,
  Index nr_atoms, //query atoms
  Index gap,
  Index al,
  Real minBC,
  Real minBC0,
  Real maxRMSD, //maximum RMSD, or -1 for no RMSD check
  const Coord *dbca, //CA database
  int seg_index[][3], //(dbca offset, segment resnr, segment length)
  int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
  BCAlignHit *hits, //must be pre-allocated with size maxhits
  int maxhits,
  Index *alstr, //string to hold the aligments, must be pre-allocated with size max_alstr
  int max_alstr, // must be at least 2*hit.alen summed over for all hits
  char *score_type,
  char *mode
)
{
  int pdb, seg;
  int nhits = 0;
  int i,j;
  Coord Xc[nr_atoms];
  Coord cx;

  //fprintf(stderr, "score_type=%s\n",score_type);
  Score3dfunc score3d;
  if (strcmp(score_type,"BC1")==0) score3d=&BC1;
  else if (strcmp(score_type,"BC2")==0) score3d=&BC2;
  else if (strcmp(score_type,"BC3")==0) score3d=&BC3;
  else score3d=&BC3;

  getCenter(atoms,nr_atoms,&cx);
  for (j=0;j<3;j++)
  for (i=0; i<nr_atoms;i++)
  Xc[i][j]=atoms[i][j]-cx[j];

  int alstr_remaining = max_alstr;
  for (pdb = 0; pdb < nr_pdbindex; pdb++) {
    int seg_offset = pdb_index[pdb][0];
    int nsegs = pdb_index[pdb][1];
    for (seg = seg_offset; seg < seg_offset + nsegs; seg++) {
      int dbca_offset = seg_index[seg][0];
      int seglen = seg_index[seg][2];
      const Coord *dbca_seg = &dbca[dbca_offset-1];
      int maxhits_remaining = maxhits - nhits;
      Index *curr_alstr = alstr + max_alstr - alstr_remaining;
      int newhits = BCAlignSegment(
        Xc, nr_atoms, dbca_seg, seglen, gap, al, minBC, minBC0,
        maxRMSD, pdb, seg, mode, &hits[nhits], maxhits_remaining,
        curr_alstr, score3d, &alstr_remaining
      );
      if (newhits == maxhits_remaining) {
        return maxhits;
      }
      if (alstr_remaining == 0) {
        return newhits;
      }
      nhits += newhits;
    }
  }
  fprintf(stderr, "nhits=%d\n", nhits);
  return nhits;
}
