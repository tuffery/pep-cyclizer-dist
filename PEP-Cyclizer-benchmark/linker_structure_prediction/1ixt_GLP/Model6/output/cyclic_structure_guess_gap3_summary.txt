#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence
cyclic_structure_guess_gap3_model1.pdb pdb1tfs-A-13-5-15 0.922659 0.827730 1.169011 SLP
cyclic_structure_guess_gap3_model2.pdb pdb4n2z-A-0-191-201 0.894241 1.011190 1.182795 QFP
cyclic_structure_guess_gap3_model3.pdb pdb4gh7-D-0-1211-1221 0.881029 0.816017 1.317589 GQQ
cyclic_structure_guess_gap3_model4.pdb pdb3nfp-A-0-97-107 0.800644 0.954893 1.356473 GVF
cyclic_structure_guess_gap3_model5.pdb pdb2w80-A-0-393-403 0.811324 0.267622 1.441864 GYN
cyclic_structure_guess_gap3_model6.pdb pdb1mlb-B-0-97-107 0.853790 1.433639 1.446400 GNY
cyclic_structure_guess_gap3_model7.pdb pdb1k2p-B-0-463-473 0.929135 1.561538 1.638644 QRP
cyclic_structure_guess_gap3_model8.pdb pdb2lvz-A-14-114-124 0.814213 2.165379 1.672585 DSP
cyclic_structure_guess_gap3_model9.pdb pdb4fo2-M-0-82-92 0.823754 1.796653 1.676112 SSP
cyclic_structure_guess_gap3_model10.pdb pdb1j2o-A-8-76-86 0.868290 0.307423 1.689253 GEP
cyclic_structure_guess_gap3_model11.pdb pdb1evy-A-0-59-69 0.802095 2.213329 1.725037 FLK
cyclic_structure_guess_gap3_model12.pdb pdb1pfl-A-3-90-100 0.806403 2.818931 1.737339 GAP
cyclic_structure_guess_gap3_model13.pdb pdb3zew-A-0-682-692 0.812446 1.563616 1.740291 SMP
cyclic_structure_guess_gap3_model14.pdb pdb5het-A-0-329-339 0.800749 2.324817 1.742004 GEG
cyclic_structure_guess_gap3_model15.pdb pdb3pru-A-0-142-152 0.840665 2.326333 1.828077 NIV
cyclic_structure_guess_gap3_model16.pdb pdb1aoz-A-0-281-291 0.852914 2.771590 1.831568 RHP
cyclic_structure_guess_gap3_model17.pdb pdb2lti-A-19-1-11 0.808303 0.595766 1.860322 GVE
cyclic_structure_guess_gap3_model18.pdb pdb3afc-B-0-454-464 0.888389 0.692694 1.960749 GFL
cyclic_structure_guess_gap3_model19.pdb pdb1tiv-A-2-44-54 0.806246 1.006992 1.995254 GRK
cyclic_structure_guess_gap3_model20.pdb pdb3t3n-A-0-43-53 0.821477 2.393652 2.143683 GMP
