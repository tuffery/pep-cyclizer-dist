#include "score3d.h"
#include "BC.h"
#include "rmsd.h"

/*
 * for testing only
 */
Real BC1(const Coord *X, const Coord *Y, int from1, int from2, int len) {
  Coord Xc[len], Yc[len];
  Coord cx, cy;
  Real det, detX, detY;
  int i,k;

  getCenter(X+from1,len,&cx);
  getCenter(Y+from2,len,&cy);

  for (k=0; k<len;k++)
    for (i=0; i<3;i++) {
        Xc[k][i] = X[from1+k][i]-cx[i];
        Yc[k][i] = Y[from2+k][i]-cy[i];
    }

  /***/
  maxsp(Xc,Yc,0,0,len, &det, 1);
  detX=detY=0;
  for (k=0; k<len;k++)
    for (i=0; i<3; i++) {
       detX+=Xc[k][i]*Xc[k][i];
       detY+=Yc[k][i]*Yc[k][i];
    }

  return det/(sqrt(detX)*sqrt(detY));

}


/*
 * for testing only
 */
Real BC2(const Coord *X, const Coord *Y, int from1, int from2, int len) {
  Coord Xc[len], Yc[len];
  Real K[3][3];
  Coord cx, cy;
  Real det, detX, detY, sum, sp;
  int i,j,k,s;
  getCenter(X+from1,len,&cx);
  getCenter(Y+from2,len,&cy);

  for (k=0; k<len;k++)
    for (i=0; i<3;i++) {
        Xc[k][i] = X[from1+k][i]-cx[i];
        Yc[k][i] = Y[from2+k][i]-cy[i];
    }

  /***/
  memset(K, 0, sizeof(K));
  for (k=0; k<len;k++)
    for (i=0; i<3; i++)
      for (j=0; j<3;j++)
	K[i][j]+=Yc[k][i]*Xc[k][j];
  det=det3x3(K);
  s=((det>=0)?1:-1);

  maxsp(Xc,Yc,0,0,len, &sp, s);
  sum=0;
  for (i=0; i<3; i++)
    for (j=0; j<3;j++)
       sum+=K[i][j]*K[i][j];
  det=0.5*(sp*sp-sum);
  /***/
  memset(K, 0, sizeof(K));
  for (k=0; k<len;k++)
    for (i=0; i<3; i++)
      for (j=0; j<3;j++)
	K[i][j]+=Xc[k][i]*Xc[k][j];

  sp=0.;for (i=0; i<3; i++) sp+=K[i][i];
  sum=0;
  for (i=0; i<3; i++)
    for (j=0; j<3;j++)
       sum+=K[i][j]*K[i][j];
  detX=0.5*(sp*sp-sum);
  /***/
  memset(K, 0, sizeof(K));
  for (k=0; k<len;k++)
    for (i=0; i<3; i++)
      for (j=0; j<3;j++)
	      K[i][j]+=Yc[k][i]*Yc[k][j];

  sp=0.;for (i=0; i<3; i++) sp+=K[i][i];
  sum=0;
  for (i=0; i<3; i++)
    for (j=0; j<3;j++)
       sum+=K[i][j]*K[i][j];
  detY=0.5*(sp*sp-sum);
  return det/(sqrt(detX)*sqrt(detY));

}

Real BC3(const Coord *X, const Coord *Y, int from1, int from2, int len) {
  return BC(X+from1, Y+from2, len);
}
