c = {}
import os
for i in range(1,301):
    d =  "../../casp11/bc-benchmark/%d/" % i
    stats = d + "statistics.txt"
    with open(stats) as f:
        lines = list([l.rstrip("\n").split() for l in f])
        casp =  lines[1][-1]
        region = int(lines[4][-3]), int(lines[4][-1])
        tm = float(lines[7][-1])
        k = casp, region
        olds = []
        for kk in c:
            if kk == k:
                olds.append(k)
                continue
            if kk[0] != casp: continue
            if abs(kk[1][0] - region[0]) > 5: continue
            if abs(kk[1][1] - region[1]) > 5: continue
            olds.append(k)
        for old in olds:
            if old[1] > tm:
                break
        else:
            c[k] = (i, tm, d)

cc = c.items()
cc.sort(key=lambda x: x[0])

counts = {}
for k,v in cc:
    i, tm, d = v
    code = k[0]
    if code not in counts: counts[code] = 0
    r = counts[code]
    t = "../casp11/"+code+ "-" + str(r+1)
    counts[code] = r + 1
    cmd = "mkdir {1}; cp {0}/*.pdb {0}/statistics.txt {1}".format(d, t)
    os.system(cmd)
    
