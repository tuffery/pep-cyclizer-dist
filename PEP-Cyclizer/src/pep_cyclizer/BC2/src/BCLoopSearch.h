#include "common.h"
typedef struct {
  int pdbid; //PDB identifier of the database
  int segid; //segment identifier of the database
  Index segment_offset; //offset within segment
  char segment_chain;
  int segment_model;
  Real bc;
  Real rmsd;
  Real rigidity;
} BCLoopSearchHit;

int BCLoopSearch (const Coord *atoms1, int nr_atoms1, const Coord *atoms2, int nr_atoms2,  //flank1 and flank2
                  int looplength, //size of the gap/loop we are searching
                  int minloopmatch, int maxloopgap, //for partial matches: minimum total length, maximum gap
                  int mirror, //looking for mirrors?
                  float minBC, float maxR, float maxRMSD,  //minimum BC score, maximum rigidity, maximum RMSD
                  const Coord *dbca, //CA database
                  int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
                  char seg_chain[], //(chain)
                  int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
                  BCLoopSearchHit *hits, //must be pre-allocated with size maxhits
                  int maxhits
                );

const int NMAX = 5000;
