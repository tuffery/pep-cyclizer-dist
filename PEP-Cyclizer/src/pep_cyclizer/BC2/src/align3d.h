#include "common.h"
#include "score3d.h"

int maxSlidingWindow(Real Scores[], int n, int w, Index index[]);
Real align3d(const Coord *Xc, int n1, const Coord *Yc, int n2, int *alen,  Index *ind1, Index *ind2, Score3dfunc score3d);
