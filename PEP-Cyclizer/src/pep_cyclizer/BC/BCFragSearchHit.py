import numpy as np
from . import Real
from .rmsd import get_rmsd, apply_matrix

class FragSearchHit(object):
    def __init__(self, query, library, hit0, fit):
        fraglength = len(query)
        self.pdbcode = library.pdb_index[hit0.pdbid][0]
        self.rmsd = hit0.rmsd
        self.chain = hit0.segment_chain
        self.model = hit0.segment_model
        seg_index_line = self.seg_index_line = hit0.segid
        segment_offset = self.segment_offset = hit0.segment_offset
        seg = library.seg_index[hit0.segid]
        self.match_start = seg[1] + segment_offset
        assert len(library.dbbb.shape) == 3, library.dbbb.shape #backbone library, not CA library
        self.bb = np.empty((fraglength, 4, 3), dtype=Real)
        self.seq = np.empty(fraglength, dtype=library.seq.dtype)
        offset = seg[0]-1 + segment_offset
        self.bb[:] = library.dbbb[offset:offset+fraglength]  ##TODO: -1 ("feature" of dbca
        self.seq[:] = library.seq[offset:offset+fraglength]  ##TODO: -1 ("feature" of dbca))
        if fit or hit0.rmsd == -1:
            pivot = np.sum(self.bb,axis=0) / fraglength
            rotmat, offset, rmsd = get_rmsd(query, self.bb[:,1])
            if hit0.rmsd == -1:
                self.rmsd = rmsd #TODO: compare with hit0.rmsd        
            if fit:
                self.bb = apply_matrix(self.bb, pivot, rotmat, offset)
                self.ca = self.bb[:,1,:]

    def pdb(self):
        AA1 = "ACDEFGHIKLMNPQRSTVWY"
        AA1seq = "ACDEFGHIKLMNPQRSTVWYXXXSXWMCXWYMDPECXXYAMMSCMAHHH"
        AA3 = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR","5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG", "HSE", "HSD","HSP"]
        AA3STRICT = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"]
        txt = ""
        tmpl = "ATOM   %4d  %s  %s  %4d    %8.3f%8.3f%8.3f  1.00  0.00\n"  #Yasaman
        atomnames = "N ", "CA", "C ", "O "
        bb = self.bb.reshape(len(self.bb)*4, 3)
        for atomnr, atom in enumerate(bb):
            resindex = atomnr//4
            resnr = resindex + 1
            AA_single = self.seq[resindex]
            AA1_ind = AA1seq.index(AA_single)  #Yasaman
            AA_Triple = AA3[AA1_ind]  #Yasaman
            txt += tmpl % (atomnr+1, atomnames[atomnr%4], AA_Triple, resnr, atom[0], atom[1],atom[2])
        return txt


class BCFragSearchHit(FragSearchHit):
    def __init__(self, query, library, hit0, fit):
        FragSearchHit.__init__(self, query, library, hit0, fit)
        self.score = hit0.bc
        self.rigidity = hit0.rigidity
