"""
BCFragSearch, BCLoopSearch, BCAlign wrapper
Sjoerd de Vries, MTi, 2016

BCFragSearch/BCLoopSearch/BCAlign author: Frederic Guyon, MTi
 Citations:
      Fast protein fragment similarity scoring using a Binet-Cauchy Kernel, Bioinformatics, Frederic Guyon  and Pierre Tuffery,  doi:10.1093/bioinformatics/btt618
      BCSearch: fast structural fragment mining over large collections of protein structures. Nucleic Acids Res. 2015 Jul 1;43(W1):W378-82. doi: 10.1093/nar/gkv492. Epub 2015 May 14.
      Guyon F, Martz F, Vavrusa M, Becot J, Rey J, Tuffery P.
"""

import sys, os
import numpy as np
from scipy.spatial import cKDTree as KDTree
from .PDBLibrary import PDBLibraryInstance
from cffi import FFI
ffi = FFI()
import threading
import functools
import queue

d=os.path.abspath(os.path.split(__file__)[0])

def load_header(f):
    lines = [l for l in open(f) if not l.strip().startswith("#")]
    return "".join(lines)

headers = "common.h", "RMSDSearch.h", "BCFragSearch.h", "BCFragSearchFlank.h", "BCLoopSearch.h", "BCAlign.h"
header_txt = "\n".join([load_header(d + "/src/" + header) for header in headers])

ffi.cdef(header_txt)
RMSDSearchlib = ffi.dlopen(d+"/lib/RMSDSearch.so")
BCFragSearchlib = ffi.dlopen(d+"/lib/BCFragSearch.so")
BCFragSearchFlanklib = ffi.dlopen(d+"/lib/BCFragSearchFlank.so")
BCLoopSearchlib = ffi.dlopen(d+"/lib/BCLoopSearch.so")
BCAlignlib = ffi.dlopen(d+"/lib/BCAlign.so")

from .BCFragSearchHit import BCFragSearchHit, FragSearchHit
from .BCFragSearchFlankHit import BCFragSearchFlankHit, FragSearchFlankHit
from .BCLoopSearchHit import BCLoopSearchHit

def ptr(array):
    return array.__array_interface__["data"][0]

def multithreader(func, *args, **kwargs):
    kwargs2 = kwargs.copy()
    nthreads = kwargs2.pop("nthreads", None)
    if nthreads is None:
        return func(*args, **kwargs2)
    else:
        library = kwargs2.pop("library")
        threads = []
        i_queue = queue.Queue()
        for n in range(nthreads):
            dd = kwargs2.copy()
            lib = library.chunk(n, nthreads)
            dd["library"] = lib
            t = threading.Thread(
              target=lambda *args, **kwargs: i_queue.put(func(*args, **kwargs)),
              args=args,
              kwargs = dd
            )
            t.start()
            threads.append(t)
        for t in threads:
            t.join()
        result = []
        i_queue.put(None)
        for qq in iter(i_queue.get, None):
            result += qq
        return result

def multithread(func):
    return functools.partial(multithreader, func)

@multithread
def RMSDSearch(
  query,
  maxRMSD,
  library, maxhits, fit
):
    assert isinstance(library, PDBLibraryInstance)
    hits0 = ffi.new("RMSDSearchHit[]", maxhits)
    nrhits = RMSDSearchlib.RMSDSearch(
      ffi.cast("const Coord *", ptr(query)), len(query),
      maxRMSD,
      ffi.cast("const Coord *", ptr(library.dbca)),
      ffi.cast("int (*)[4]", ptr(library.seg_index_array)),
      ffi.cast("int (*)[2]", ptr(library.pdb_index_array)), len(library.pdb_index_array),
      hits0,
      maxhits
    )
    hits = []
    for n in range(nrhits):
        hit = FragSearchHit(query, library, hits0[n], fit)
        hits.append(hit)
    return hits

@multithread
def _BCFragSearch(
  query,
  mirror, minBC, maxR, maxRMSD,
  library, maxhits, fit
):
    assert isinstance(library, PDBLibraryInstance)
    hits0 = ffi.new("BCFragSearchHit[]", maxhits)
    nrhits = BCFragSearchlib.BCFragSearch(
      ffi.cast("const Coord *", ptr(query)), len(query),
      mirror, minBC, maxR, maxRMSD,
      ffi.cast("const Coord *", ptr(library.dbca)),
      ffi.cast("int (*)[4]", ptr(library.seg_index_array)),
      ffi.cast("char *", ptr(library.seg_chain_array)),
      ffi.cast("int (*)[2]", ptr(library.pdb_index_array)), len(library.pdb_index_array),
      hits0,
      maxhits
    )
    hits = []
    for n in range(nrhits):
        hit = BCFragSearchHit(query, library, hits0[n], fit)
        hits.append(hit)
    return hits

class BCFragSearch:
    query = None
    mirror = False
    minBC = None
    maxR = None #-1
    maxRMSD = -1
    library = None
    maxhits = None
    fit = False
    nthreads = None
    def run(self):
        self.fit = self.fit
        self.mirror = self.mirror
        self.maxRMSD = self.maxRMSD
        assert self.query is not None
        assert self.mirror in (True, False)
        assert self.minBC >= -1 and self.minBC <= 1
        assert self.maxR == -1 or self.maxR > 0
        assert self.maxRMSD == -1 or self.maxRMSD > 0
        assert self.library is not None
        assert self.maxhits > 0
        assert self.fit in (True, False), self.fit
        d = {k:v for k,v in list(self.__dict__.items()) if not k.startswith("_")}
        return _BCFragSearch(**d)

@multithread
def _BCFragSearchFlank(
  query,
  mirror, minBC, maxR, maxRMSD, maxHitRMSD,
  library, maxhits, fit
):
    assert isinstance(library, PDBLibraryInstance)
    hits0 = ffi.new("BCFragSearchHitFlank[]", maxhits)
    nrhits = BCFragSearchFlanklib.BCFragSearchFlank(
      ffi.cast("const Coord *", ptr(query)), len(query),
      mirror, minBC, maxR, maxRMSD, maxHitRMSD,
      ffi.cast("const Coord *", ptr(library.dbca)),
      ffi.cast("int (*)[4]", ptr(library.seg_index_array)),
      ffi.cast("char *", ptr(library.seg_chain_array)),
      ffi.cast("int (*)[2]", ptr(library.pdb_index_array)), len(library.pdb_index_array),
      hits0,
      maxhits
    )
    hits = []
    for n in range(nrhits):
        hit = BCFragSearchFlankHit(query, library, hits0[n], fit)
        hits.append(hit)
    return hits

class BCFragSearchFlank:
    query = None
    mirror = False
    minBC = None
    maxR = None #-1
    maxRMSD = -1
    maxHitRMSD = -1
    library = None
    maxhits = None
    fit = False
    nthreads = None
    def run(self):
        self.fit = self.fit
        self.mirror = self.mirror
        self.maxRMSD = self.maxRMSD
        self.maxHitRMSD = self.maxHitRMSD
        assert self.query is not None
        assert self.mirror in (True, False)
        assert self.minBC >= -1 and self.minBC <= 1
        assert self.maxR == -1 or self.maxR > 0
        assert self.maxRMSD == -1 or self.maxRMSD > 0
        assert self.maxHitRMSD == -1 or self.maxHitRMSD > 0
        assert self.library is not None
        assert self.maxhits > 0
        assert self.fit in (True, False), self.fit
        d = {k:v for k,v in list(self.__dict__.items()) if not k.startswith("_")}
        return _BCFragSearchFlank(**d)

@multithread
def _BCLoopSearch(
  flank1, flank2,
  looplength,  minloopmatch, maxloopgap,
  #for partial matches: at least minloopmatch loop residues must be matched, gaps inside the loop may be at most maxloopgap
  mirror, minBC, maxR, maxRMSD, library, clash_body, maxhits
):
    assert isinstance(library, PDBLibraryInstance)
    assert len(flank1) + len(flank2) <= BCLoopSearchlib.NMAX
    clash_body_tree = None
    if clash_body is not None:
        clash_body_tree = KDTree(np.array(clash_body))

    hits0 = ffi.new("BCLoopSearchHit[]", maxhits)
    nrhits = BCLoopSearchlib.BCLoopSearch(
      ffi.cast("const Coord *", ptr(flank1)), len(flank1),
      ffi.cast("const Coord *", ptr(flank2)), len(flank2),
      looplength, minloopmatch, maxloopgap, mirror, minBC, maxR, maxRMSD,
      ffi.cast("const Coord *", ptr(library.dbca)),
      ffi.cast("int (*)[4]", ptr(library.seg_index_array)),
      ffi.cast("char *", ptr(library.seg_chain_array)),
      ffi.cast("int (*)[2]", ptr(library.pdb_index_array)), len(library.pdb_index_array),
      hits0,
      maxhits
    )
    
    hits = []
    for n in range(nrhits):
        hit = BCLoopSearchHit(library, flank1, looplength, flank2, hits0[n], clash_body_tree)
        hits.append(hit)
    return hits
    

class BCLoopSearch:
    flank1 = None
    flank2 = None
    looplength = None
    minloopmatch = None
    maxloopgap = None
    mirror = False
    minBC = -1
    maxR = -1
    maxRMSD = -1
    library = None
    clash_body = None
    maxhits = None
    nthreads = None
    def run(self):
        self.mirror = self.mirror
        self.maxRMSD = self.maxRMSD
        self.clash_body = self.clash_body
        assert self.flank1 is not None
        assert self.flank2 is not None
        assert self.looplength >= 2
        assert self.minloopmatch <= self.looplength
        assert self.maxloopgap >= 0
        assert self.mirror in (True, False)
        assert self.minBC >= -1 and self.minBC <= 1
        assert self.maxR == -1 or self.maxR > 0
        assert self.maxRMSD == -1 or self.maxRMSD > 0
        assert self.library is not None
        assert self.maxhits > 0
        d = {k:v for k,v in list(self.__dict__.items()) if not k.startswith("_")}
        return _BCLoopSearch(**d)
