"""
Creates backbone database to reconstruct BCXSearch hits
A backbone is created for every residue that has a CA
Missing backbone atoms are filled in at -999, -999, -999

Input:
- Database name (DBname)
- List of PDB files

Output:
 - <DBname>.npy file containing all backbone data
    shape: (..., 4, 3)
    dtype: float32
 - <DBname>.segindex file
   One line per contiguous backbone segment
   Fields per line:
     First field: <start coordinate index> (in <DBname>.npy)
     Second field: <residue number of the first residue>
     Second field:  <segment length>
 - <DBname>.pdbindex file
   One line per PDB file
   Fields per line:
     First field: PDB code
     Second field: <start segment index>  (in <DBname>.segindex)
     Third field: <number of segments>
- <DBname>-SEQ.npy file containing all sequences (one letter code)
    shape: (...)
    dtype: |S1 (numpy one-byte dtype)

Sjoerd de Vries, MTi, 2016-2017
"""

bb_atoms = "N", "CA", "C", "O"

import sys, os
import numpy as np
from PyPDB import PyPDB
single_letter = PyPDB.dico_AA

dbname = sys.argv[1]
pdbfiles = sys.argv[2:]

segindexf = open(dbname + ".segindex", "w")
pdbindexf = open(dbname + ".pdbindex", "w")

coorlen = 0
resmax = 10000
coors = np.empty((resmax,4,3),dtype="float32")
seq = np.empty(resmax,dtype="|S1")

segindex = []
pdbindex = []

newcoors = np.empty((200000,4,3),dtype="float32")
newseq = np.empty(200000,dtype="|S1")
def process_pdb(pdblines):
    newcoors[:,:] = -999
    pdb = PyPDB.PDB(pdblines)
    newsegs = []
    start = coorlen
    pos = 0
    last_resnr = None
    for model in range(pdb.nModel):
        pdb.setModel(model + 1)
        for l in pdb:
            has_ca = False
            resnr = int(l[0].resNum())
            icode = l[0].icode()
            if icode.isalpha():
                continue
            code = single_letter.get(l.name, "X")
            newcoors0 = {}
            has_ca = False
            for ll in l:
                if ll.alt() not in (" ", "A"): continue
                if len(ll.icode().strip()): continue
                if l.name == "MSE" and ll.header().strip() in ("ATOM", "HETATM"):
                    code = "M"
                else:
                    if ll.header().strip() != "ATOM":
                        continue
                aname = ll.atmName().strip()
                if aname == "CA":
                    has_ca = True
                if aname in bb_atoms:
                    newcoors0[aname] = ll.xyz()
            if not has_ca: continue
            newseq[pos] = code
            for bb_atomnr, bb_atom in enumerate(bb_atoms):
                if bb_atom in newcoors0:
                    newcoors[pos,bb_atomnr] = newcoors0[bb_atom]
            pos += 1
            if resnr - 1 != last_resnr:
                if last_resnr is not None:
                    newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos))
                currseg_pos = pos
                currseg_resnr = resnr
            last_resnr = resnr
    if last_resnr is not None:
        newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos+1))


    return pos, newsegs

pdbcodes = set()
for pdbfile in pdbfiles:
    pdbcode = os.path.splitext(pdbfile)[0]
    assert pdbcode not in pdbcodes, pdbcode #no duplicate PDB codes
    pdblines = open(pdbfile).readlines()
    ncoors, newsegs = process_pdb(pdblines)
    nsegs = len(newsegs)
    if coorlen + ncoors > resmax:
        resmax = int(max(1.1*resmax, coorlen+ncoors))
        coors.resize((resmax, 4, 3))
        seq.resize(resmax)
    coors[coorlen:coorlen+ncoors] = newcoors[:ncoors]
    seq[coorlen:coorlen+ncoors] = newseq[:ncoors]
    coorlen += ncoors
    #print pdbcode, ncoors, nsegs, coorlen
    pdbindex.append((pdbcode, len(segindex), nsegs))
    segindex.extend(newsegs)

#write out indices
for index, resnr, nsegs in segindex:
    print >> segindexf, index, resnr, nsegs

for code, index, nsegs in pdbindex:
    print >> pdbindexf, code, index, nsegs

#dump coordinates
np.save(dbname, coors[:coorlen])
np.save(dbname+"-SEQ", seq[:coorlen])
