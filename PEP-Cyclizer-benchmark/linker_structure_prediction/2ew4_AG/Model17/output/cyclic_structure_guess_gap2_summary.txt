#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence
cyclic_structure_guess_gap2_model1.pdb pdb3s0z-A-0-64-73 0.908973 0.132147 1.036366 PG
cyclic_structure_guess_gap2_model2.pdb pdb1oa8-A-0-650-659 0.812387 0.142272 1.253360 FG
cyclic_structure_guess_gap2_model3.pdb pdb2fyj-A-4-26-35 0.921581 2.761404 1.358864 CG
cyclic_structure_guess_gap2_model4.pdb pdb1m0w-A-0-471-480 0.867624 0.376537 1.396873 EG
cyclic_structure_guess_gap2_model5.pdb pdb2fyj-A-8-26-35 0.891671 1.630062 1.513620 CG
cyclic_structure_guess_gap2_model6.pdb pdb1f9d-A-0-198-207 0.809629 0.885345 1.550417 FG
cyclic_structure_guess_gap2_model7.pdb pdb2fyj-A-14-26-35 0.831197 1.616805 1.709846 CG
cyclic_structure_guess_gap2_model8.pdb pdb2kom-A-15-478-487 0.815242 2.391351 1.727367 IG
cyclic_structure_guess_gap2_model9.pdb pdb1w9m-A-0-5-14 0.809781 2.908041 2.223641 TA
cyclic_structure_guess_gap2_model10.pdb pdb1v60-A-12-55-64 0.814605 0.651298 2.347915 AV
