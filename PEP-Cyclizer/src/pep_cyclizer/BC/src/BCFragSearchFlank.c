/*
 * Adaptation of BSSearch (routines in BCscore3_PT.c) by Sjoerd de Vries, MTi, 2016
 *
 * BCFragSearch/BCLoopSearch/BCAlign author: Frederic Guyon, MTi
 * Citations:
 *      Fast protein fragment similarity scoring using a Binet-Cauchy Kernel, Bioinformatics, Frederic Guyon  and Pierre Tuffery,  doi:10.1093/bioinformatics/btt618
 *
 *
 * gcc -shared -o BCLoopSearchlib.so BCLoopSearch.c -fPIC -O3 -lm ###TODO
*/

#include "common.h"
#include "BC.h"
#include "rigidity.h"
#include "rmsd.h"
#include "BCFragSearchFlank.h"
#include <stdio.h>
#include <memory.h>


int BCFragSearchFlank (const Coord *atoms, int nr_atoms, //fragment to search
              int mirror, //looking for mirrors?
              float minBC, float maxR, float maxRMSD, float maxHitRMSD,//minimum BC score, maximum rigidity, maximum RMSD
              const Coord *dbca, //CA database
              int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
              char seg_chain[], //(chain)
              int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
              BCFragSearchHitFlank *hits, //must be pre-allocated with size maxhits
              int maxhits
             )
{
  Coord Xc[nr_atoms];
  const Coord *Y;
  Coord Yc[nr_atoms];
  center(atoms, nr_atoms, Xc);
  Real detXX = selfBC(Xc, nr_atoms);
  //fprintf(stderr, "%f %f %f  %f %f %f   %f %f %f   %f %f %f ", Xc[0][0], Xc[0][1], Xc[0][2], Xc[1][0], Xc[1][1], Xc[1][2], Xc[2][0], Xc[2][1], Xc[2][2], Xc[3][0], Xc[3][1], Xc[3][2]);
  if (fabs(detXX)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",nr_atoms);return -1;
  }
  Real sqdetXX = sqrt(detXX);
  int pdb, seg;
  int nhits = 0;

  ////////////////////////////////////
  ////// hit loop coordinates
  int FlankLen = 4;
  int nrLoop = nr_atoms - (2*FlankLen);
  Coord HitLoop[nrLoop];
  Coord HitLoopC[nrLoop];
  for (int jj=0;jj<nrLoop;jj++) {
    for (int ii=0;ii<3;ii++) {
      HitLoop[jj][ii] = atoms[FlankLen + jj][ii];
    }
  }
  center(HitLoop,nrLoop,HitLoopC);
  Real detXXLoop = selfBC(HitLoopC, nrLoop);
  Real sqdetXXLoop = sqrt(detXXLoop);
  ////// hit flanks coordinates
  int nrFlank = FlankLen * 2; 
  int indd = 0;
  Coord HitFlank[nrFlank];
  Coord HitFlankC[nrFlank];
  for (int jj=0;jj<nrFlank;jj++) {
    if (jj<FlankLen) {indd = jj;
    }else {indd = nr_atoms - nrFlank + jj;}
    for (int ii=0;ii<3;ii++){
      HitFlank[jj][ii] = atoms[indd][ii];
    }
  }
  center(HitFlank,nrFlank,HitFlankC);
  Real detXXFlank = selfBC(HitFlankC, nrFlank);
  Real sqdetXXFlank = sqrt(detXXFlank);
  ////////////////////////////////////////

  for (pdb = 0; pdb < nr_pdbindex; pdb++) {
    int seg_offset = pdb_index[pdb][0];
    int nsegs = pdb_index[pdb][1];
    for (seg = seg_offset; seg < seg_offset + nsegs; seg++) {
      int dbca_offset = seg_index[seg][0];
      int seglen = seg_index[seg][2];
      int seg1_model = seg_index[seg][3];
      char seg1_chain = seg_chain[seg];
      const Coord *dbca_seg = &dbca[dbca_offset-1];
      for (int n = 0; n < seglen - nr_atoms + 1; n++) {
        Y = dbca_seg + n;

        //Is the match any good?
        center(Y, nr_atoms, Yc);
        float score0 = fastBC(Xc, Yc, nr_atoms, sqdetXX);
        float rigid = -1;
  
        if ((!mirror) && (score0 <minBC)) continue;
        if (mirror && (score0 >-minBC)) continue;
        rigid = rigidity(Xc,Yc,nr_atoms);
        if (maxR > -1) {
          if (rigid>maxR) continue;
        }
        
        //RMSD check
        Real rms = -1;
        if (maxRMSD > -1) {
          rmsd(Xc,Yc,0,0,nr_atoms, &rms);
          if (rms>maxRMSD) continue;
        }
        ///////////////////////////////////////////////////////////////////////
        // evaluate loop
        Coord CandidLoop[nrLoop];
        Coord CandidLoopC[nrLoop];
        for (int jj=0;jj<nrLoop;jj++) {
          for (int ii=0;ii<3;ii++) {
            CandidLoop[jj][ii] = Y[FlankLen + jj][ii];
          }
        }
        center(CandidLoop,nrLoop,CandidLoopC);
        float LoopScore0 = fastBC(HitLoopC, CandidLoopC, nrLoop, sqdetXXLoop);
        float LoopRigidity = rigidity(HitLoopC,CandidLoopC,nrLoop);
        //Hit RMSD check
        Real LoopRMS = -1; 
        if (maxHitRMSD > -1) {
          rmsd(HitLoopC,CandidLoopC,0,0,nrLoop, &LoopRMS);
          if (LoopRMS>maxHitRMSD) continue;
        }
        // evaluate Flanks
        Coord CandidFlank[nrFlank];
        Coord CandidFlankC[nrFlank];
        for (int jj=0;jj<nrFlank;jj++) {
          if (jj<FlankLen) {indd = jj;
          }else {indd = nr_atoms - nrFlank + jj;}
          for (int ii=0;ii<3;ii++){
            CandidFlank[jj][ii] = Y[indd][ii];
          }
        }
        center(CandidFlank,nrFlank,CandidFlankC);
        float FlankScore0 = fastBC(HitFlankC, CandidFlankC, nrFlank, sqdetXXFlank);
        float FlankRigidity = rigidity(HitFlankC,CandidFlankC,nrFlank);
        Real FlankRMS = -1;
        rmsd(HitFlankC,CandidFlankC,0,0,nrFlank, &FlankRMS);

        //FLANK RMSD check
        //if (FlankRMS>maxRMSD) continue;
        //if ((!mirror) && (FlankScore0 <minBC)) continue;
        //if (mirror && (FlankScore0 >-minBC)) continue;
        
        ///////////////////////////////////////////////////////////////////////  

        if (nhits == maxhits) {
          fprintf(stderr, "MAXHITS reached!"); return maxhits;
        }

        BCFragSearchHitFlank *hit = &hits[nhits];
        hit->pdbid = pdb;
        hit->segid = seg;
        hit->segment_chain = seg1_chain;
        hit->segment_model = seg1_model;
        hit->segment_offset = n;
        hit->bc = score0;
        hit->rigidity = rigid;
        hit->rmsd = rms;
        hit->looprmsd = LoopRMS;
        hit->flankrmsd = FlankRMS;
        hit->loopbc = LoopScore0;
        hit->looprigidity = LoopRigidity;
        hit->flankbc = FlankScore0;
        hit->flankrigidity = FlankRigidity;
        nhits++;
      }
    }
  }
  return nhits;
}
