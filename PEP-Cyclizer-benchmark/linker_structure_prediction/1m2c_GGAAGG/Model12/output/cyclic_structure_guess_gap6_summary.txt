#Name Candidate_id flank_BCscore flank_rigidity flank_RMSD Sequence
cyclic_structure_guess_gap6_model1.pdb pdb4kca-A-0-102-115 0.914620 0.149123 0.813484 NLAGSG
cyclic_structure_guess_gap6_model2.pdb pdb4ph0-A-0-98-111 0.928168 0.300288 0.858547 AQPNAG
cyclic_structure_guess_gap6_model3.pdb pdb1fs7-A-0-81-94 0.800899 0.813981 0.895468 VAWAGY
cyclic_structure_guess_gap6_model4.pdb pdb1pb5-A-1-10-23 0.931291 0.198220 0.913375 GNKVCS
cyclic_structure_guess_gap6_model5.pdb pdb3st8-A-0-459-472 0.929271 0.182544 0.917041 RKRPGS
cyclic_structure_guess_gap6_model6.pdb pdb5cwk-A-0-79-92 0.839330 1.295480 0.987498 REQPGS
cyclic_structure_guess_gap6_model7.pdb pdb1x8z-C-0-53-66 0.842668 0.318018 0.990692 DGGVDP
cyclic_structure_guess_gap6_model8.pdb pdb5lvc-a-0-78-91 0.882300 0.483193 1.016361 KGADVS
cyclic_structure_guess_gap6_model9.pdb pdb1bvr-E-0-200-213 0.885190 1.087694 1.017115 GGALGE
cyclic_structure_guess_gap6_model10.pdb pdb4x0j-A-0-179-192 0.851092 0.623415 1.031040 KGNGSA
cyclic_structure_guess_gap6_model11.pdb pdb1pyv-A-3-10-23 0.882630 1.486148 1.038930 AQRGGG
cyclic_structure_guess_gap6_model12.pdb pdb1bu7-A-0-76-89 0.879202 2.576180 1.043783 DFAGDG
cyclic_structure_guess_gap6_model13.pdb pdb5gka-A-0-78-91 0.935301 0.716201 1.051387 KGADVS
cyclic_structure_guess_gap6_model14.pdb pdb1r4t-A-12-153-166 0.826221 1.189632 1.081369 AGSQVE
cyclic_structure_guess_gap6_model15.pdb pdb4wjg-4-0-179-192 0.803763 0.173107 1.089148 KGNGSA
cyclic_structure_guess_gap6_model16.pdb pdb2qtl-A-0-401-414 0.809231 1.663970 1.089534 CSKQGA
cyclic_structure_guess_gap6_model17.pdb pdb1pyv-A-10-10-23 0.941860 1.964035 1.093258 AQRGGG
cyclic_structure_guess_gap6_model18.pdb pdb5cje-A-0-164-177 0.846576 0.173845 1.103085 VAPTGG
cyclic_structure_guess_gap6_model19.pdb pdb3qi8-B-0-76-89 0.866692 2.676447 1.123116 DFAGDG
cyclic_structure_guess_gap6_model20.pdb pdb2igp-A-0-165-178 0.847255 0.561643 1.131408 MYTVGA
