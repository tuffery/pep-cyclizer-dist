"""
Creates backbone database to reconstruct BCXSearch hits
A backbone is created for every residue that has a CA
Missing backbone atoms are filled in at -999, -999, -999

Input: Database dir and database name
Database must be in the HHsuite format, for example the pdb70 database
  this database can be downloaded at http://wwwuser.gwdg.de/~compbiol/data/hhsuite/databases/hhsuite_dbs/
Database dir must contain
  - HHsearch .ffindex file (<DATABASENAME>_pdb.ffindex)
  - HHsearch .ffdata file (<DATABASENAME>_pdb.ffdata)

Output:
 - dbca.npy file containing all backbone data
    shape: (..., 4, 3)
    dtype: float32
 - dbca.segindex file
   One line per contiguous backbone segment
   Fields per line:
     First field: <start coordinate index> (in dbca.npy)
     Second field: <residue number of the first residue>
     Second field:  <segment length>
 - dbca.pdbindex file
   One line per PDB file
   Fields per line:
     First field: PDB code
     Second field: <start segment index>  (in dbca.segindex)
     Third field: <number of segments>
- <DBname>-SEQ.npy file containing all sequences (one letter code)
    shape: (...)
    dtype: |S1 (numpy one-byte dtype)

Sjoerd de Vries, MTi, 2016-2017
"""

bb_atoms = "N", "CA", "C", "O"

import sys, os
import numpy as np
from PyPDB import PyPDB

hhsuite_database_dir = sys.argv[1]
hhsuite_database_name = sys.argv[2]
outputdir = sys.argv[3]

segindexf = open(outputdir + "/dbca.segindex", "w")
pdbindexf = open(outputdir + "/dbca.pdbindex", "w")

coorlen = 0
resmax = 10000
coors = np.empty((resmax,4,3),dtype="float32")
seq = np.empty(resmax,dtype="|S1")

segindex = []
pdbindex = []

#Read PDB index
pdb_ffindexfile = os.path.join(hhsuite_database_dir, hhsuite_database_name) + "_pdb.ffindex"
pdb_ffdatafile = os.path.join(hhsuite_database_dir, hhsuite_database_name) + "_pdb.ffdata"
for f in (pdb_ffindexfile, pdb_ffdatafile):
    if not os.path.exists(f):
        raise Exception("Cannot locate HHsuite database file %s" % f)

newcoors = np.empty((200000,4,3),dtype="float32")
newseq = np.empty(200000,dtype="|S1")
def process_pdb(pdblines):
    newcoors[:,:] = -999
    pdb = PyPDB.PDB(pdblines)
    newsegs = []
    start = coorlen
    pos = 0
    last_resnr = None
    for model in range(pdb.nModel):
        pdb.setModel(model + 1)
        for l in pdb:
            has_ca = False
            resnr = int(l[0].resNum())
            icode = l[0].icode()
            if icode.isalpha():
                continue
            newcoors0 = {}
            has_ca = False
            for ll in l:
                if ll.alt() not in (" ", "A"): continue
                if ll.header().strip() != "ATOM": continue
                aname = ll.atmName().strip()
                if aname == "CA":
                    has_ca = True
                if aname in bb_atoms:
                    newcoors0[aname] = ll.xyz()
            if not has_ca: continue
            newseq[pos] = code
            for bb_atomnr, bb_atom in enumerate(bb_atoms):
                if bb_atom in newcoors0:
                    newcoors[pos,bb_atomnr] = newcoors0[bb_atom]
            pos += 1
            if resnr - 1 != last_resnr:
                if last_resnr is not None:
                    newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos))
                currseg_pos = pos
                currseg_resnr = resnr
            last_resnr = resnr
    if last_resnr is not None:
        newsegs.append((start+currseg_pos, currseg_resnr, pos-currseg_pos+1))


    return pos, newsegs

pdb_ffdatahandle = open(pdb_ffdatafile, "rb")
for l in open(pdb_ffindexfile):
    lp = len(pdbindex)
    ll = l.split()
    pdbcode = ll[0]
    #if pdbcode != "1ffk_Y": continue
    pdbpos, pdblen = int(ll[1]), int(ll[2])
    if pdblen in (0,1): continue
    pdb_ffdatahandle.seek(pdbpos)
    pdblines = pdb_ffdatahandle.read(pdblen-1).splitlines()
    ncoors, newsegs = process_pdb(pdblines)
    nsegs = len(newsegs)
    if coorlen + ncoors > resmax:
        resmax = int(max(1.1*resmax, coorlen+ncoors))
        coors.resize((resmax, 4, 3))
        seq.resize((resmax, 4, 3))
    coors[coorlen:coorlen+ncoors] = newcoors[:ncoors]
    seq[coorlen:coorlen+ncoors] = newseq[:ncoors]
    coorlen += ncoors
    #print pdbcode, ncoors, nsegs, coorlen
    pdbindex.append((pdbcode, len(segindex), nsegs))
    segindex.extend(newsegs)
    if lp > 0 and lp % 1000 == 0:
        print >> sys.stderr,  "%d PDBs processed" % lp


#write out indices
for index, resnr, nsegs in segindex:
    print >> segindexf, index, resnr, nsegs

for code, index, nsegs in pdbindex:
    print >> pdbindexf, code, index, nsegs

#dump coordinates
np.save(outputdir + "/dbca", coors[:coorlen])
np.save(outputdir + "/dbca-SEQ", seq[:coorlen])
