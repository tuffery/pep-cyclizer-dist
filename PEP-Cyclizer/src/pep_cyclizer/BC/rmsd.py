import numpy as np

def get_rmsd(atoms1, atoms2):
    l = len(atoms1)
    com1, com2 = np.sum(atoms1,axis=0)/l, np.sum(atoms2,axis=0)/l
    atoms1 = atoms1 - com1
    atoms2 = atoms2 - com2
    E0 = np.sum(atoms1*atoms1) + np.sum(atoms2*atoms2)
    V, S, Wt = np.linalg.svd( np.dot( np.transpose(atoms1), atoms2))
    reflect =  np.linalg.det(V) * np.linalg.det(Wt)
    if reflect == -1.0:
        S[-1] = -S[-1]
        V[:,-1] = -V[:,-1]
    U = V.dot(Wt).transpose()
    sd = abs(E0 - (2.0 * sum(S)))
    rmsd = np.sqrt(sd / l)
    return U, com1-com2, rmsd

def apply_matrix(atoms, pivot, rotmat, trans):
    ret = []
    for atom in atoms:
        a = atom-pivot
        atom2 = a.dot(rotmat) + pivot + trans
        ret.append(atom2)
    return np.array(ret)
