
import os
from .Config import *
import argparse


def mk_dir(name_dir):
	if not(os.path.exists(name_dir)):
		os.mkdir(name_dir)
	if not name_dir.endswith("/"):
		name_dir += '/'
	return name_dir

def print_options(options):
	"""
	@return: a string describing the options setup for the run
	"""
	try:
		rs = ""
		for option in list(options.__dict__.keys()):
			rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
		return rs
	except:
		return ""

def parse_args(print_opts = False):
	
	parser = argparse.ArgumentParser()

	parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
						action="store", default = None)

	parser.add_argument("--cyclize_ready", dest="cyclize_ready", help="Is the PDB already prepared for cyclization (Nter, Cter amino acids define loop)",
						action="store_true", default = False)    

	parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
						action="store", default=DFLT_PDB_ID)
	
	parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
						action="store", default=DFLT_WORK_PATH)
	
	parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
						action="store", default=DFLT_NPROC)
	
	parser.add_argument("--nt", dest="nthreads", type = int, help="number of threads (%d)" % DFLT_NTHREADS,
						action="store", default=DFLT_NTHREADS)
	
	# parser.add_argument("--loop_sze", dest="loop_sze", type = int, help="loop size",
	# 					action="store", default=None)
	
	parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Linker size",
						action="store", default=None)  
 
	# parser.add_argument("--loop_seq", dest="loop_seq", help="fasta file with the sequence of the missing loop",
	# 					action="store", default="none")

	parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
						action="store", default=None)
	
	parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum) (%f)" % DFLT_BC_MIN,
						action="store", default=DFLT_BC_MIN)

	parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
						action="store", default=DFLT_RIGIDITY_MAX)
	
	parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
						action="store", default=DFLT_FLANK_SZE)
	
	parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
						action="store", default=DFLT_RMSD_MAX)

	parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
						action="store", default=None) 

	parser.add_argument("--out_nseq", dest="out_nseq", help="The maximal number of sequences drawn/estimated using fbt",
						action="store", type=int, default=OUT_NSEQ) 

	parser.add_argument("--max_tries", dest="max_tries", help="The maximal number of failed backtracking",
						action="store", type=int, default=2000) 

	parser.add_argument("--cter_aa", dest="cter_aa", help="cter amino acid of the linker (corresponds to Nter amino acid of open peptide)",
						action="store", default=None) 
	
	parser.add_argument("--use_flanking_aas", dest="use_flanking_aas", help="use known N/Cter amino acids of the open peptide to constrain likely sequence generation",
						action="store_true", default=False) 
	
	parser.add_argument("--nter_aa", dest="nter_aa", help="nter amino acid of the linker (corresponds to Cter amino acid of open peptide)",
						action="store", default=None) 

	parser.add_argument("--run_mode", dest="run_mode", help="running mode (\"cluster\" or \"machine\")(%s)" % DFLT_MODE,
						action="store", default=DFLT_MODE) 

	parser.add_argument("--bank_local_path", dest="bank_local_path", help="path to data banks (%s)" %DFLT_BANK_ROOT_PATH,
						action="store", default=DFLT_BANK_ROOT_PATH)

	parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters (%s)" % DFLT_PDB_CLS_PATH,
						action="store", default=DFLT_PDB_CLS_PATH)
	
	parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB (%s)" % DFLT_PDB_PATH,
						action="store", default=DFLT_PDB_PATH)
	
	parser.add_argument("--pdb_subset", dest="pdb_subset", help="name of indexed PDB (%s)" % DFLT_PDB_SUBSET,
						action="store", default=DFLT_PDB_SUBSET)
	
	parser.add_argument("--ppp_path", dest="ppp_path", help="path to ppp3 (%s)" % DFLT_HOME_PPP,
						action="store", default=DFLT_HOME_PPP) 

	parser.add_argument("--prof_path", dest="prof_path", help="path to profiles (%s)" % DFLT_PROF_PATH,
						action="store", default=DFLT_PROF_PATH)

	parser.add_argument("--demo", dest="demo", help="selecting the demo mode for either \"Modeling the linker sequence\" or \"Modeling the linker conformation\"",
						action="store", default=None)

	parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
						action="store", default=DFLT_LBL)
	parser.add_argument("--tm_ref_w", dest="tm_ref_w", help="Dirichlet like weight for reference transition matrix (%f)" % TM_REF_WEIGHT,
						action="store", type=float, default=TM_REF_WEIGHT)

	parser.add_argument("--tm_obs_w", dest="tm_obs_w", help="Dirichlet like weight for observed transition matrix (%f)" % TM_OBS_WEIGHT,
						action="store", type=float, default=TM_OBS_WEIGHT)

	parser.add_argument("--min_pseudo_freq", dest="min_pseudo_freq", help="Minimal observed frequency for constrained amino acids (%f)" % PSEUDO_FREQ,
						action="store", type=float, default=PSEUDO_FREQ)

	parser.add_argument("--use_sized_bank", dest="use_sized_bank", help="use databank size dependent transitions and frequencies",
						action="store_true", default=True) #False) Y.K. using the size dependent bank

	options = parser.parse_args() # Do not pass sys.argv

	if options.work_path[-1] != "/":
		options.work_path = options.work_path + "/"
	if options.pdb_cls_path[-1] != "/":
		options.pdb_cls_path = options.pdb_cls_path + "/"
	if options.pdb_path[-1] != "/":
		options.pdb_path = options.pdb_path + "/"
				
	if print_opts:
		print(print_options(options))
	return options
