/*
 * Adaptation of BSSearch (routines in BCscore3_PT.c) by Sjoerd de Vries, MTi, 2016
 *
 * BCFragSearch/BCLoopSearch/BCAlign author: Frederic Guyon, MTi
 * Citations:
 *      Fast protein fragment similarity scoring using a Binet-Cauchy Kernel, Bioinformatics, Frederic Guyon  and Pierre Tuffery,  doi:10.1093/bioinformatics/btt618
 *
 *
 * gcc -shared -o BCLoopSearchlib.so BCLoopSearch.c -fPIC -O3 -lm ###TODO
*/

#include "common.h"
#include "BC.h"
#include "rigidity.h"
#include "rmsd.h"
#include "BCFragSearch.h"
#include <stdio.h>
#include <memory.h>


int BCFragSearch (const Coord *atoms, int nr_atoms, //fragment to search
              int mirror, //looking for mirrors?
              float minBC, float maxR, float maxRMSD,//minimum BC score, maximum rigidity, maximum RMSD
              const Coord *dbca, //CA database
              int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
              char seg_chain[], //(chain)
              int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
              BCFragSearchHit *hits, //must be pre-allocated with size maxhits
              int maxhits
             )

{
  Coord Xc[nr_atoms];
  const Coord *Y;
  Coord Yc[nr_atoms];
  center(atoms, nr_atoms, Xc);
  Real detXX = selfBC(Xc, nr_atoms);
  if (fabs(detXX)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",nr_atoms);return -1;
  }
  Real sqdetXX = sqrt(detXX);
  int pdb, seg;
  int nhits = 0;
  for (pdb = 0; pdb < nr_pdbindex; pdb++) {
    int seg_offset = pdb_index[pdb][0];
    int nsegs = pdb_index[pdb][1];
    for (seg = seg_offset; seg < seg_offset + nsegs; seg++) {
      int dbca_offset = seg_index[seg][0];
      int seglen = seg_index[seg][2];
      int seg1_model = seg_index[seg][3];
      char seg1_chain = seg_chain[seg];
      const Coord *dbca_seg = &dbca[dbca_offset-1];
      for (int n = 0; n < seglen - nr_atoms + 1; n++) {
        Y = dbca_seg + n;

        //Is the match any good?
        center(Y, nr_atoms, Yc);
        float score0 = fastBC(Xc, Yc, nr_atoms, sqdetXX);
        float rigid = -1;
  
        if ((!mirror) && (score0 <minBC)) continue;
        if (mirror && (score0 >-minBC)) continue;
        rigid = rigidity(Xc,Yc,nr_atoms);
        if (maxR > -1) {
          if (rigid>maxR) continue;
        }
        
        //RMSD check
        Real rms = -1;
        if (maxRMSD > -1) {
          rmsd(Xc,Yc,0,0,nr_atoms, &rms);
          if (rms>maxRMSD) continue;
        }

        if (nhits == maxhits) {
          fprintf(stderr, "MAXHITS reached!"); return maxhits;
        }

        BCFragSearchHit *hit = &hits[nhits];
        hit->pdbid = pdb;
        hit->segid = seg;
        hit->segment_chain = seg1_chain;
        hit->segment_model = seg1_model;
        hit->segment_offset = n;
        hit->bc = score0;
        hit->rigidity = rigid;
        hit->rmsd = rms;
        nhits++;
      }
    }
  }
  return nhits;
}
