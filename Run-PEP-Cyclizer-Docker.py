#!/usr/bin/env python3

"""
author: Yasaman Karami
25 July 2023

Requires: 
	PyPDB3 (PyPDB compatible with python3).
		git clone https://gitlab.rpbs.univ-paris-diderot.fr/src/PyPDB3.git
                python3 setup.py install --user
	Fasta3 (Fasta compatible with python3)
                git clone https://gitlab.rpbs.univ-paris-diderot.fr/src/Fasta3.git
                python3 setup.py install --user
        gromacs_py (for easy minimisation).
                git clone https://github.com/samuelmurail/gromacs_py.git
                follow instructions
        oscar-star (for side chain positioning)

Example running docker:

cd deme

########################### DOCKER MODE ###################################
### Structure prediction
Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --linker_seq 1m2c_gap6.fasta --bank_local_path LOCAL_PATH_TO_BANKS --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py

### Sequence prediction
Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --linker_sze 6 --aa_constraint_fname aa_cons.txt --bank_local_path LOCAL_PATH_TO_BANKS --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py

### Sequence prediction with no linker size
Run-PEP-Cyclizer-Docker.py --run_docker --target 1m2c_m1.pdb --bank_local_path LOCAL_PATH_TO_BANKS --pep_docker_name pep-cyclizer --gro_docker_name gromacs:2020.4.gromacs-py

########################### LOCAL MODE ###################################
### Structure prediction
Run-PEP-Cyclizer-Docker.py --target 1m2c_m1.pdb --linker_sze 6 --linker_seq 1m2c_gap6.fasta --bank_local_path LOCAL_PATH_TO_BANKS --gromacs_path PATH_TO_gromacs_py --oscar_path PATH_TO_oscar --pep_cyclizer_path PATH_TO_PEP_CYCLIZER

### Sequence prediction
Run-PEP-Cyclizer-Docker.py --target 1m2c_m1.pdb --linker_sze 6 --aa_constraint_fname aa_cons.txt --bank_local_path LOCAL_PATH_TO_BANKS --gromacs_path PATH_TO_gromacs_py --oscar_path PATH_TO_oscar--pep_cyclizer_path PATH_TO_PEP_CYCLIZER

### Sequence prediction with no linker size
Run-PEP-Cyclizer-Docker.py --target 1m2c_m1.pdb --bank_local_path LOCAL_PATH_TO_BANKS --gromacs_path PATH_TO_gromacs_py --oscar_path PATH_TO_oscar --pep_cyclizer_path PATH_TO_PEP_CYCLIZER

"""

###########################
###  L I B R A R I E S  ###
###########################
import os
import sys
import argparse
import numpy as np
from Bio import SeqIO
import multiprocessing as mp
from optparse import OptionParser

############################################################################
############################################################################
AAs = "ARNDCEQGHILKMFPSTWYV"

# The working path
DFLT_WORK_PATH     = "./"                             # The directory to work in, must contain the target pdb 
WORKING_SUBDIR     = "pep_cyclizer_candidate_search"  # The name of the subdirectory with step by step results
DFLT_PDB_ID        = None                             # The PDB id of a homolog of the target (to remove the homologs)
DFLT_LBL           = "cyclic"                         # the prefix label for the results
DFLT_PDB_SUBSET    = "pdb70"

# BC search default parameters
DFLT_BC_MIN        = 0.8
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.

# parallelism
DFLT_NPROC         = 8
DFLT_NTHREADS      = 8
MAX_JOB            = 300
CLUSTER            = False

# Weights for Dirichlet applied to transition matrix.
TM_REF_WEIGHT = 1.0
TM_OBS_WEIGHT = 0.0

# Weights for Dirichlet applied to amino acid frequencies.
FR_REF_WEIGHT = 0.
FR_OBS_WEIGHT = 1.

# Minimal frequency assigned to constrained amino acids 
PSEUDO_FREQ = 0.8

# Maximal number of sequences drawn (not considering redundancy)
OUT_NSEQ = 150
############################################################################
############################################################################
def gro_machine(gromacs_path, model, name):
	command = "%s/gromacs_py/minimize_pdb_and_cyclic.py -f %s -n %s -dir . -keep -cyclic" %(gromacs_path, model, name)
	print("\n",command)
	os.system(command)
	return

def mk_dir(name_dir):
	if not(os.path.exists(name_dir)):
		os.mkdir(name_dir)
	if not name_dir.endswith("/"):
		name_dir += '/'
	return name_dir

def print_options(options):
	"""
	@return: a string describing the options setup for the run
	"""
	try:
		rs = ""
		for option in list(options.__dict__.keys()):
			rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
		return rs
	except:
		return ""

def parse_args(print_opts = False):
	
	parser = argparse.ArgumentParser()

	parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
						action="store", required = True, default = None)

	parser.add_argument("--bank_local_path", dest="bank_local_path", help="path to data banks",
						action="store", required = True, default=None) 

	parser.add_argument("--run_docker", dest="run_docker", help="in docker mode (\"True\") or not (\"False\")",
						action="store_true", default=False)

	parser.add_argument("--cyclize_ready", dest="cyclize_ready", help="Is the PDB already prepared for cyclization (Nter, Cter amino acids define loop)",
						action="store_true", default = False)    

	parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
						action="store", default=DFLT_PDB_ID)
	
	parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
						action="store", default=DFLT_WORK_PATH)
	
	parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
						action="store", default=DFLT_NPROC)
	
	parser.add_argument("--nt", dest="nthreads", type = int, help="number of threads (%d)" % DFLT_NTHREADS,
						action="store", default=DFLT_NTHREADS)
	
	parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Linker size",
						action="store", default=None)  
 
	parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
						action="store", default=None)
	
	parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum) (%f)" % DFLT_BC_MIN,
						action="store", default=DFLT_BC_MIN)

	parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
						action="store", default=DFLT_RIGIDITY_MAX)
	
	parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
						action="store", default=DFLT_FLANK_SZE)
	
	parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
						action="store", default=DFLT_RMSD_MAX)

	parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
						action="store", default=None) 

	parser.add_argument("--out_nseq", dest="out_nseq", help="The maximal number of sequences drawn/estimated using fbt",
						action="store", type=int, default=OUT_NSEQ) 

	parser.add_argument("--max_tries", dest="max_tries", help="The maximal number of failed backtracking",
						action="store", type=int, default=2000) 

	parser.add_argument("--cter_aa", dest="cter_aa", help="cter amino acid of the linker (corresponds to Nter amino acid of open peptide)",
						action="store", default=None) 
	
	parser.add_argument("--use_flanking_aas", dest="use_flanking_aas", help="use known N/Cter amino acids of the open peptide to constrain likely sequence generation",
						action="store_true", default=False) 
	
	parser.add_argument("--nter_aa", dest="nter_aa", help="nter amino acid of the linker (corresponds to Cter amino acid of open peptide)",
						action="store", default=None)	

	parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters",
						action="store", default=None)

	parser.add_argument("--bank_path", dest="bank_path", help="path to the directory where all the data banks are stored (blast_db, pdb70, pdbChainID, PDB_clusters, PyPPP_profiles, SVM)",
						action="store", default=None)
	
	parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB",
						action="store", default=None)
	
	parser.add_argument("--pdb_subset", dest="pdb_subset", help="name of indexed PDB (%s)" % DFLT_PDB_SUBSET,
						action="store", default=DFLT_PDB_SUBSET)

	parser.add_argument("--prof_path", dest="prof_path", help="path to profiles",
						action="store", default=None)

	parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
						action="store", default=DFLT_LBL)
	parser.add_argument("--tm_ref_w", dest="tm_ref_w", help="Dirichlet like weight for reference transition matrix (%f)" % TM_REF_WEIGHT,
						action="store", type=float, default=TM_REF_WEIGHT)

	parser.add_argument("--tm_obs_w", dest="tm_obs_w", help="Dirichlet like weight for observed transition matrix (%f)" % TM_OBS_WEIGHT,
						action="store", type=float, default=TM_OBS_WEIGHT)

	parser.add_argument("--min_pseudo_freq", dest="min_pseudo_freq", help="Minimal observed frequency for constrained amino acids (%f)" % PSEUDO_FREQ,
						action="store", type=float, default=PSEUDO_FREQ)

	parser.add_argument("--use_sized_bank", dest="use_sized_bank", help="use databank size dependent transitions and frequencies",
						action="store_true", default=True) #False) Y.K. using the size dependent bank

	#### If docker mode, these arguments are mandatory
	parser.add_argument("--pep_docker_name", dest="pep_docker_name", help="name of PEP-Cyclizer docker image",
						action="store", default=None)

	parser.add_argument("--gro_docker_name", dest="gro_docker_name", help="name of gromacs_py docker image",
						action="store", default=None)

	#### If local mode, these arguments are mandatory

	parser.add_argument("--oscar_path", dest="oscar_path", help="path to Oscar",
						action="store", default=None) 

	parser.add_argument("--pep_cyclizer_path", dest="pep_cyclizer_path", help="path to the main directory of PEP-Cyclizer",
						action="store", default=None) 

	parser.add_argument("--gromacs_path", dest="gromacs_path", help="path to gromacs_py",
						action="store", default=None) 

	options = parser.parse_args() # Do not pass sys.argv

	if print_opts:
		print(print_options(options))
	return options

if __name__=="__main__":
        
	options = parse_args(False)

	if options.run_docker:
		print("\n\n!!! You are using the Docker mode to run PEP-Cyclizer !!!!\n\n")
		if options.pep_docker_name is None:
			print("Error! Please specify the PEP-Cyclzier docker image name (--pep_docker_name is missing)!\n")
			sys.exit(0)
		if options.gro_docker_name is None:
			print("Error! Please specify the gromacs_py docker image name (--gro_docker_name is missing)!\n")
			sys.exit(0)

		DFLT_BANK_ROOT_PATH  = "/scratch/banks"
		DFLT_HOME            = "/usr/local/pep_cyclizer/"          					# The root directory for all the banks
		gro_docker_name      = options.gro_docker_name    #"gromacs:2020.4.gromacs-py"
		Docker_image         = options.pep_docker_name    #"pep-cyclizer"                   				# Name of the docker image
		run_docker_cmd       = "docker run -it --rm -v $(pwd):$(pwd) -v %s:%s -w $(pwd)" %(options.bank_local_path, DFLT_BANK_ROOT_PATH)
		gro_docker_cmd       = "docker run -it --rm -v $(pwd):$(pwd) -w $(pwd)"
	else:
		print("\n\n!!! You are running PEP-Cyclizer locally without the docker image !!!!\n")
		if options.oscar_path is None:
			print("Error! Please specify the path to Oscar directory (--oscar_path is missing)!\n")
			sys.exit(0)
		if options.gromacs_path is None:
			print("Error! Please specify the path to the gromacs_py directory (--gromacs_path is missing)!\n")
			sys.exit(0)
		if options.pep_cyclizer_path is None:
			print("Error! Please specify the path to the main PEP-Cyclizer directory (--pep_cyclizer_path is missing)!\n")
			sys.exit(0)

		DFLT_BANK_ROOT_PATH  = options.bank_local_path
		DFLT_HOME 	     = "%s/src/pep_cyclizer/" %options.pep_cyclizer_path
		gromacs_path         = options.gromacs_path
		MULUL 		     = "%s/src/OSCAR/library/z/" %options.oscar_path
		OSCAR_STAR_PATH      = "%s/bin" %options.oscar_path
	
	DFLT_PDB_CLS_PATH   = DFLT_BANK_ROOT_PATH + "/PDB_clusters/"
	DFLT_PDB_PATH       = DFLT_BANK_ROOT_PATH + "/pdbChainID/"
	DFLT_PROF_PATH      = DFLT_BANK_ROOT_PATH + "/PyPPP_profiles/"

	
	if options.work_path[-1] != "/":
		options.work_path = options.work_path + "/"
	if options.work_path == "./":
		options.work_path = os.getcwd() + "/"
	if options.pdb_cls_path is None:
		options.pdb_cls_path = DFLT_PDB_CLS_PATH
	if options.pdb_path is None:
		options.pdb_path = DFLT_PDB_PATH
	if options.prof_path is None:
		options.prof_path = DFLT_PROF_PATH
				
	model_dir = mk_dir(options.work_path + WORKING_SUBDIR)

	### input linker sequence
	LoopSeqFileAddr = options.work_path + "____"
	if options.linker_seq is not None:
		LoopSeqFileAddr = options.work_path + options.linker_seq

	if os.path.isfile(LoopSeqFileAddr) and os.path.getsize(LoopSeqFileAddr) > 0 :
		count = 0
		fasta_sequences = SeqIO.parse(open(LoopSeqFileAddr),'fasta')
		for seq_record in fasta_sequences:
			count += 1
			LoopSeq = seq_record.seq
		if count > 1:
			print("Error! Please specify only one sequence in your fasta file!\n")
			sys.exit(0)
		options.linker_sze = len(LoopSeq)
		for ii in range(options.linker_sze):
			if LoopSeq[ii] not in AAs:
				print("Error! The linker sequence MUST contain only standard amino acids! Found %s.\n" % LoopSeq[ii] )
				sys.exit(0)
		options.rmsd_max = 2.5
		options.pdb_subset = "pdbChainID"
		nb_tot = 8
		nameing_pre = options.Lbl + "_structure_guess_gap"
		name_vis = "structure_guess"
	else:
		if options.linker_sze is None:
			if options.aa_constraint_fname is not None:
				print("WARNING! The linker size will be assigned based on the sequence constraint file provided!\n")
			else:
				print("WARNING! When no linker size is passed, the size will be predicted based on the distance between the N-terminus and C-terminus.\n\nThe sequence constraints (A and G) will also be assigned automatically.\n")
		LoopSeq = None
		options.rmsd_max = 3
		nb_tot = 10
		nameing_pre = options.Lbl + "_sequence_guess_gap"
		name_vis = "sequence_guess"

	print(print_options(options))
	print("\n")

	if options.run_docker:
		######################################################################
		'''
		USING DOCKER IMAGE
		'''
		######################################################################
		##### Prep Inputs and Remove Homologs and Propose Linker Size (if Needed) ####################
		print("1/%d: Verifying input files" %nb_tot)
		### check the input pdb and prepare all the files
		cmd = DFLT_HOME + "Prep_input.py"
		args = "--wpath %s --target %s --pdb_id %s --cls_path %s" %(options.work_path, options.target, options.pdb_id, options.pdb_cls_path)
		if options.linker_sze is not None:
			args += " --linker_sze %s" %str(options.linker_sze)
		if LoopSeq is not None:
			args += " --linker_seq %s" %LoopSeq
		if options.aa_constraint_fname is not None:
			args += " --aa_constraint_fname %s " %options.aa_constraint_fname
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))

		if os.path.exists("%s/error.log" %options.work_path):
			sys.exit(0)
		if LoopSeq is None:
			if not os.path.exists("%s/linker_size_pred.txt" %options.work_path):
				print("Error! Please make sure all the arguments are set correctly!")
				sys.exit(0)
			options.linker_sze = int(open("%s/linker_size_pred.txt" %options.work_path, "r").readlines()[0].split()[0])
			if  options.aa_constraint_fname is None:
				options.aa_constraint_fname = "aa_const_pred.txt"
		##### BCLoopSearch ###################################################
		print("2/%d: BCLoopSearch and clustering" %nb_tot)
		cmd = DFLT_HOME + "LoopSearch.py"
		args = "--wpath %s --target %s --pdb_path %s --pdb_subset %s --bc_min %s --rigidity %s --rmsd_max %s --run_docker --linker_sze %s " %(options.work_path, options.target, options.pdb_path, options.pdb_subset, str(options.bc_min), str(options.rigidity), str(options.rmsd_max), options.linker_sze)
		if LoopSeq is not None:
			args += " --linker_seq %s " %LoopSeq
		### checking amino acid constraints
		if options.aa_constraint_fname is not None:
			args += " --aa_constraint_fname %s " %options.aa_constraint_fname
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		### check if there was any hit found or not
		BCLoopRes = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/BCLoopCluster_Final_BC.txt"
		if not os.path.isfile(BCLoopRes) or os.path.getsize(BCLoopRes) <= 0 :
			print("Error! No candidate was found for your input file!")
			sys.exit(0)
		else:
			BCLoopResFile = open(BCLoopRes,'r').readlines()
			print("Found %d hits (size %d)\n" %(len(BCLoopResFile),options.linker_sze))
		##### PyPPP Prep ####################################################
		#verifying the existance of conformational profile for the target
		#Otherwise we call PyPPP to measure the profile
		print("3/%d: Conformational profiles" %nb_tot)
		PPP_name = "target_seq"
		fst_name = PPP_name + ".fst"
		os.chdir(model_dir)
		cmd = "PyPPP3Exec"
		args = "-s %s -l %s --2012 --noPlot" %(fst_name, PPP_name)
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		os.chdir(options.work_path)
		##### Measuring JSD ###################################################		
		print("4/%d: Measuring Jenson Shannon distances" %nb_tot)
		cmd = DFLT_HOME + "PyPPP_score.py"
		args = "--ppp_path %s --wpath %s --linker_sze %s --target %s --flank_sze %s --run_docker" %(options.prof_path, options.work_path, str(options.linker_sze), options.target, str(options.flank_sze))
		if LoopSeq is not None:
			args += " --linker_seq %s" %LoopSeq
		out_dir = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/"
		BCLoopRes = open(out_dir + "BCLoopCluster_Final_BC.txt", "r").readlines()
		nbCandid = min(len(BCLoopRes),MAX_JOB)
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		##### Candid Prep ####################################################
		print("5/%d: Preparing the candidates" %nb_tot)
		cmd = DFLT_HOME + "Prep_candidates.py"
		args = "--wpath %s --target %s --linker_sze %s " %(options.work_path, options.target, str(options.linker_sze))
		if LoopSeq is not None:
			args += " --linker_seq %s" %LoopSeq
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		### check if there was any hit found or not
		TopModels = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/top_models.list"
		if not os.path.isfile(TopModels) or os.path.getsize(TopModels) <= 0 :
			print("Error! No candidate was found for your input file!")
			sys.exit(0)
		##### Oscar-Star #####################################################
		print("6/%d: positioning linker side chains" %nb_tot)
		cmd = "oscarstar"
		out_dir = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/Candidates/"
		pdb_data = out_dir + "data"
		pdb_read = open(pdb_data, 'r').readlines()
		nbCandid = len(pdb_read)
		if nbCandid <= 1:
			print("Error! No candidate was found!")
			sys.exit(0)
		os.chdir(out_dir)
		args = ""
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		#### gromacs ########################################################
		os.chdir(options.work_path)
		print("7/%d: minimization" %nb_tot)
		cmd = "minimize_pdb_and_cyclic.py"
		out_dir = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/"
		gromacs_dir = out_dir + 'Candidates/'
		input_list = out_dir + "input_gromacs.list"
		name_list = out_dir + "gromacs_names.list"
		nbCandid = len(open(name_list, 'r').readlines())
		if nbCandid <= 1:
			candidate("No candidate was found!")
			sys.exit(0)
		os.chdir(gromacs_dir)
		input_list_read = open(input_list, "r").readlines()
		name_list_read = open(name_list, "r").readlines()
		pool = mp.Pool(1)
		for i in range(nbCandid):
			model = input_list_read[i].split("\n")[0]
			name = name_list_read[i].split("\n")[0]
			args = "-f %s -n %s -dir . -keep -cyclic" %(model, name)
			pool.apply_async(os.system("%s %s %s %s" %(gro_docker_cmd, gro_docker_name, cmd, args)))
		pool.close()
		pool.join()
		##### Scoring #######################################################
		os.chdir(options.work_path)
		print("8/%d: Scoring the candidates" %nb_tot)
		cmd = DFLT_HOME + "scoring.py"
		args = "--wpath %s --target %s --l %s --linker_sze %s " %(options.work_path, options.target, options.Lbl, str(options.linker_sze))
		if LoopSeq is not None:
			args += "--linker_seq %s" %LoopSeq
		os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		summary_file = options.work_path + nameing_pre + str(options.linker_sze) + "_summary.txt"
		summary_file_lines = open(summary_file,'r').readlines()
		if len(summary_file_lines) <= 1:
			print("Error! Failed to run minimization, please verify your input PDB")
			sys.exit(0)
		##### Logo ##########################################################
		if LoopSeq is None:
			print("9/%d: Generating the logo" %nb_tot)
			cmd = DFLT_HOME + "create_logo_only.py"
			s_file = options.work_path + WORKING_SUBDIR + "/" + "Gap" + str(options.linker_sze) + "/" + "BCSearch/" + nameing_pre + str(options.linker_sze) + ".fasta"
			logo_fn = options.work_path + nameing_pre + str(options.linker_sze) + ".png"
			model_s_fn = options.work_path + options.Lbl + "_candidate_linkers.txt"
			args = "--input-sequences %s --logo_fname %s " %(s_file, logo_fn)
			if LoopSeq is not None:
				args += "--true_seq %s " %LoopSeq
			if options.aa_constraint_fname != None:
				args += "--aa_constraint_fname %s " %options.aa_constraint_fname
			os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))
		##### Sequ ##########################################################
		if LoopSeq is None:
			print("10/%d: Printing likely sequences" %nb_tot)
			cmd = DFLT_HOME + "likely_sequences.py" 
			args = "--input-sequences %s --model_s_fname %s --out_nseq %s --max_tries %s " %(s_file, model_s_fn, str(options.out_nseq), str(options.max_tries))
			if (options.nter_aa != None) and (options.cter_aa != None):
				args += "--nter_aa %s --cter_aa %s " %(nter_aa, cter_aa)
			else:
				if options.use_flanking_aas:
					print("Fasta for likely_sequences read frome: %s" % options.work_path+WORKING_SUBDIR+"/target_seq.fst")
					pdbseq = Fasta.fasta(options.work_path+WORKING_SUBDIR+"/target_seq.fst")
					nter_aa = pdbseq[pdbseq.ids()[0]].data['s'][-1]
					cter_aa = pdbseq[pdbseq.ids()[0]].data['s'][0]
					args += "--nter_aa %s --cter_aa %s " %(nter_aa, cter_aa)
			if options.aa_constraint_fname != None:
				args += "--aa_constraint_fname %s" %(options.aa_constraint_fname)
			os.system("%s %s %s %s" %(run_docker_cmd, Docker_image, cmd, args))

		print("Finished")

	else:
		######################################################################
		'''
		RUNNING ON A LOCAL COMPUTER AND NOT USING THE DOCKER IMAGE
		'''
		######################################################################
		do_run = True
		
		#1. Prep input
		print("1/%d: Verifying input files" %nb_tot)
		command = "%s/Prep_input.py --wpath %s --target %s --cls_path %s" % (DFLT_HOME, options.work_path, options.target, options.pdb_cls_path)   
		if options.pdb_id is not None:
			command += " --pdb_id %s" %(options.pdb_id)
		if options.linker_sze is not None:
			command += " --linker_sze %d" %(options.linker_sze)
		if LoopSeq is not None:
			command += " --linker_seq %s" %(LoopSeq)
		if options.aa_constraint_fname != None:
			command += " --aa_constraint_fname %s " %(options.aa_constraint_fname)
		if do_run:
			os.system(command)

		if os.path.exists("%s/error.log" %options.work_path):
			sys.exit(0)

		if LoopSeq is None:
			if not os.path.exists("%s/linker_size_pred.txt" %options.work_path):
				print("Error! Please make sure all the arguments are set correctly!")
				sys.exit(0)
			options.linker_sze = int(open("%s/linker_size_pred.txt" %options.work_path, "r").readlines()[0].split()[0])
			if  options.aa_constraint_fname is None:
				options.aa_constraint_fname = "aa_const_pred.txt"

		# 2. Loop search
		print("2/%d: BCLoopSearch and clustering" %nb_tot)
		command = "%s/LoopSearch.py --wpath %s --target %s --nt %s --linker_sze %d --pdb_path %s --pdb_subset %s --bc_min %s --rigidity %s --rmsd_max %s" %(DFLT_HOME, options.work_path, options.target, str(options.nthreads), options.linker_sze, options.pdb_path, options.pdb_subset, str(options.bc_min), str(options.rigidity), str(options.rmsd_max))
		if LoopSeq is not None:
			command += " --linker_seq %s" %(LoopSeq)
		if options.aa_constraint_fname != None:
			command += " --aa_constraint_fname %s " %(options.aa_constraint_fname)
				
		if do_run:
			os.system(command)
		### check if there was any hit found or not
		BCLoopRes = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/BCLoopCluster_Final_BC.txt"
		if not os.path.isfile(BCLoopRes) or os.path.getsize(BCLoopRes) <= 0 :
			print("Error! No candidate was found for your input file!")
			sys.exit(0)
		else:
			BCLoopResFile = open(BCLoopRes,'r').readlines()
			print("Found %d hits (size %d)\n" %(len(BCLoopResFile),options.linker_sze))

		# 3. PyPPP
		print("3/%d: Conformational profiles" %nb_tot)
		command = "%s/PyPPP_prep.py --wpath %s --target %s --pyppp_path %s" %(DFLT_HOME, options.work_path, options.target, "%s/src/pyppp3-light" %options.pep_cyclizer_path)
		if do_run:
			os.system(command)
			os.system("chmod +x " + options.work_path + "runPyPPP.sh")
			os.environ["PPP_ROOT"] = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/pyppp3-light"
			os.environ["BANK_ROOT"] = "/home/ykarami/Documents/MTi/PEP-Cyclizer/banks"
			os.system(options.work_path + "runPyPPP.sh")
			os.system("rm " + options.work_path + "runPyPPP.sh")

		#do_run = False
		# 4. PyPPP scoring
		print("4/%d: Measuring Jenson Shannon distances" %nb_tot)
		command = "%s/PyPPP_score.py --wpath %s --target %s --linker_sze %d --flank_sze %d --np %d --ppp_path %s" %(DFLT_HOME, options.work_path, options.target, options.linker_sze, options.flank_sze, options.nproc, options.prof_path)
		if LoopSeq is not None:
			command += " --linker_seq %s" %(LoopSeq)
		if do_run:
			os.system(command)

		# 5. Preparation for SC & minimization: glue fragments and assing expected sequence for linker.
		print("5/%d: Preparing the candidates" %nb_tot)
		command = "%s/Prep_candidates.py --wpath %s --target %s --linker_sze %d" %(DFLT_HOME, options.work_path, options.target, options.linker_sze)
		if LoopSeq is not None:
			command += " --linker_seq %s" % (LoopSeq)

		if do_run:
			os.system(command)

		# 6. SC positioning on new sequence
		print("6/%d: positioning linker side chains" %nb_tot)
		Loop_dir = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/" 
		BC_final_result = Loop_dir + "BCLoopCluster_Final_BC.txt"
		if os.path.exists(BC_final_result):
			out_dir =  Loop_dir + "Candidates/"
			if os.path.exists(out_dir):
				os.chdir(out_dir)
				command = "export MULUL=%s && ls refe*.pdb > data && %s/oscarstar " % (MULUL, OSCAR_STAR_PATH)
				if do_run:
					os.system(command)

		# 7. Gromacs minimization
		print("7/%d: minimization" %nb_tot)
		out_dir = model_dir + "Gap" + str(options.linker_sze) + "/BCSearch/"
		BC_final_result = out_dir + "BCLoopCluster_Final_BC.txt"
		if do_run:
			if os.path.exists(BC_final_result):        
				gromacs_dir = out_dir + 'Candidates/'
				if os.path.exists(gromacs_dir):
					input_list_read = open(out_dir + "input_gromacs.list", "r").readlines()
					name_list_read = open(out_dir + "gromacs_names.list", "r").readlines()
					nbCandid = len(input_list_read)
					os.chdir(gromacs_dir)
					pool = mp.Pool(1)
					for i in range(nbCandid):
						model = input_list_read[i].split("\n")[0]
						name = name_list_read[i].split("\n")[0]
						pool.apply_async(gro_machine, args=(gromacs_path, model, name,))
					pool.close()
					pool.join()

		# 8. Scoring
		print("8/%d: Scoring the candidates" %nb_tot)
		os.chdir(options.work_path) 
		command = "%s/scoring.py --wpath %s --target %s --linker_sze %d --l %s" %(DFLT_HOME, options.work_path, options.target, options.linker_sze, options.Lbl)
		if LoopSeq is not None:
			command += " --linker_seq %s" %(LoopSeq)
		if do_run:
			os.system(command)

		# 9. Logo creation
		print("9/%d: Generating the logo" %nb_tot)
		s_file = options.work_path + WORKING_SUBDIR + "/" + "Gap" + str(options.linker_sze) + "/" + "BCSearch/" + nameing_pre + str(options.linker_sze) + ".fasta"
		logo_fn = options.work_path + nameing_pre + str(options.linker_sze) + ".png"
		model_s_fn = options.work_path + options.Lbl + "_candidate_linkers.txt"
		command = "%s/create_logo_only.py --input-sequences %s --logo_fname %s " % (DFLT_HOME, s_file, logo_fn)
		if LoopSeq is not None:
			command += " --true_seq %s" %(LoopSeq)
		if options.aa_constraint_fname != None:
			command += " --aa_constraint_fname %s " %(options.aa_constraint_fname)
		if do_run:
			os.system(command)

		# 10. Likely sequences
		print("10/%d: Printing likely sequences" %nb_tot)
		if LoopSeq is None:
			command = "%s/likely_sequences.py --input-sequences %s --model_s_fname %s --out_nseq %s --max_tries %s " % (DFLT_HOME, s_file, model_s_fn, str(options.out_nseq), str(options.max_tries))
			if (options.nter_aa != None) and (options.cter_aa != None):
				command += " --nter_aa %s " %(options.nter_aa)
				command += " --cter_aa %s " %(options.cter_aa)
			else:
				if options.use_flanking_aas:
					print(("Fasta for likely_sequences read frome: %s" % options.work_path+WORKING_SUBDIR+"/target_seq.fst"))
					pdbseq = Fasta.fasta(options.work_path+WORKING_SUBDIR+"/target_seq.fst")
					nter_aa = pdbseq[pdbseq.ids()[0]].data['s'][-1]
					cter_aa = pdbseq[pdbseq.ids()[0]].data['s'][0]
					command += " --nter_aa %s " %(nter_aa)
					command += " --cter_aa %s " %(cter_aa)

			if options.aa_constraint_fname != None:
				command += " --aa_constraint_fname %s " %(options.aa_constraint_fname)
			if do_run:
				os.system(command)

