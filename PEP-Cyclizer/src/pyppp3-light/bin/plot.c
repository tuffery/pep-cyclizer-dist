#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cairo.h>

typedef struct _color {
  float r, g, b;
} color;

void read_probfile(FILE *f, float *p, int nr, int nc)
{
  int i = 0;
  while(!feof(f) && i < nr*nc) {
    if (fscanf(f, "%f", &p[i]) == 1) i++;
  }

  if (! (i == nr*nc) ) {
    fprintf(stderr, "Could not load the prob file (%d %d %d)\n", i, nr, nc);
    exit(3);
  }

  // normalize profile
  double sum;
  int r, c;
  for(c=0 ; c<nc ; c++) {
    sum = 0;
    for(r=0 ; r<nr ; r++) sum += p[c*nr+r];
    for(r=0 ; r<nr ; r++) p[c*nr+r] = p[c*nr+r]/sum;
  }
}

int main(int argc, char **argv)
{
  char *sequence, *filename, *title;
  int n, nrows, ncols, i, j;
  FILE *f;
  float *probs;
  cairo_surface_t *surface;
  cairo_t *cr;

  char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZa";
  char *alpha = "aAVWZBCDEOSRQIFUPHGYJKLMNXT";
  int reordered[27];
  for(i=0 ; i<27 ; i++) {
    reordered[i] = strchr(alphabet, alpha[i])-alphabet;
  }

  int hexcolors[] = {0xFF0000, 0xE20000, 0xC40000, 0xA80000, 0x8B0000, 0x0000CD,
		     0x0000D3, 0x0000D9, 0x0000DF, 0x0000E6, 0x0000EC, 0x0000F2,
		     0x0000F8, 0x0000FF, 0x0000F8, 0x0000F2, 0x0000EC, 0x0000E6,
		     0x0000DF, 0x0000D9, 0x0000D3, 0x0000CD, 0x006400, 0x008A00,
		     0x00B100, 0x00D800, 0x00FF00};
  color colors[27];
  for(i=0 ; i<27 ; i++) {
    colors[i].r = (float)( (hexcolors[i]>>16) & 0xFF)/255;
    colors[i].g = (float)( (hexcolors[i]>>8)  & 0xFF)/255;
    colors[i].b = (float)( hexcolors[i]       & 0xFF)/255;
  }

  // parameters
  if (argc != 4) {
    fprintf(stderr, "Usage: %s prob_file sequence title\n", argv[0]);
    exit(1);
  }

  filename = argv[1];
  sequence = argv[2];
  title = argv[3];

  n = strlen(sequence);

  // load prob file
  nrows = 27;
  ncols = n-3;
  probs = (float*) malloc(ncols*nrows*sizeof(float));

  f = fopen(filename, "r");
  if (f == NULL) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    exit(2);
  }
  read_probfile(f, probs, nrows, ncols);
  fclose(f);
 
  // draw
  int column_width = 15;
  int padding = 30;
  int width = column_width * n + 4*padding;
  int height = 400;
  char label[32];
  int unit_width = floor(((float)width-4*padding)/ncols);

  width = unit_width*ncols+4*padding;
  surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, width, height);
  cr = cairo_create (surface);

  // white background
  cairo_set_source_rgb(cr, 1, 1, 1);
  cairo_rectangle(cr, 0, 0, width, height);
  cairo_fill(cr);

  // title
  cairo_text_extents_t extents;
  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size(cr, 14);

  cairo_text_extents(cr, title, &extents);
  int title_height = extents.height + 20;
  cairo_move_to(cr, width/2 - extents.width + extents.x_bearing, extents.height + 10);
  cairo_show_text(cr, title);
  cairo_set_font_size(cr, 10);

  // y-axis
  float x0 = 2*padding;
  float y0 = height-padding;
  float x, y;
  height = height -2*padding - title_height;
  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_move_to(cr, x0-padding/2, y0);
  cairo_line_to(cr, x0-padding/2, y0-height);
  cairo_stroke(cr);
  float step;
  for(step=0.0 ; step <= 1.0 ; step += 0.2) {
    x = x0-padding/2;
    y = y0-step*height;

    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x-5, y);
    cairo_stroke(cr);

    sprintf(label, "%.1f", step);
    cairo_text_extents(cr, label, &extents);
    cairo_move_to(cr, x-10-(extents.width+extents.x_bearing), y+extents.height+extents.y_bearing/2);
    cairo_show_text(cr, label);
  }

  // x-axis
  for(i=0 ; i<ncols ; i++) {
    sprintf(label, "%c", sequence[i]);
    cairo_text_extents(cr, label, &extents);
    x = x0 + i*unit_width + unit_width/2 - (extents.width/2 + extents.x_bearing);
    y = y0 + 10;
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, label);
    y = y0 - height - 10;
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, label);
  }

  // boxes
  float value;
  int col;
  color c;
  cairo_set_line_width(cr, 0.5);
  for(i=0 ; i<ncols ; i++) {
    x = x0 + i*unit_width;
    y = y0;
    for(j=0 ; j<nrows ; j++) {
      col = reordered[j];
      value = probs[i*nrows+col];
      c = colors[j];

      cairo_set_source_rgb(cr, c.r, c.g, c.b);
      cairo_rectangle(cr, x, y, unit_width, (float)-height*value);
      cairo_fill(cr);
      cairo_set_source_rgb(cr, 0, 0, 0);
      cairo_rectangle(cr, x, y, unit_width, (float)-height*value);
      cairo_stroke(cr);

      y -= (float)height*value;
    }
  }

  // finish
  char output[256];
  snprintf(output, 256, "%s.svmi8.27.prob.probplot.27.png", title);
  cairo_surface_write_to_png(surface, output);

  return 0;
}
