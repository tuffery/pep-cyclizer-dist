import os

DOCKER = int(os.environ['IS_DOCKER'])

if DOCKER :
	run_mode   = "docker"
else:
	run_mode   = "PC"

DFLT_MODE      = run_mode

# The working path
DFLT_WORK_PATH     = "./"                             # The directory to work in, must contain the target pdb 
WORKING_SUBDIR     = "pep_cyclizer_candidate_search"  # The name of the subdirectory with step by step results
DFLT_PDB_ID        = None                             # The PDB id of a homolog of the target (to remove the homologs)
DFLT_LBL           = "cyclic"                         # the prefix label for the results

# Location of the various databanks
if run_mode == "docker":
	Docker_image         = "pep-cyclizer"                   				# Name of the docker image
	DFLT_BANK_ROOT_PATH  = "/scratch/banks"             					# The root directory for all the banks
	LOCAL_BANK_PATH_USER = "/home/ykarami/Documents/MTi/PEP-Cyclizer/banks"	# The local directory were all the banks are downloaded
	DFLT_HOME_PPP        = "/usr/local/pyppp3-light/"
	DFLT_DEMO_DATA_PATH  = "/service/env/PEPCyclizer/demo/"
	run_docker_cmd = "sudo docker run -it --rm -v $(pwd):$(pwd) -v %s:%s -w $(pwd)" %(LOCAL_BANK_PATH_USER, DFLT_BANK_ROOT_PATH)
	gro_docker_cmd = "sudo docker run -it --rm -v $(pwd):$(pwd) -w $(pwd)"
	gro_docker_name = "gromacs:2020.4.gromacs-py"
        
else:
	DFLT_BANK_ROOT_PATH = "/home/ykarami/Documents/MTi/PEP-Cyclizer/banks"
	Docker_image 	   = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/pep_cyclizer/"
	DFLT_HOME_PPP      = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/pyppp3-light/"
	OSCAR_STAR_PATH    = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/oscar/bin"
	MULUL              = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/oscar/src/OSCAR/library/z/"
	gromacs_path       = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/src/gromacs_py/gromacs_py/"
	DFLT_DEMO_DATA_PATH   = "/home/ykarami/Documents/MTi/PEP-Cyclizer/SourceCode/demo/"

DFLT_PDB_CLS_PATH   = DFLT_BANK_ROOT_PATH + "/PDB_clusters/"
DFLT_PDB_PATH       = DFLT_BANK_ROOT_PATH + "/pdbChainID/"
DFLT_PROF_PATH      = DFLT_BANK_ROOT_PATH + "/PyPPP_profiles/"
DFLT_PDB_SUBSET     = "pdb70"

# about modules/code location/invocation
DFLT_DIST_ROOT      = "/usr/local"
DFLT_HOME           = DFLT_DIST_ROOT + "/pep_cyclizer/"
#oscar_star_docker   = "dev-registry-v2.rpbs.univ-paris-diderot.fr/oscar-star"

# BC search default parameters
DFLT_BC_MIN        = 0.8
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.

# parallelism
DFLT_NPROC         = 8
DFLT_NTHREADS      = 8

#DFLT_MODE          = "machine"  #else "cluster"
MAX_JOB            = 300
CLUSTER            = False

#DFLT_MOBYLE_JOBS_PATH = "/data/jobs/"

# Weights for Dirichlet applied to transition matrix.
TM_REF_WEIGHT = 1.0 #0.9
TM_OBS_WEIGHT = 0.0 #0.1

# Weights for Dirichlet applied to amino acid frequencies.
FR_REF_WEIGHT = 0.
FR_OBS_WEIGHT = 1.

# Minimal frequency assigned to constrained amino acids 
PSEUDO_FREQ = 0.8 #0.1

# Maximal number of sequences drawn (not considering redundancy)
OUT_NSEQ = 150

