FROM debian:trixie

MAINTAINER Yasaman Karami  <yasaman.karami@inria.fr>

# Installation of debian packages
RUN apt-get -q -y update

RUN apt-get install -q -y \
  vim \
  gcc \
  gcc-multilib \
  python3 \
  awscli \
  hostname \
  zlib* 

RUN apt-get install -q -y \
  build-essential \
  python3-pip \
  ghostscript \
  htop \
  clang \
  lldb \
  llvm \
  libboost-all-dev \
  libopenmpi-dev \
  binutils \
  wget \
  git \
  openssh-server

RUN apt-get clean && apt-get purge && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN pip install numpy --break-system-packages
RUN pip install scipy --break-system-packages
RUN pip install biopython --break-system-packages
RUN pip install weblogo --break-system-packages
RUN pip install cffi --break-system-packages
RUN mkdir /var/run/sshd && \
echo 'root:screencast' | chpasswd && \
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

# Don't use dash for compatibility 
RUN ln -sfn /bin/bash /bin/sh

# Install PyPDB
COPY src/PyPDB3 /usr/local/PyPDB3
RUN cd /usr/local/PyPDB3/ && python3 setup.py install

# Install Fasta3
COPY src/Fasta3 /usr/local/Fasta3
RUN cd /usr/local/Fasta3 && python3 setup.py install

# Insall Oscat Star
ENV MULUL /src/OSCAR/library/z/
COPY src/oscar/bin /src/bin
COPY src/oscar/src /src/
ENV PATH=${PATH}:/src/bin

# pep-cyclizer specific code
COPY src/pep_cyclizer /usr/local/pep_cyclizer

# Install PyPPP
COPY src/pyppp3-light /usr/local/pyppp3-light
RUN cd /usr/local/pyppp3-light/thirdparty/ && tar xzf blast-2.2.26-x64-linux.tar.gz && rm -f blast-2.2.26-x64-linux.tar.gz
RUN cd /usr/local/pyppp3-light && python3 setup.py build &&  python3 setup.py install
ENV PPP_ROOT=/usr/local/pyppp3-light
ENV BANK_ROOT=/scratch/banks 
ENV PATH=${PATH}:/usr/local/pyppp3-light/thirdparty/blast-2.2.26/bin
