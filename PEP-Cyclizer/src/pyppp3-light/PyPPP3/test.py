#from svmutil import *
from svm import *
import sys
import mmap
import os
    
print("Loading model %s" % sys.argv[1])
m = libsvm.svm_load_model_binary(sys.argv[1])
model = toPyModel(m)
labels = model.get_labels()
n_classes = model.get_nr_class()
print(n_classes, " classes")
print(labels)

print("Loading scaling data")
with open(sys.argv[2], 'r') as f:
    scaling = [[float(v) for v in x.strip().split()] for x in f]

print("Loading prediction data")
with open(sys.argv[3], 'r') as f:
    tab = [[float(v) for v in x.strip().split()] for x in f]

print("Scaling data")
for row in range(len(tab)):
    for col in range(len(tab[row])):
        tab[row][col] -= scaling[col][0]
        tab[row][col] /= scaling[col][1]

print("Prediction")
probs = (c_double*(len(tab)*n_classes))()
results = []
for row in tab:
    xi, max_idx = gen_svm_nodearray(row)
    results.append(libsvm.svm_predict_probability(m, xi, probs))

print(results)
