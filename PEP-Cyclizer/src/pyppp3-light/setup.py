#!/usr/bin/python
from distutils.core import setup

setup(name='PyPPP3',
      version='1.0',
      description='A generator ofr HMM-SA profiles',
      long_description="""\
PyPPP : a python class to run prediction of HMM-SA profiles from amino acid sequence.

Written (2008-2011) by P. Tuffery, INSERM, France

This class is used in production since 2010 at the RPBS structural
bioinformatics platform. 

""",
      author='P. Tuffery',
      author_email=['pierre.tuffery@univ-paris-diderot.fr'],
      url='http://bioserv.rpbs.univ-paris-diderot.fr',
      scripts = ['PyPPP3Exec'],
      packages = ['PyPPP3'],
      # py_modules=['PyBlast'],
      classifiers=['License :: None Approved :: Author\'s property',
                   'Operating System :: Unix',
                   'Programming Language :: Python',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
      license='Non free, non open source'
     )

