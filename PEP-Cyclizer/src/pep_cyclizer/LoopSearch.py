#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018
MTi

Revised by P. Tuffery, 2019.
"""

###########################
###  L I B R A R I E S  ###
###########################
import numpy as np
import sys
sys.path.append('../BC2')
from BC2 import PDBLibrary
from BC2 import BCLoopSearch
from PyPDB import PyPDB as PDB
import sys, os
sys.path.append('.')
from rmsd import *
from scipy.cluster.hierarchy import fcluster
from scipy.cluster.hierarchy import dendrogram, linkage
import scipy.cluster.hierarchy as hcl
from scipy.spatial.distance import squareform
import argparse
import aa_constraints
#from Config import *

#########################################################################
#########################################################################
AAs = "ARNDCQEGHILKMFPSTWYVBZX*"
blosum62 = [
      [ 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4],
      [-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4],
      [-2,  0 , 6 , 1 ,-3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4 ],
      [-2, -2 , 1 , 6 ,-3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4 ],
      [ 0, -3 ,-3 ,-3  ,9 ,-3, -4 ,-3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4 ],
      [-1,  1 , 0 , 0 ,-3,  5 , 2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4 ],
      [-1,  0 , 0 , 2 ,-4,  2 , 5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4 ],
      [ 0 ,-2  ,0 ,-1 ,-3 ,-2 ,-2,  6 ,-2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4 ],
      [-2 , 0 , 1 ,-1 ,-3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4 ],
      [-1 ,-3 ,-3 ,-3 ,-1 ,-3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4 ],
      [-1 ,-2 ,-3 ,-4 ,-1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4 ],
      [-1 , 2 , 0 ,-1 ,-3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4 ],
      [-1 ,-1 ,-2 ,-3 ,-1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4 ],
      [-2 ,-3 ,-3 ,-3 ,-2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4 ],
      [-1 ,-2 ,-2 ,-1 ,-3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4 ],
      [ 1 ,-1 , 1 , 0 ,-1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4 ],
      [ 0 ,-1 , 0 ,-1 ,-1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4 ],
      [-3 ,-3 ,-4 ,-4 ,-2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4 ],
      [-2, -2 ,-2 ,-3 ,-2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4 ],
      [ 0 ,-3 ,-3 ,-3 ,-1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4 ],
      [-2 ,-1 , 3 , 4 ,-3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4 ],
      [-1 , 0 , 0 , 1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4 ],
      [ 0 ,-1 ,-1 ,-1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4 ],
      [-4 ,-4, -4 ,-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1 ]]
# BC search default parameters
DFLT_BC_MIN        = 0.8
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.
# parallelism
DFLT_NPROC         = 8
DFLT_NTHREADS      = 8
MAX_JOB            = 300
CLUSTER            = False
WORKING_SUBDIR     = "pep_cyclizer_candidate_search"  # The name of the subdirectory with step by step results


def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def Simil_exact_blo(aa_cons, hitSeq):
    Score = 0; blosum_score = 0; iden_len = 0; blos_len = 0; sub_score = 0; Final_flag = 0
    for i in range(0,len(hitSeq)):
        ind = str(i+1)
        if len(aa_cons[ind]) >= 1 :
            blos_len += 1
            sub_score = 0
            pos2 = AAs.find(hitSeq[i])
            for j in range(0,len(aa_cons[ind])):
                pos1 = AAs.find(aa_cons[ind][j])
                blosum = blosum62[pos1][pos2]
                if blosum > sub_score :
                    sub_score = blosum
        else:
            sub_score = 0
        blosum_score += sub_score

    if blosum_score >= 0 :
        Final_flag = 1
    return Final_flag

def Simil_exact(aa_cons, hitSeq):
    Score = 0; blosum_score = 0; iden_len = 0; blos_len = 0; sub_score = 0; Final_flag = 0
    for i in range(0,len(hitSeq)):
        ind = str(i+1)
        if len(aa_cons[ind]) == 1 :
            blos_len += 1
            pos1 = AAs.find(aa_cons[ind][0])
            pos2 = AAs.find(hitSeq[i])
            blosum_score += blosum62[pos1][pos2]
        elif len(aa_cons[ind]) > 1 :
            pos2 = hitSeq[i]
            sub_score = 0
            iden_len += 1
            for j in range(0,len(aa_cons[ind])):
                pos1 = aa_cons[ind][j]
                if (pos1 == pos2):
                    sub_score = 1
        else:
            sub_score = 1
        Score = Score + sub_score

    if Score >= (iden_len//2) and blosum_score >= 0 :
        Final_flag = 1
    return Final_flag

def Simil_blosum(refeSeq, hitSeq):
    '''
    BLOSUM score
    '''
    Score = 0; Final_flag = 0  
    for i in range(0,len(refeSeq)):
        ## blosum score
        pos1 = AAs.find(refeSeq[i])
        pos2 = AAs.find(hitSeq[i])
        Score = Score + blosum62[pos1][pos2]

    if Score >= 0:
        Final_flag = 1
    return Final_flag

## Clustering
def write_candidates(hits, hitList, outputdir, seq_flank, flank_size, loopSeq, aa_cons):
    pdb_dir = mk_dir(outputdir + "PDB_files")
    newHit = []
    for hit in hits:
        if hit.valid:
            CandidSeq = hit.seq[flank_size:(len(hit.seq)-flank_size)]
            if loopSeq is not None:
                SeqSimil = Simil_blosum(loopSeq,CandidSeq)
            else:
                if len(aa_cons) > 0:
                    ###SeqSimil = Simil_exact_blo(aa_cons,CandidSeq)
                    SeqSimil = Simil_exact(aa_cons,CandidSeq)
                else:
                    SeqSimil = 1
            if SeqSimil == 1:
                c = hit.pdbcode
                name = c + "-" + hit.chain + "-" + str(hit.model) + "-" + str(hit.match_start) + "-" + str(hit.match_end)
                pdbtxt = hit.pdb()
                oname = pdb_dir + "/" + name + ".pdb"
                hit.output_name = oname
                open(oname, "w").write(pdbtxt)
                newHit.append(hit)
                hitList.write(pdb_dir + "/" + name + ".pdb\n")
    hitList.close()
    return newHit

def write_clustering(outputdir,clustHits,ClustID,flank_size,th_rigidity,th_BC,cluster_ind):
    clustName = outputdir + "/BCLoopCluster_" + ClustID + "_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt"
    clustList = open(clustName,'w')
    keys = list(cluster_ind.keys())
    clust_len = len(cluster_ind)
    for j,hit in enumerate(clustHits):
        hitLoopSeq = hit.seq[flank_size:(len(hit.seq)-flank_size)]
        CandLoopSeq = "".join(str(x) for x in hitLoopSeq)
        FlankLoopSeq = "".join(str(x) for x in hit.seq)
        clust_ind_write = []
        for i in range(0,clust_len):
            clust_ind_write.append(cluster_ind[keys[i]][j])
        clust_ind_write = " ".join(str(x) for x in clust_ind_write)
        clustList.write("%s %s %d %d %d %f %f %f %s %s %s\n"%(hit.pdbcode, hit.chain, hit.model, hit.match_start, hit.match_end, hit.score, hit.rigidity, hit.rmsd, CandLoopSeq, FlankLoopSeq, clust_ind_write))

def Simil(refeSeq,hits,flank_size):
    # measuring sequence similarity between every candidate and the sequence of the gap
    Simil = [];
    for hit in hits:
        hitSeq = hit.seq[flank_size:(len(hit.seq)-flank_size)]
        Score = 0
        for i in range(0,len(refeSeq)):
            if(refeSeq[i] == hitSeq[i]):
                Score += 1
        perScore = float(Score) // len(refeSeq)
        intScore = int(perScore * 100)
        Simil.append(intScore)
    return Simil

def cluster(RMSD,threshold,outputdir):
    ### 3D clustering of the candidates based on the RMSD
    Z = hcl.linkage(squareform(RMSD))
    clusters = np.array(fcluster(Z,threshold, criterion='distance'))
    return clusters

def sort(hitList,clust,Similarity,outputdir,chunck,list_ind,th_rigidity,flank_size,th_BC):
    '''
    choosing one candidate with the highest sequence similarity from each cluster
    storing the members of each cluster
    '''
    clust_DIR = outputdir + "/Clusters"
    if not(os.path.exists(clust_DIR)):
        os.mkdir(clust_DIR)
    clust_DIR += '/'
    ClustTick = np.zeros(len(clust), dtype='int32'); ind = 0
    hitnames = open(hitList,'r').readlines()
    hit_pdb_name = []
    for i in range(0,len(hitnames)):
        hit_pdb_name.append(hitnames[i].split("\n")[0])
    hit_pdb_name = np.array(hit_pdb_name)

    for j in range(0,max(clust)):
        clustInd = np.argwhere(clust == (j+1))
        #print clustInd
        Maxx = np.max(Similarity[clust == (j+1)])
        #print Maxx
        Maxx_Ind = np.argwhere(Similarity[clustInd] == Maxx)[:,0]
        #print Maxx_Ind
        Ind_clust_max = clustInd[Maxx_Ind][:,0]
        Sorted_Ind = np.argsort(hit_pdb_name[Ind_clust_max])
        index = int(np.squeeze(clustInd[Sorted_Ind[0]]))  #int(clustInd[Sorted_Ind[0]])
        ClustTick[index] = j+1
        ## writing the cluster members
        clustOut = open(clust_DIR + "cluster_" + list_ind + "_" + str(chunck) + "_" + str(j+1) + "_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt","w")
        for k in range(0,len(clustInd)):
            ind = int(np.squeeze(clustInd[k]))
            name = hit_pdb_name[ind]
            pdbfile = PDB.PDB(name)
            pdbSeq = pdbfile.aaseq()
            seq_loop = pdbSeq[flank_size:(len(pdbSeq)-flank_size)]
            clustOut.write("%s\t%s\t%s\n" %(name, str(Similarity[ind]), seq_loop))
        clustOut.close()
    return ClustTick

def prepHitList(hitListname,start1,end1,nchunks):
    hitList = open(hitListname , 'r').readlines()    
    subListName = hitListname + str(nchunks)
    sub_hitList = open(subListName , 'w')
    for i in range(start1,end1):
        sub_hitList.write(hitList[i])
    return subListName

def divide_cluster(hits, loopSeq, outputdir, hitListname, threshold, list_ind, nbCluster,flank_size,th_BC,th_rigidity):
    clustHits = []; cluster_ind = []; ClustInd= []; ind = 0; nbClust=0;
    newListName = outputdir + "/list_" + list_ind + "_Cluster_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt"
    newList = open(newListName,'w')
    Similarity = Simil(loopSeq,hits,flank_size)
    Similarity = np.array(Similarity)
    nbHits = len(hits)
    ClustTick = np.zeros(nbHits)
    Clust_Ind = [] #np.array(nbHits)
    chunksize = nbCluster
    nchunks = int(float(nbHits)/chunksize+0.9999999)
    for i in range(nchunks):
        start1 = i*chunksize
        end1 = (i+1) * chunksize
        if end1 > nbHits:
            end1 = nbHits
        subListName = prepHitList(hitListname,start1,end1,i)
        RMSDmat = RMSDall(subListName,outputdir,flank_size,len(loopSeq))
        clust = cluster(RMSDmat,threshold,outputdir)
        nbClust = max(clust)
        subClustTick = sort(subListName,clust,Similarity[start1:end1],outputdir,i,list_ind,th_rigidity,flank_size,th_BC)
        ClustTick[start1:end1] = subClustTick
        for k in range(start1,end1):
            ll = k - start1
            indd = list_ind + "_" + str(i) + "_" + str(subClustTick[ll])
            Clust_Ind.append(indd)

    for hit in hits:
        if ClustTick[ind] > 0:
            newList.write(hit.output_name + "\n")
            clustHits.append(hit)
            ClustInd.append(Clust_Ind[ind])
        ind += 1
    return newListName, clustHits, ClustTick, nbClust, ClustInd

def Clustering(model_dir, looplength, th_BC, th_rigidity, flank_size, hits, seq_flank, loopSeq, aa_cons):
    nbCluster = 25000
    gap_dir = mk_dir(model_dir + "Gap" + str(looplength))
    outputdir = mk_dir(gap_dir + "BCSearch")   

    hitListname = outputdir + "/list_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt"
    hitList = open(hitListname,'w')
    newHits = write_candidates(hits, hitList, outputdir, seq_flank, flank_size, loopSeq, aa_cons)
    cluster_ind = {}
    ###########################################
    if loopSeq is None: ### if the sequence is not given, we skip the clustering
        list_ind = ''
        write_clustering(outputdir,newHits,list_ind,flank_size,th_rigidity,th_BC,cluster_ind)
    else:
        if len(newHits) == 1:
            list_ind = "1st"
            write_clustering(outputdir,newHits,list_ind,flank_size,th_rigidity,th_BC, cluster_ind)
        else:
            sys.stderr.write( "Clustering %d hits (size %d)\n" % (len(hits), looplength) )
            list_ind = "1st"; threshold = 1
            newListName, clustHits, ClustTick, nbClust, cluster_ind[list_ind] = divide_cluster(newHits, loopSeq, outputdir, hitListname, threshold, list_ind, nbCluster, flank_size, th_BC, th_rigidity)
            list_ind = "2nd"; threshold = 1
            newListName, clustHits, ClustTick, nbClust, cluster_ind[list_ind] = divide_cluster(clustHits, loopSeq, outputdir, newListName, threshold, list_ind, nbCluster, flank_size, th_BC, th_rigidity)
            roundN = 3
            while(nbClust >= nbCluster):
                list_ind = str(roundN); threshold = 1
                newListName, clustHits, ClustTick, nbClust, cluster_ind[list_ind] = divide_cluster(clustHits, loopSeq, outputdir, newListName, threshold, list_ind, nbCluster, flank_size, th_BC, th_rigidity)
                roundN += 1
            sys.stderr.write( "LoopSearch: Found %d clusters of hits (size %d)\n" % (len(clustHits), looplength) )

            write_clustering(outputdir, clustHits, list_ind,flank_size,th_rigidity,th_BC, cluster_ind)
    #############################################
    os.system("cp " + outputdir + "BCLoopCluster_" + list_ind + "_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt " + outputdir + "BCLoopCluster_Final_BC.txt")
    return

## BCSearch
def BCrun(model_dir, looplength, dbdir, dbname, th_BC, th_rigidity, num_cpu, th_RMSD, loopSeq, aa_cons):

    sys.stderr.write( "Searching for loop size %d\n" % (looplength) )
    gap_dir = mk_dir(model_dir + "Gap" + str(looplength))
    outputdir = mk_dir(gap_dir + "BCSearch")
    refe_pdb = PDB.PDB(gap_dir + "GappedPDB.pdb")
    refe_array = get_ca(refe_pdb)
    
    flank1 = PDB.PDB(gap_dir + "flank1.pdb")
    flank2 = PDB.PDB(gap_dir + "flank2.pdb")
    flank1_ca = get_ca(flank1)
    flank2_ca = get_ca(flank2)
    flank_size = len(flank1)
    seq_flank = flank1.aaseq() + flank2.aaseq()

    homologs = []
    if os.path.exists(model_dir + "homologs.list"):
        homolog_file = open(model_dir + "homologs.list","r").readlines()
        for i in range(len(homologs)):
            linee = homolog_file[i].split()[0]
            homologs.append(linee)

    library = PDBLibrary(dbdir, dbname)
    if len(homologs):
        library = library.exclude_codes(homologs)

    run = BCLoopSearch()
    run.flank1 = flank1_ca
    run.flank2 = flank2_ca
    run.looplength = looplength
    run.minloopmatch = looplength
    run.maxloopgap = 0
    run.minBC = th_BC
    run.maxR = th_rigidity
    run.maxRMSD = th_RMSD
    run.library = library
    run.clash_body = refe_array
    run.maxhits = 90000000 #500000 #90000000 #1000000
    run.nthreads = num_cpu
    hits = run.run()
    codes = {}
    sys.stderr.write( "LoopSearch: Found %d raw hits (size %d)\n" % (len(hits), looplength) )
    
    if len(hits) == 0:
        sys.stderr.write("Error! no hit was found!")
    else:
        Clustering(model_dir, looplength, th_BC, th_rigidity, flank_size, hits, seq_flank, loopSeq, aa_cons)

    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
                        action="store", required=True, default = None)

    parser.add_argument("--aa_constraint_fname", dest="aa_constraint_fname", help="File specifying the list of the desired amino acids at each position",
                        action="store", default=None) 

    parser.add_argument("--wpath", dest="work_path", help="path to target",
                        action="store", default=None)
    
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--nt", dest="nthreads", type = int, help="number of threads (%d)" % DFLT_NTHREADS,
                        action="store", default=DFLT_NTHREADS)
    
    parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB",
                        action="store", default=None)
    
    parser.add_argument("--pdb_subset", dest="pdb_subset", help="name of indexed PDB",
                        action="store", default=None)
    
    parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum) (%f)" % DFLT_BC_MIN,
                        action="store", default=DFLT_BC_MIN)
    
    parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
                        action="store", default=DFLT_RIGIDITY_MAX)
    
    parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
                        action="store", default=DFLT_RMSD_MAX)
    
    parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None)

    parser.add_argument("--run_docker", dest="run_docker", help="in docker mode (\"True\") or not (\"False\")",
                        action="store_true", default=False)

    parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Linker size",
                        action="store", default=None)  
        
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")


    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    if options.pdb_path[-1] != "/":
        options.pdb_path = options.pdb_path + "/"
        
    #print(print_options(options))    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr   = options.work_path
    protName     = options.target
    pdbdir       = options.pdb_path
    pdbName      = options.pdb_subset
    BCth         = options.bc_min
    RigidityTh   = options.rigidity
    RMSDth       = options.rmsd_max
    LoopSize     = options.linker_sze
    loopSeq      = options.linker_seq
    aa_type_file = options.aa_constraint_fname
    mode         = options.run_docker
    nbThreads    = options.nthreads

    aa_cons = {}
    if aa_type_file != None:
        aa_cons = aa_constraints.constraints(options.aa_constraint_fname)

    linker_search_dir = mk_dir(options.work_path + WORKING_SUBDIR) # Impose sub tree
    BCrun(linker_search_dir, options.linker_sze, pdbdir, pdbName, BCth, RigidityTh, nbThreads, RMSDth, loopSeq, aa_cons)


