#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
import BC
from   PyPDB import PyPDB as PDB
import os
from rmsd import *
import argparse
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
PROT_DIR           = "pep_cyclizer_test"
PROT_DIR           = "pep_cyclizer_candidate_search"

#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def find_clash(clash_body_tree, loop,):
    Clash_th = 3  
    FinalClashes = 0
    loop_bb = get_ca(loop)
    loop_bb = np.array(loop_bb)
    
    if clash_body_tree is not None:
        clashes = clash_body_tree.query_ball_point(loop_bb,Clash_th)
        for l in clashes:
          for x in l:
            FinalClashes = FinalClashes + 1

    return FinalClashes


def read_and_score(Waddr, RMSDvallines, gappedPDB, Loop_addr, LoopStart, flank_len, LoopSize, LoopSeq):
    top_all = []
    rep_top = 50  #increasing number of sequences
    refe_len = len(gappedPDB)
    ### clash body
    refe_array = get_ca(gappedPDB)
    clash_body = refe_array
    clash_body_tree = None
    if clash_body is not None:
        clash_body_tree = KDTree(np.array(clash_body))

    score = np.zeros((len(RMSDvallines),2))
    nbb = 0
    for j in range(0,len(RMSDvallines)):
        if RMSDvallines[j].startswith("#"): continue
        candid = RMSDvallines[j].split()
        PyPPP_score_file = Waddr + "PyPPP_score_" + candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4] + ".txt"
        if not os.path.exists(PyPPP_score_file): continue
        score[nbb,0] = float(candid[7]) # flank RMSD
        score[nbb,1] = j
        nbb = nbb + 1
    SortArguRMSD = np.argsort(score[0:nbb,0])
    ## top candidates using RMSD
    if nbb < rep_top:
        rep_top = nbb
    ind = 0; nb_top = 0

    while ind < nbb and nb_top < rep_top:
        line_nb = int(score[SortArguRMSD[ind],1])
        candid = RMSDvallines[line_nb].split()
        pdb_code = candid[0]
        pdb_chain = candid[1]
        pdb_model = int(candid[2])
        pdb_start = int(candid[3])
        pdb_end = int(candid[4])
        name = pdb_code + "-" + pdb_chain + "-" + str(pdb_model) + "-" + str(pdb_start) + "-" + str(pdb_end)
        candid = PDB.PDB(Loop_addr + "PDB_files/" + name + ".pdb")
        candid_linker = candid[flank_len : (len(candid) - flank_len)]
        cand_final = gappedPDB + candid_linker
        ### check for clashes
        if LoopSeq is None:
           is_clash = 0
        else:
           is_clash = find_clash(clash_body_tree, candid_linker)
        if is_clash == 0:
            top_all.append(line_nb)
            nb_top += 1
        ind += 1
    ind_top = np.array(top_all)
    return ind_top

def Join_refe_candid(refe,flank_len,RMSDfile,outputdir,Loop_addr,LoopSeq,LoopSize):
    AAs = "ARNDCEQGHILKMFPSTWYV"
    AA3 = ("ALA","ARG","ASN","ASP","CYS","GLU","GLN","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL")
    candid_file1 = open(outputdir + "data", "w")
    candid_file2 = open(Loop_addr + "input_gromacs.list", "w")
    candid_file3 = open(Loop_addr + "gromacs_names.list", "w")
    candid_file4 = open(Loop_addr + "top_models.list", "w")
    refe_len = len(refe)

    RMSDvallines = open(RMSDfile,"r").readlines()
    ind_top = read_and_score(Loop_addr, RMSDvallines, refe, Loop_addr, refe_len, LoopSize, flank_len, LoopSeq)

    for j in ind_top:
        if RMSDvallines[j].startswith("#"): continue
        linee = RMSDvallines[j].split()
        pdb_code = linee[0]
        pdb_chain = linee[1]
        pdb_model = int(linee[2])
        pdb_start = int(linee[3])
        pdb_end = int(linee[4])
        Name = pdb_code + "-" + pdb_chain + "-" + str(pdb_model) + "-" + str(pdb_start) + "-" + str(pdb_end)
        oname = outputdir + Name + ".pdb"
        candid = PDB.PDB(Loop_addr + "PDB_files/" + Name + ".pdb")
        candid.out(oname)
        candid_len = len(candid)
        candid_bb = get_bb(candid)

        PyPPP_score_file = Loop_addr + "PyPPP_score_" + Name + ".txt"
        if not os.path.exists(PyPPP_score_file): continue
        PyPPP_line = open(PyPPP_score_file,'r').readlines()[0]

        '''
        ### align candids to the refe
        refe_flank   = refe[(refe_len - flank_len):refe_len] + refe[0:flank_len]
        candid_flank = candid[0:flank_len] + candid[(candid_len - flank_len):candid_len]
        refe_flank_ca = get_ca(refe_flank)
        candid_flank_ca = get_ca(candid_flank)
        rotmat, offset, rmsdAll = fit(refe_flank_ca, candid_flank_ca)
        pivot = np.sum(candid_flank_ca,axis=0) / len(candid_flank_ca)
        new_bb = apply_matrix(candid_bb, pivot, rotmat, offset) 
        candid_new_pdb = update_bb(candid, new_bb) 
        '''
        cand_final = refe + candid[flank_len : (candid_len - flank_len)]  #candid_new_pdb[flank_len : (candid_len - flank_len)]
        new_len = len(cand_final)
        nbAtom = 1
        ### if missing SC use oscar to recover
        nb_SC, list_SC = cand_final.SCatmMiss()
        #nb_BB, list_BB = cand_final.BBatmMiss()

        for i in range(0,new_len):
                res_label = cand_final[i].rName() + '_' + cand_final[i].chnLbl() + '_' + cand_final[i].rNum() + '_' + cand_final[i].riCode()
                cand_final[i].rNum(i+1)
                cand_final[i].chnLbl('A')
                ### modifying residue names if we know the sequence
                if (i >= refe_len) and (LoopSeq is not None):
                    ind = i - refe_len
                    res_ind = AAs.find(LoopSeq[ind])
                    res_name = AA3[res_ind]
                    cand_final[i].rName(res_name)
                for k in range(0,len(cand_final[i].atms)):
                        cand_final[i].atms[k].alt(" ")
                        cand_final[i].atms[k].atmNum(nbAtom)
                        nbAtom = nbAtom + 1
                        if i < refe_len:
                            Xseg = ''
                            if len(cand_final[i].atms[k].txt) > 79:
                               before = 78 #79
                               Xseg += ' '
                               tail = '\n'  #cand_final[i].atms[k].txt[80:]
                            else:
                               before = len(cand_final[i].atms[k].txt) - 1
                               for ii in range(80 - len(cand_final[i].atms[k].txt)):
                                  Xseg += ' '
                               tail = '\n'
                            if cand_final[i].rName() == 'HIS' or res_label in list_SC:
                               Xseg += ' '
                            else:
                               Xseg += 'X'
                            cand_final[i].atms[k].txt = "%s%s%s" % (cand_final[i].atms[k].txt[0:before],Xseg,tail)

        cand_final.out(outputdir + 'refe_' + Name + '.pdb') 
        candid_file1.write('refe_' + Name + '.pdb\n')
        candid_file2.write('refe_' + Name + '_model.pdb\n')
        candid_file3.write('refe_' + Name + '_mini\n')
        candid_file4.write(PyPPP_line)

    return


def Prep_Candidates(model_dir, GapNum, loopSeq):
    
    gap_dir = mk_dir(model_dir + "Gap" + str(GapNum))
    Loop_addr = mk_dir(gap_dir + "BCSearch")
    refe = PDB.PDB(model_dir + "modified_open_peptide.pdb")
    # print "reference PDB is: %s" % (model_dir + "modified_open_peptide.pdb")
    refe_len = len(refe)
    flank1 = PDB.PDB(gap_dir + "flank1.pdb")
    flank_len = len(flank1)

    top_model_file_name = Loop_addr + "top_models.list"
    # print top_model_file_name
    if os.path.isfile(top_model_file_name):
       top_model_file = open(top_model_file_name,"r").readlines()
       if len(top_model_file) >0 :
          sys.stderr.write("The file containing top models already exists!\n")
          return

    BC_final_result = Loop_addr + "BCLoopCluster_Final_BC.txt"  #"PyPPP_accepted.txt"
    if(os.path.exists(BC_final_result) and len(open(BC_final_result,"r").read())>0) :
        outputdir = mk_dir(Loop_addr + "Candidates")
        print("output dir: %s" % (Loop_addr + "Candidates"))
        Join_refe_candid(refe,flank_len,BC_final_result,outputdir,Loop_addr,loopSeq,GapNum)
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    
    parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Loop size",
                        action="store", default=None) 
    
    parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None) 
        
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")


    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    #print(print_options(options))    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr = options.work_path
    protName   = options.target
    LoopSize   = options.linker_sze
    loopSeq    = options.linker_seq


    model_dir = targetAddr + PROT_DIR + "/"
    Prep_Candidates(model_dir, LoopSize, loopSeq)

