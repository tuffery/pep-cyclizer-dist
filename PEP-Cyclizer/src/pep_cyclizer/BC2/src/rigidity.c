#include "common.h"
#include "util.h"

Real rigidity(const Coord X[], const Coord Y[], int len) {
  Coord Xm, Ym;
  Real sum1, sum2, sum;
  int i;

  getCenter(X,len, &Xm);
  getCenter(Y,len, &Ym);
  sum=0;
  for (i=0;i<len;i++) {
    sum1=dist2(Xm, X[i]);
    sum2=dist2(Ym, Y[i]);
    sum=max(sum,fabs((sum1-sum2)/(sum1+sum2)));
  }
  sum1=dist2(X[0], X[len-1]);
  sum2=dist2(Y[0], Y[len-1]);
  // sum=max(sum,fabs((sum1-sum2)/(sum1+sum2)));
  sum=max(sum,fabs((sum1-sum2)));
  return sum;
}
