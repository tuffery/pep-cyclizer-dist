#include "rmsd.h"
#include <stdlib.h>

/*
 * RMSD stuff
 */
void random_vect(int n, Real *v, unsigned short *randstate) {
  int i;
  for (i=0; i<n; i++)
    v[i]= (Real)erand48(randstate)/(RAND_MAX+1.0);
}

Real inner(Real *x, Real *y, int n) {
  int i;
  Real sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;

}

void product(Real *result, Real *A, Real *x, int n) {
  int i, j;
  Real sum;

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i+n*j]*x[j];
    result[i]=sum;
  }
}


int power(Real *a, int n, int maxiter, Real eps, Real *v,
  unsigned short *randstate) {
  int niter,i;
  Real y[4];
  Real sum, l, normy, d;
  Real w[4]={0,0,0,0};

  random_vect(n, y, randstate);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    product(y, a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } //while (d>eps*fabs(l) && niter<maxiter);
  while (d>eps*fabs(l) && niter<10000);
  *v=l;
 return niter;
}

Real best_shift(Real *a, int n) {
  Real m, M, s;
  Real t, sum;
  int i, j;
  t=a[0];
  for (i=1; i<n; i++) t=max(t, a[i+n*i]);
  M=t;
  t=a[0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i+n*j]);
    t=min(t, a[i+n*i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++)
    a[i+n*i]=a[i+n*i]+s;
  return s;
}


int shift_power(Real *a, int n, int maxiter, Real eps, Real *v,
  unsigned short *randstate) {
  Real sh;
  int niter;
  sh=best_shift(a, n);

  niter=power(a, n, maxiter, eps, v, randstate);
  *v=*v-sh;
  return niter;
}

/*
 * calculs des coefficients d'Euler
 * X et Y must be centered first
 */
void euler(Real *X,  Real *Y, int n, Real *v) {
  Real K[3][3], A[16];
  Real sum;
  int i,j,k;

  unsigned short randstate[3]={0,0,0}; //for reproducible results
  for (i=0; i<3;i++)
    for (j=0; j<3; j++) {
      sum=0;
      for (k=0; k<n;k++)
	     sum+=Y[i+3*k]*X[j+3*k];
      K[i][j]=sum;
    }

  A[0+4*0]=K[0][0]+K[1][1]+K[2][2];
  A[1+4*1]=K[0][0]-K[1][1]-K[2][2];
  A[2+4*2]=-K[0][0]+K[1][1]-K[2][2];
  A[3+4*3]=-K[0][0]-K[1][1]+K[2][2];
  A[0+4*1]=A[1+4*0]=K[1][2]-K[2][1];
  A[0+4*2]=A[2+4*0]=K[2][0]-K[0][2];
  A[0+4*3]=A[3+4*0]=K[0][1]-K[1][0];
  A[1+4*2]=A[2+4*1]=K[0][1]+K[1][0];
  A[1+4*3]=A[3+4*1]=K[0][2]+K[2][0];
  A[2+4*3]=A[3+4*2]=K[1][2]+K[2][1];
  shift_power(A, 4, 10000, EPS, v, randstate);
}
/*
 * rmsd  entre X:3xN et Y:3xN stockés dans des vecteurs
 * par colonnes mises bout à bout.
 * effet de bord: centrage de X et Y
 */
void rmsd(const Coord X[],  const Coord Y[], int from1, int from2, int len, Real *r) {
  Real XX[3*len],YY[3*len];
  Coord Xm, Ym;
  Real x, y, sum, v;
  int i,j;

  getCenter(X+from1,len,&Xm);
  getCenter(Y+from2,len,&Ym);

  sum=0;
  for (i=0; i<3; i++)
    for (j=from1; j<from1+len; j++) {
      x=X[j][i]-Xm[i];
      XX[3*(j-from1)+i]=x;
      sum+=x*x;
    }
  for (i=0; i<3; i++)
    for (j=from2; j<from2+len; j++) {
      y=Y[j][i]-Ym[i];
      YY[3*(j-from2)+i]=y;
      sum+=y*y;
    }
   euler(XX, YY, len, &v);
   *r=sqrt(fabs(sum-2*v)/len);
 }

/*
 * maximize scalar product (X,Y)
 * suppose X and Y centered.
 * returns : (X,Y) when X and Y superposed
 * but X, Y unchanged
 */
void maxsp (const Coord X[],  const Coord Y[], int from1, int from2, int len, Real *r, int sign) {
  Real XX[3*len],YY[3*len];
  Real v;
  int i,j;

  if (sign>=0) sign=1; else sign=-1;

  for (i=0; i<3; i++)
    for (j=from1; j<from1+len; j++) {
      XX[3*(j-from1)+i]=X[j][i];
    }
  for (i=0; i<3; i++)
    for (j=from2; j<from2+len; j++) {
       YY[3*(j-from2)+i]=sign*Y[j][i];
    }

  euler(XX, YY, len, &v);
  *r=v;
}
