#!/usr/bin/env python3

"""
author: P. Tuffery
June 2019

This module to manage amino acid contraints per positions

"""

###########################
###  L I B R A R I E S  ###
###########################

import sys, os

protein_alphabet = ('ACDEFGHIKLMNOPQRSTUVWYBJZX*-adefghiklmnopqrstuvwybjzx')

def raw_constraints(aa_constraint_f):
    
    f = open(aa_constraint_f, 'r')
    lines = f.readlines()
    f.close()
    rs = {}

    for l in lines:
        l.replace(" ","")
        it  = l.rstrip().split(":")
        c_pos = int(it[0]) - 1 # from 0, file is from 1
        c_aas = "".join( it[1:])
        rs[c_pos] = c_aas

    return rs
    
def constraints(aa_constraint_f):
    
    f = open(aa_constraint_f, 'r')
    lines = f.readlines()
    f.close()
    rs = {}

    for l in lines:
        l.replace(" ","")
        it  = l.rstrip().split(":")
        c_pos = int(it[0]) - 1 # from 0, file is from 1
        c_aas = it[1:]
        rs[str(c_pos+1)] = c_aas

    return rs
    
def summed_aa_constraints(true_seq, aa_constraint_f):
    """
    Initialize amino acid contraints per position, as contraint for logo generation. All positions are summed. 

    true_seq: case where experimental linker sequence is known. 
    aa_constraint_f: file specifying constraints per positions.

    return: a string of contrainted amino acids and one for the others (from the protein_alphabet, i.e. DISTINGUISHING BETWEEN UPPER AND LOWER CASE!
    """
    not_cons = ''
    aa_cons  = ''

    if true_seq is not None:
        aa_cons = true_seq
    else:
        # Loop/linker sequence unkown. We look for constraints. Note the weblogolib does not manage different constraints per position, so we concatenate all of it ...
        if aa_constraint_f is not None and os.path.exists(aa_constraint_f):
            f = open(aa_constraint_f)
            lines = f.readlines()
            f.close()
            for l in lines:
                l = l.replace("\n","")
                it = l.split(":")
                if len(it) <= 1 : continue
                linker_pos = int(it[0])
                # if linker_pos <= 0 or linker_pos >10 : continue # Hmm. Should not be there.
                aa_cons += "".join(it[1:])
    aa_cons = "".join(set(aa_cons))
    
    for c in protein_alphabet:
        if c not in aa_cons:
            not_cons += c
    
    return aa_cons, not_cons

