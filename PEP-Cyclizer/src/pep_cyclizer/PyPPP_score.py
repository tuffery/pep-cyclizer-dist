#!/usr/bin/env python3

"""
author: Yasaman Karami
12 December 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import sys
import os
from PyPDB import PyPDB as PDB
from PyPDB.Geo3DUtils import *
import numpy as np
from optparse import OptionParser
from Bio import SeqIO
import multiprocessing as mp
import argparse
#from Config import *  # Global configuration HERE


DFLT_NPROC         = 1
DFLT_WORK_PATH     = "./"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_FLANK_SZE     = 4
DFLT_LINE_NB       = 0
pseudo_count       = 0.000001
DFLT_MODE          = "machine" 
PROT_DIR           = "pep_cyclizer_test"
PROT_DIR           = "pep_cyclizer_candidate_search"
#DFLT_BANK          = "/scratch/banks/bc_banks"
#DFLT_PROF_PATH     = DFLT_BANK + "/PyPPP_profiles/"
MAX_JOB            = 300
######################################
#### Jenson Shanon ###################
def ShenEnt(vec):
    s1 = 0
    for i in range(0,27):
      log2val = np.log2(vec[i])
      s1 += (vec[i] * log2val)
    return (s1 * -1)

def JenShen(p1,p2):
    term1 = 0; term2 = 0; term3 = 0
    term1vec = np.zeros(27)

    indd = np.argwhere(p1==0)
    if len(indd) > 0: p1[indd] = pseudo_count
    indd = np.argwhere(p2==0)
    if len(indd) > 0: p2[indd] = pseudo_count

    for j in range(0,27):
        term1vec[j] = (p1[j] + p2[j])/2
    term1 = ShenEnt(term1vec)

    H_exp = ShenEnt(p1)
    term2 = H_exp
    H_pred = ShenEnt(p2)
    term3 = H_pred

    score = term1 - (0.5 * term2) - (0.5 * term3)
    return score

def JenShenAll(p1,p2,lenSeq):

    JSD = 0; JCD_max = 0
    for i in range(0,lenSeq):
      val = JenShen(p1[i,],p2[i,])
      JSD += val
      if val > JCD_max:
        JCD_max = val
    JSD = JSD / lenSeq
    return JSD, JCD_max

def read_file(file_to_read, size2):

    if not os.path.exists(file_to_read):
        hit_prf = np.zeros((1,size2))
        return hit_prf
    file_lines = open(file_to_read,'r').readlines()
    lenFile = len(file_lines)
    hit_prf = np.zeros((lenFile,size2))
    for i in range(0,lenFile):
        linee = file_lines[i].split("\n")[0]
        hit_file_line = linee.split(" ")
        for j in range(0,size2):
            hit_prf[i,j] = float(hit_file_line[j])
    return hit_prf

def read_seq(sub_hit_prf,candid_name,candid_prf_start,looplength):
    candid_prf = read_file(candid_name, 27)
    if (candid_prf_start+flank_size) > len(candid_prf) : #when PyPPP is not applied
        return "C_err", "C_err"
    sub_candid_prf = candid_prf[candid_prf_start : (candid_prf_start+looplength),]
    score = JenShenAll(sub_hit_prf,sub_candid_prf,looplength)
    return score

def read_seq_flank(sub_hit_prf,candid_name,candid_prf_start,flank_size, flank_len, LoopSize):
    candid_prf = read_file(candid_name, 27)
    if (candid_prf_start+flank_size) > len(candid_prf) : #when PyPPP is not applied
        return "C_err", "C_err"
    sub_candid_prf = np.zeros(((flank_len*2),27))
    for j in range(0,flank_len):
        sub_candid_prf[j] = candid_prf[candid_prf_start+j,]
    for j in range(0,flank_len):
        sub_candid_prf[flank_len+j] = candid_prf[candid_prf_start+flank_size+LoopSize+j,]
    looplength = flank_len * 2
    score = JenShenAll(sub_hit_prf,sub_candid_prf,looplength)
    return score

def JSD(CandidFile, LoopSize, Waddr, PyPPP_addr, sub_hit_prf, flank_size, loop_seq):

    candid = CandidFile.split()
    pdb_code = candid[0]
    pdb_chain = candid[1]
    pdb_model = int(candid[2])
    pdb_start = int(candid[3])
    pdb_end = int(candid[4])
    CandidSeq = candid[8]
    CandidFlankLoopSeq = candid[9]

    name = pdb_code + "-" + pdb_chain
    candid_name = PyPPP_addr + name + ".svmi8.27.prob"
    fst_name = PyPPP_addr + name + ".fst"
    if not os.path.isfile(fst_name): 
        sys.stderr.write("There is no profile for " + name + "\n")
        return
    candid_seq_file = list(SeqIO.parse(fst_name, "fasta"))
    candid_seq = candid_seq_file[0].seq
    candid_prf_start = candid_seq.find(CandidFlankLoopSeq)
    if candid_prf_start == -1: 
        sys.stderr.write("There is a break in the selected loop candidate! Failing for %s in %s\n" % (CandidFlankLoopSeq, name))
        return
    
    if loop_seq is None:
        flank_len = flank_size - 3
        PyPPP_score1, PyPPP_score2 = read_seq_flank(sub_hit_prf,candid_name,candid_prf_start,flank_size, flank_len, LoopSize)

    else:
        if LoopSize < 4: looplength = 1
        else: looplength = LoopSize - 3
        candid_prf_start = candid_prf_start + flank_size
        PyPPP_score1, PyPPP_score2 = read_seq(sub_hit_prf,candid_name,candid_prf_start, looplength)

    hit = CandidFile.split("\n")[0] + " " + str(PyPPP_score1) + " " + str(PyPPP_score2)
    ### saving the results
    if PyPPP_score1 != "C_err" and PyPPP_score1 <= 0.4:
       ScoreFile = open(Waddr + "PyPPP_score_" + pdb_code + "-" + pdb_chain + "-" + str(pdb_model) + "-" + str(pdb_start) + "-" + str(pdb_end) + ".txt", "w")
       ScoreFile.write(hit + "\n")

    return
    
def JSD_thread(candidate_lines,LoopSize,Loop_dir,PyPPP_dir,sub_target_prof,flank_size,nbThreads,loop_seq):

    nbHit = len(candidate_lines)
    #pool = mp.Pool()
    for i in range(nbHit):
        data = candidate_lines[i].split("\n")[0]
        JSD(candidate_lines[i], LoopSize,Loop_dir,PyPPP_dir,sub_target_prof,flank_size,loop_seq)
        #pool.apply_async(JSD, args=(candidate_lines[i], LoopSize,Loop_dir,PyPPP_dir,sub_target_prof,flank_size,loop_seq,))
    #pool.close()
    #pool.join()
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in list(options.__dict__.keys()):
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


if __name__=="__main__":


    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ppp_path", dest="ppp_path", help="path to profiles",
                        action="store", default=None)

    parser.add_argument("--target", dest="target", help="gapped PDB to model the missing region",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target",
                        action="store", default=None)
    
    parser.add_argument("--linker_sze", dest="linker_sze", type = int, help="Loop size",
                        action="store", default=None)  
    
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)
    
    parser.add_argument("--linker_seq", dest="linker_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None)
    
    parser.add_argument("--run_docker", dest="run_docker", help="in docker mode (\"True\") or not (\"False\")",
                        action="store_true", default=False)

    parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
                        action="store", default=DFLT_NPROC) 

    
    options = parser.parse_args() # Do not pass sys.argv
    # print options.__dict__.keys()
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    #print(print_options(options))

    #    sys.exit(0)

    # input
    PyPPP_dir  = options.ppp_path
    targetAddr = options.work_path
    LoopSize   = options.linker_sze
    LoopSeq    = options.linker_seq
    target     = options.target
    flank_size = options.flank_sze
    mode       = options.run_docker
    nbThreads  = options.nproc

    model_dir = targetAddr + PROT_DIR + "/"
    gap_dir = model_dir + "Gap" + str(LoopSize) + "/"
    Loop_dir = gap_dir + "BCSearch/"

    flank1 = PDB.PDB(gap_dir + "flank1.pdb")
    flank2 = PDB.PDB(gap_dir + "flank2.pdb")
    flank1_seq = flank1.aaseq()
    flank2_seq = flank2.aaseq()

    ### read target pyppp profile
    PPP_name = "target_seq"
    fst_name = model_dir + PPP_name + ".fst"
    if not os.path.isfile(fst_name):
       sys.stderr.write("Conformation profile does not exist!\n")
    target_prof = model_dir + PPP_name + ".svmi8.27.prob"
    hit_prf = read_file(target_prof, 27)
    if len(hit_prf) < 1:
        sys.stderr.write("cannot read the target PyPPP profile!\n")
    ### extract the target sub profile
    if LoopSeq is None:
        looplength = flank_size - 3
        if looplength <= 0:
            sys.stderr.write("Please consider larger flank size (default=4)!\n")
        sub_target_prof = np.zeros(((looplength*2),27))
        for j in range(0,looplength):
            sub_target_prof[j] = hit_prf[(len(hit_prf)-(looplength-j)),]
        for j in range(0,looplength):
            sub_target_prof[looplength+j] = hit_prf[j,]
    else:
        LoopFlankSeq = flank1_seq + LoopSeq + flank2_seq
        target_seq_file = list(SeqIO.parse(fst_name, "fasta"))
        target_seq = target_seq_file[0].seq
        refe_prf_start = target_seq.find(LoopFlankSeq)
        refe_prf_start += flank_size
        if LoopSize < 4:
            looplength = 1
        else:
            looplength = LoopSize - 3
        sub_target_prof = hit_prf[refe_prf_start : (refe_prf_start + looplength),] 
    ### read candidates
    top_model_file_name = Loop_dir + "top_models.list" 
    if os.path.isfile(top_model_file_name):
       top_model_file = open(top_model_file_name,"r").readlines()
       if len(top_model_file) >0 :
          sys.stderr.write("The file containing top models already exists!")

    f = open(Loop_dir + "BCLoopCluster_Final_BC.txt", "r")
    candidate_lines = f.readlines()
    f.close()
    # CandidFile = open(Loop_dir + "BCLoopCluster_Final_BC.txt", "r").readlines()
    nbCandid = len(candidate_lines)
    ### measure JSD
    '''
    if mode :
        nb_line = int(os.environ['TASK_ID']) - 1
        nb_repeat = int(float(nbCandid)/MAX_JOB+0.9999999)
        for j in range(0,nb_repeat):
            ind_line = (j * MAX_JOB) + nb_line
            if ind_line >= nbCandid: continue
            CandidFileLine = candidate_lines[ind_line]
            if CandidFileLine.startswith("#"):
                sys.stderr.write("Error reading BCLoopSearch results file!\n")
            JSD(CandidFileLine,LoopSize,Loop_dir,PyPPP_dir,sub_target_prof,flank_size,LoopSeq)
    else:
    '''
    JSD_thread(candidate_lines,LoopSize,Loop_dir,PyPPP_dir,sub_target_prof,flank_size,nbThreads,LoopSeq)
    
