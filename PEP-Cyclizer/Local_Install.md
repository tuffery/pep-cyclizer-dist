# This script is for illustrative purpose only. 
# Wo warranty it works.

## 1. INSTALL PYPDB
#### Either with a conda environment 
```
conda create -n PyPDB -c conda-forge python=3.8 numpy scipy biopython weblogo cffi
source ~/anaconda3/etc/profile.d/conda.sh
conda activate PyPDB
```

#### or direct installation
```
path="/home/User/Documents/PEP-Cyclizer"
cd $path/src/PyPDB3
python3 setup.py install
```


## 2.INSTALL Fasta3
```
cd $path/src/Fasta3 
python3 setup.py install
```


## 3.INSTALL gromacs_py
```
cd $path/gromacs_py
pip install .
pip install -r requirements_dev.txt
```


## 4.INSTALL PyPPP
```
cd $path/src/pyppp3-light/thirdparty/
tar xzf blast-2.2.26-x64-linux.tar.gz
cd /$path/src/pyppp3-light
python3 setup.py build
python3 setup.py install
export PATH=${PATH}:$path/src/pyppp3-light/thirdparty/blast-2.2.26/bin
```
#### UNNECESSARY IN PRINCIPLE, BUT POSSIBLY IMPORTANT TO MAKE blastpgp work on your machine if you need to change the path to the banks (default is /scratch/banks):
By default, the volume with the banks is mounted by the image as /scratch/banks
If, for some reason you need to change this, you need to adapt the uniref90.pal for the uniref bank.
edit the blast_db/2012-10-31/uniref90.pal and modify the paths to access the bank taking into account ROOT_DIR/banks
```
cd blast_db/2012-10-31/
```
Do something like:
```
cat uniref90.pal.ini | sed 's/ROOT_DIR/THE\/ROOT\/DIR\/BANK\/PATH/g' # The \/ to specify / to sed.
```

e.g. if ROOT_DIR is /tmp/PEP_Cylizer and the banks are in /tmp/PEP_Cylizer/banks:
```
cat uniref90.pal.ini | sed 's/BLAST_ROOT_DIR/\/tmp\/pep-cyclizer-dist\/banks/g' > uniref90.pal.tmp
cat uniref90.pal.ini | sed 's/BLAST_ROOT_DIR/\/scratch\/banks/g' > uniref90.pal.tmp
```
check everything is correct:
```
cat uniref90.pal.tmp
```
 Look for the line starting with: TITLE and take the path after it (the * at the end of line is important):
```
ls -l /tmp/PEP-Cyclizer/banks/lib/blast_db/2012-10-31/uniref90.fasta*
```
Any message like "No such file or directory": the substitution is not correct. Please fix it.
Once OK:
```
mv uniref90.pal.tmp uniref90.pal
```

