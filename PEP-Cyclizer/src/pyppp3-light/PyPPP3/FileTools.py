#!/usr/bin/env python3

# -- IMPORTS --

# Python standards modules
import sys
import os
import os.path
import shutil
import tarfile


# -- IO MANAGEMENT --
def openFile( FN, mode = "w", verbose = 0 ):
    """
    Clean open a file. Returns a file handle (stream).

    @param FN     : input file name
    @param mode   : openning mode r,w,a ...
    @param verbose: verbose mode
    """
    try:
        iF = open( FN, mode )
    except:
        sys.stderr.write("Error: impossible to open %s file in '%s' mode !" % ( FN, mode ))
        sys.exit(0)
        
    return iF


def closeFile( iF, verbose = 0 ):
    """
    Clean closing of a file.
    
    @param iF     : file handle to close
    @param verbose: verbose mode
    """
    
    try:
        iF.close()
    except:
        sys.stderr.write("Error: Could not close file handle : %s\n" %  iF)
        sys.exit(0)

    return


def fileGetLines( FN, ret = 0, verbose = 0 ):
    """
    Open txt file, read and returns lines.

    @param FN     : input file name
    @param ret    : do we need to ignore carriage return
    @param verbose: verbose mode
    """

    if verbose:
        sys.stderr.write("Will read %s file lines ... \n"%FN)

    lines = []
    
    F = openFile( FN, mode = "r", verbose = verbose )
        
    try:
        lines = F.readlines()
    except:
        sys.stderr.write("Error: Could not parse file %s !\n"%FN)
        sys.exit(0)

    oL = []
    for aL in lines:
        if aL.strip() == "\n":
            continue
        if ret == 0:
            oL.append(aL.replace("\n",""))
        else:
            oL.append(aL)

    closeFile( F, verbose = verbose )

    return oL

def writeLines( FN, lines, verbose = 0 ):
    """
    Write input lines in files FN.
    """
    if verbose:
        sys.stderr.write("Will write to %s file." % FN)
        
    oF = openFile( FN )
    for line in lines:
        oF.write( line )
    closeFile( oF )
    
    return

def writeList( handle, lines, ret = 0, verbose = 0 ):
    """
    Write a string (list) to an open file handle

    @param handle : the file handle
    @param lines  : line(s) (list) to write
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """

    if isinstance( lines, str ):
        if ret:
            aStr = '%s\n' % lines
        else:
            aStr = '%s' % lines
        try:
            handle.write( aStr )
        except:
            sys.stderr.write("Error: impossible to write %s line to file. Aborted.\n" % lines)
            sys.exit(0)
    elif isinstance( lines, list ):
        for line in lines:
            if ret:
                aStr = '%s\n' % line
            else:
                aStr = '%s' % line
            try:
                handle.write( aStr )
            except:
                sys.stderr.write("Error: impossible to write %s line to file. Aborted.\n" % line)
                sys.exit(0)
    try:
        handle.flush()
    except:
        pass
    return
    
def filePutLines( FN, lines, ret = 0, verbose = 0 ):
    """
    Flat lines into a file.
    
    @param FN     : output file name
    @param lines  : lines to write into FN
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """
    
    if verbose:
        sys.stderr.write("Will write lines to %s file ... \n" % FN)

    F = openFile( FN, mode = "w", verbose = verbose )
    writeList( F, lines, ret = ret, verbose = verbose )
    closeFile( F, verbose = verbose )

    return 

def checkFileStatus( FN, verbose = 0 ):
    """
    Check if the input file exists and is not empty.

    @param FN     : input file name
    @param verbose: verbose mode
    
    @return: file status (0 if ok)    
    """

    if not os.path.exists( FN ):
        return 1

    statinfo = os.stat( FN )
    
    if statinfo.st_size == 0:
        return 2

    return 0


def tar( archive, fileNames, dereference = False, verbose = 0 ):
    """
    Make a tarball archive with given input files.
    
    @param archive    : tarball file name
    @param filenames  : files names list to archive
    @param dereference: follow symbolic links 
    @param verbose    : verbose mode
    """

    if verbose:
        sys.stderr.write("Will archive following files to %s (dereference:%s)\n" % (archive, str(dereference)))
        for aFile in fileNames:
            sys.stderr.write("%s\n" % aFile)

    tar = tarfile.open( name = archive, mode = "w:gz" )
    tar.dereference = dereference
    for name in fileNames:
        tar.add( name )
    tar.close()
    
    if verbose:
        sys.stderr.write("Archiving done.\n")

    return

