#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Maupetit J, 2008
# Inspired from doPsiPredMTX.sh
# Tuffery P, 2010
# Adapted for PPP and 2 layer predictor.

# ** Dependancies **
# 
#  - blastpgp (NCBI-BLAST)
#  - makemat (NCBI-BLAST)
#  - get_clean_matrix (EBGM)
#  - doSeriesE1071.8.R (EBGM)
#  - HMMPred (EBGM)
#  - ProbTools (P. Tuffery)

# -- HISTORY --
# -- 2009, sep 17
# Bugfix: PSIPRED constrains are now efficient. In -fbn X mode, if
# psiprf has less than X letters, HMMPred had some more letters until
# X # is respected. We now post-treat the prediction (phmm27-2) to
#!/usr/bin/env python

# garantee that psiprf is respected.
# 

# ------------------------- Modules To Import
import sys
import os
import os.path
import shutil
import subprocess
from optparse import OptionParser

# Configuration import
from PyPPP3.PyPPP import *
# from PyPPP3.Fasta import *
from PyPPP3.ModelHandler import *

# GO !
if __name__ == "__main__":
    job = PPP(args = sys.argv, standalone = True, client = False, server = False)
    job.start()
    if not job.cmd_line_status:
        exit(1)
    # job.parse_specific()
    if job.options.verbose:
        sys.stderr.write("%s\n" % job)

    x = Fasta.fasta(job.options.seqFile)
    try:
        aaSeq = x[x.ids()[0]].s()
    except:
        sys.stderr.write("Could not identify sequence. \nIs filename specified with the -s option valid? (see PyPPP3Exec -h for options) \nAborting.\n")
        sys.exit(0)

    # load models
    mh = None
    if job.options.doProb:
        if job.options.verbose:
            sys.stderr.write("Loading models...\n")
        if job.options.model2012:
            mapping = {}
            for letter in 'ACDEFGHIKLMNPQRSTVWY':
                mapping[letter] = 'X'
            mh = ModelHandler(DFLT_SVM_2012_PATH, 'X', mapping, job.options.verbose)
        elif job.options.model2006:
            mapping = {}
            for letter in 'ACDEFGHIKLMNPQRSTVWY':
                mapping[letter] = 'X'
            mh = ModelHandler(DFLT_SVM_2006_PATH, 'X', mapping, job.options.verbose)
        else:
            modelPath = job.options.SVMlibPath
            modelPath = os.path.join(modelPath, job.options.SVMmodel)
            if job.options.split_models:
                if job.options.verbose:
                    sys.stderr.write("Loading models: path is %s" % modelPath)
                mh = ModelHandler(modelPath, verbose = job.options.verbose)
            else:
                mapping = {}
                for letter in 'ACDEFGHIKLMNPQRSTVWY':
                    mapping[letter] = 'X'
                mh = ModelHandler(modelPath, 'X', mapping, job.options.verbose)
    
    # do the prediction
    basename = job.pppp(aaSeq, mh, verbose = job.options.verbose)
    output = "%s.svmi8.27.prob" % job.options.label
    try:
        shutil.move("%s.log" % basename, "%s.log" % job.options.label)
    except IOError:
        pass
    try:
        shutil.move("%s.pssm" % basename, "%s.pssm" % job.options.label)
    except IOError: # file may not exist
        pass
    try:
        shutil.move("%s.pssm.scaled" % basename, "%s.pssm.scaled" % job.options.label)
    except IOError: # file may not exist
        pass
    try:
        shutil.move("%s.chk" % basename, "%s.chk" % job.options.label)
    except IOError: # file may not exist
        pass
    # shutil.copy then os.remove instead of  shutil.move
    shutil.copy("%s.svmi8" % basename, "%s.svmi8" % job.options.label)
    os.remove("%s.svmi8" % basename)
    if os.path.isfile("%s.fst" % basename):
        os.unlink("%s.fst" % basename)

    if  job.options.doProb:
        shutil.move("%s.svmi8.27.prob" % basename, output)
        if job.options.doPlot:
            if job.options.verbose:
                sys.stderr.write("Generating profile image\n")
            cmd = "%s/bin/plot %s %s %s" % (PPP_ROOT, output, aaSeq, job.options.label)
            if job.options.verbose:
                sys.stderr.write("%s\n" % cmd)
            os.system(cmd)
    if job.options.verbose:
        sys.stderr.write("Done\n")
