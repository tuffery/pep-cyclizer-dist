#!/usr/bin/env python3

# -*- coding: utf-8 -*-

# Maupetit J, 2008
# Inspired from doPsiPredMTX.sh
# Tuffery P, 2010
# Adapted for PPP and 2 layer predictor.

# ** Dependancies **
# 
#  - blastpgp (NCBI-BLAST)
#  - makemat (NCBI-BLAST)
#  - get_clean_matrix (EBGM)
#  - doSeriesE1071.8.R (EBGM)
#  - HMMPred (EBGM)
#  - ProbTools (P. Tuffery)

# -- HISTORY --
# -- 2009, sep 17
# Bugfix: PSIPRED constrains are now efficient. In -fbn X mode, if
# psiprf has less than X letters, HMMPred had some more letters until
# X # is respected. We now post-treat the prediction (phmm27-2) to
# garantee that psiprf is respected.
#
# 2014-2015: updates by A. Lamiable & P. Tuffery
# 

# ------------------------- Modules To Import
import sys
import os
import os.path
import subprocess
import time
import re
import math
from optparse import OptionParser
import tempfile
import multiprocessing as mp

# Configuration import
from .Config import *

from .svm import *
#from plot import plot_profile

# local modules
from .FileTools import *

try:
    import PyPDB.PyPDB as PDB
    hasPyPPP = True
except:
    hasPyPPP = False

try:
    import PyBlast.PyBlast as PyBlast
    hasPyBlast = True
except:
    hasPyBlast = False

import Fasta.Fasta as Fasta

from  .PyOptions import *



class PPP(Options):
    """
    A class to predict protein structural profile from amino acid sequence.
    Take a sequence input, apply predictor and return SA profile (27 states).
    """

    def __init__(self, args = ["PPP"], options = None, version=None, standalone=True, client = False, server = False, verbose = False):
        """
        @param options : a dictionary of the options among which 
                         some are valid.
        """
        if version == None:
            version = "PyPPP"+ " v%s"%VERSION
        if not hasattr(self,"clp"):
            Options.__init__(self, args = args, options = options, version=version, verbose = verbose)
        self.__cmdLine_options(standalone=standalone, client=client, server = server)

    def start(self, server = False, verbose = False):
        """
        Effective options parsing
        on return, PPP ready to run.
        """
        self.parse()
        if verbose:
            self.options.verbose = verbose
        self.options.aaSeq = None
        self.options.s2Seq = None
        self.options.server = server

        self.cmd_line_status = self.__check_options(verbose = True)
 
    def __repr__(self):
        try:
            rs = "PPP"
            for option in list(self.options.__dict__.keys()):
                rs = "%s\n  %10s: %s" % (rs, str(option),getattr(self.options,option))
            return rs
        except:
            return ""

    def __cmdLine_options(self, version = None, standalone=True, client = False, server = False):
        """
        Command line definition.
        These options will be propagated to CABuilder.
        @return: command line options and arguments (optionParser object)
        """

        if standalone or client:
            self.clp.add_option("-s", "--iSeq", action="store", dest="seqFile",   type="string",
                                help="amino acid sequence (fasta file)", default = None)
            
            self.clp.add_option("-l", "--label", action="store", dest="label",   type="string",
                                help="label", default = "PPP")
            
            self.clp.add_option("--noUpper", action="store_false",      dest="toUpper",    
                                help="Switch forcing input sequence to upper case to false (i.e. preserve lowercase)", default = True)

            self.clp.add_option("--noPlot", action="store_false", dest="doPlot",    
                                help="do not generate png image of profile", default = True)
            
        if client or server:
            self.clp.add_option("--socket", action="store",      dest="pppSOCKET",
                               help="User named socket (multiple executions on same machine. Same option must be passed to PyPPP3c). (%s)" % DFLT_SOCKET, default = DFLT_SOCKET)

        if standalone or server:
            self.clp.add_option("--blastDbPath", action="store",      dest="dbPath",    
                                help="path to blast banks",                                                default = DFLT_BLAST_DB_PATH)

            self.clp.add_option("--blastDb", action="store",      dest="database",    
                                help="blast bank",                                                         default = DFLT_BLAST_BANK)

            self.clp.add_option("--SVMmodel", action="store",      dest="SVMmodel",    
                                help="Model to perform SA SVM prediction (not in use) (default %s)" % (DFLT_SVM_MODEL),                            default = DFLT_SVM_MODEL)
        
            self.clp.add_option("--SVMlibPath", action="store",      dest="SVMlibPath",    
                                help="Path to model to perform SA SVM prediction (default %s)" % (DFLT_SVM_LIB_ROOT_PATH),                            default = DFLT_SVM_LIB_ROOT_PATH)
        
            self.clp.add_option("-n", "--n_cores", action="store", dest="n_cores", type="int", help="Number of cores to be used by psiblast", default=DFLT_NCORES)

            self.clp.add_option("-e", "--evalue", action="store", dest="evalue", type="float", help="evalue for psiblast", default=DFLT_EVALUE)

            # if hasPyBlast:
            self.clp.add_option("--2012", action="store_true", dest="model2012",
                                help="Use the 2012 model and blastpgp", default = True) # Impose 2012 

            self.clp.add_option("--2006", action="store_true", dest="model2006",
                                help="Use the 2006 model and blastpgp", default = False)

            self.clp.add_option("--2016", action="store_true", dest="model2016",
                                help="Use the 2016 model and psiblast_mod", default = False)

            self.clp.add_option("--blast_version", action="store", dest="blast_version",
                                help="Blast version (blastpgp, psiblast, psiblast_alexis)", default = "psiblast")

            self.clp.add_option("--split_models", action="store_true", dest="split_models",
                                help="Are we using 20 models (True) or 1 model (False) ?", default = False)
            
            self.clp.add_option("--psiblast_word_size", action="store", dest="word_size",
                                help="Psiblast -word_size parameter", default=2, type="int")
            
        if client:
            self.clp.add_option("-q", "--quit", action="store_true",      dest="quit",    
                                help="asks the daemon to shut down", default = False)

        if standalone or server:
            self.clp.add_option("--noProb", action="store_false",      dest="doProb",    
                                help="just pssm, no prediction", default = True)

    def __check_options(self, verbose = False):
        """
        check_options: will check the consistency of the arguments passed to PyGreedy

        @param verbose     : explain inconsistencies detected.
        @return True/False : if True, no inconsistency was detected.
        """
       
        if self.options.model2012:
            self.options.model         = "2012"
            self.options.blast_version = "blastpgp"
            self.options.split_models  = False
            self.options.dbPath        = DFLT_BLAST_2012_DB_PATH
            self.options.database      = DFLT_BLAST_2012_DATABASE
            self.options.SVMlibPath    = DFLT_SVM_2012_PATH
            
        if self.options.model2006:
            self.options.model         = "2006"
            self.options.blast_version = "blastpgp"
            self.options.split_models  = False
            self.options.dbPath        = DFLT_BLAST_2006_DB_PATH
            self.options.database      = DFLT_BLAST_2006_DATABASE
            self.options.SVMlibPath    = DFLT_SVM_2006_PATH
            
        if self.options.model2016:
            self.options.model         = "2016"
            self.options.blast_version = "psiblast_mod"
            self.options.split_models  = True
            self.options.dbPath        = DFLT_BLAST_2016_DB_PATH
            self.options.database      = DFLT_BLAST_2016_DATABASE
            self.options.SVMlibPath    = DFLT_SVM_2016_PATH

        if self.options.server:
            return True
        
        try:
            x = Fasta.fasta(self.options.seqFile)
            aaSeq = x[x.ids()[0]].s()
        except:
            sys.stderr.write("PyPPP: Sorry: Could not read sequence (%s)\n" % self.options.seqFile)
            return False

        if not x[x.ids()[0]].check_standard():
            sys.stderr.write("PyPPP: Sorry: Only standard amino acids are presently accepted.\n")
            return False
                
        self.options.s2Seq = None

        return True

    def runOldBlast(self, basename, dbpath = DFLT_BLAST_2012_DB_PATH, db = DFLT_BLAST_2012_DATABASE, verbose=False):
        
        # db_path = os.path.join(self.options.dbPath, self.options.database)
        db_path = os.path.join(dbpath, db)
        blast_cmd = ("export BLASTMAT=%s && %s/blastpgp -a %s -b 0 -j 3 -h %f -d %s -C %s.chk -W %d -M BLOSUM80 -i %s.fst -o %s.blast" %
                     (DFLT_BLASTPGP_MAT_PATH, DFLT_BLASTPGP_BIN_PATH, self.options.n_cores, 0.1, db_path, basename, self.options.word_size, basename, basename))
        if verbose:
            sys.stderr.write(blast_cmd + "\n")
        os.system(blast_cmd)
        lodcmd = "echo %s.chk > %s.pn ; echo %s.fst > %s.sn; " % (os.path.basename(basename), basename, os.path.basename(basename), basename)
        lodcmd = "%s export BLASTMAT=%s && %s/makemat -P %s -U BLOSUM62; %s/get_clean_matrix %s.mtx | tail -n +2 " % (lodcmd, DFLT_BLASTPGP_MAT_PATH, DFLT_MAKEMAT_PATH,
                                                                          basename, DFLT_GETCLEANMATRIX_PATH , basename)
        if verbose:
            sys.stderr.write(lodcmd + "\n")

        p = subprocess.Popen(lodcmd, shell=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)
        (outt,log) = p.communicate()
        out = outt.decode('utf-8')
        f = open("%s.lod" % basename, "w")
        f.write(out)
        f.close()
        self.lodds = []
        for line in out.split("\n")[:-1]:
            self.lodds.append(" ".join(line.split()[1:]))

    def run_blast(self, basename, engine = "psiblast", dbpath = DFLT_BLAST_2016_DB_PATH, db = DFLT_BLAST_2016_DATABASE, verbose = False):
        evalue = self.options.evalue
        # call psiblast, raising the evalue until we get a PSSM
        pssm_file = basename + ".pssm"
        chk_file = basename + ".chk"
        log_file = basename + ".log"
        if os.path.isfile(pssm_file):
            os.unlink(pssm_file)
        i = 0

        # db_path = os.path.join(self.options.dbPath, self.options.database)
        database = os.path.join(dbpath, db)
        # sys.stderr.write("psiblast db: %s (%s / %s) \n" % (database, dbpath, db))
        
        while (not os.path.isfile(pssm_file)) and i < 10:
            cmd = 'psiblast'
            if engine == "psiblast_mod":
                cmd = '%s/psiblast_mod' % DFLT_PSIBLASTMOD_BIN_PATH
            blast_cmd = ("%s -db %s -out_pssm %s -query %s.fst -out_ascii_pssm %s -num_iterations 3 -comp_based_stats 1 -matrix BLOSUM80 -evalue %f -word_size %d -num_threads %s -out %s" %
                         (cmd, database, chk_file,
                          basename, pssm_file, evalue, self.options.word_size,
                          self.options.n_cores, log_file))
            if verbose:
                sys.stderr.write("%s\n" % blast_cmd)

            os.system(blast_cmd)
            i += 1
            evalue *= 10

        self.lodds = []
        if engine == "psiblast_mod":
            pssm_file += '.scaled'
            if verbose:
                sys.stderr.write("pssm file: %s\n" % pssm_file)
            with open(pssm_file) as f:
                for line in f:
                    a = line.split()
                    if len(a) >= 22:
                        l = []
                        for i in [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22]:
                            l.append(a[i])
                        self.lodds.append(" ".join(l))
        else:
            with open(pssm_file) as f:
                for line in f:
                    a = line.split()
                    if len(a) >= 44 and a[0].isdigit() and re.match('^[ACDEFGHIKLMNPQRSTVWY]$', a[1]):
                        self.lodds.append(" ".join([s for s in a[2:22]]))

    def make_svmi8(self, basename, verbose =  0):
        if self.lodds == None:
            raise Exception('must call psiblast beforehands')

        dL = "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
        f = open("%s.svmi8" % basename, 'w')
        l = len(self.lodds)
        for i in range(0, l - 3):
            if i-2 < 0:
                f.write(dL)
            else:
                f.write("%s " % self.lodds[i-2])

            if i-1 < 0:
                f.write(dL)
            else:
                f.write("%s " % self.lodds[i-1])

            f.write("%s %s %s %s " % (self.lodds[i], self.lodds[i+1], self.lodds[i+2], self.lodds[i+3]))

            if i+4 >= l:
                f.write(dL)
            else:
                f.write("%s " % self.lodds[i+4])

            if i+5 >= l:
                f.write(dL)
            else:
                f.write("%s " % self.lodds[i+5])

            f.write("\n")
        f.close()

    def pppp(self, aaSeq, model_handler, verbose = True):
        """ parallel ppp """
        if self.options.debug:
            sys.stderr.write("pppp: entering\n")
        self.aaSeq = aaSeq
        self.model_handler = model_handler

        #labels = pyms['A'].get_labels()
        # n_classes = model_handler.getPym('A').get_nr_class()

        handle, filename = tempfile.mkstemp('.fst')
        fst = os.fdopen(handle, "w")
        fst.write("> PPP%s\n")
        fst.write("%s\n" % aaSeq)
        fst.close()

        basename = filename.replace(".fst", "")
        if self.options.debug:
            sys.stderr.write("pppp: running blast basename is %s\n" % basename)
        if self.options.model == "2006":
            self.runOldBlast(basename, dbpath = self.options.dbPath, db = self.options.database, verbose=verbose)
        elif self.options.model == "2012":
            # sys.stderr.write("Using 2012 prediction\n")
            self.runOldBlast(basename, dbpath = self.options.dbPath, db = self.options.database, verbose=verbose)
        elif self.options.model == "2016":
            # sys.stderr.write("Using 2016 prediction\n")
            self.run_blast(basename, engine = self.options.blast_version, dbpath = self.options.dbPath, db = self.options.database, verbose=verbose)
        else:
            if self.options.blast_version == 'blastpgp':
                self.runOldBlast(basename, dbpath = self.options.dbPath, db=self.options.database, verbose=verbose)
            else:
                self.run_blast(basename, engine = self.options.blast_version, dbpath = self.options.dbPath, db=self.options.database, verbose=verbose)

            
        self.make_svmi8(basename, verbose=verbose)
        
        if verbose or self.options.debug:
            sys.stderr.write("pppp basename: %s\n" % basename)
        if not self.options.doProb:
            return basename
        
        svmi8file = "%s.svmi8" % basename
        if self.options.debug:
            sys.stderr.write("loading svmi8 file %s\n" % svmi8file)
        f = open(svmi8file, 'r')
        self.svmi8 = [[float(v) for v in x.strip().split()] for x in f]
        f.close()

        if verbose or self.options.debug:
            sys.stderr.write("Running SVM prediction\n")

        # splits the SVMI8 file in two blocks
        middle = int(len(self.svmi8)/2)
        blocks = [list(range(0, middle)), list(range(middle, len(self.svmi8)))]
        # create a communication pipe for the processes
        output = mp.Queue()
        # create two processes, one for each block of the SVMI8
        labels = model_handler.getPym('A').get_labels()
        n_classes = model_handler.getPym('A').get_nr_class()
        processes = [mp.Process(target=predict, args=(self, indices, n_classes, output)) for indices in blocks]

        # run the processes
        for p in processes:
            p.start()
        
        # merge the lines and reorder them
        results = [None for i in range(len(self.svmi8))]
        count_done = 0
        while count_done < 2:
            tuple = output.get()
            if tuple == 'DONE':
                count_done += 1
            else:
                i, row = tuple
                results[i] = row

        # wait for them to finish
        for p in processes:
            p.join()
 
        # output the results
        if verbose:
            sys.stderr.write("prob files is: %s.27.prob\n" % svmi8file)
        
        if self.options.debug:
            sys.stderr.write("pppp will write %s ... \n" % (svmi8file + '.27.prob'))
        OVSProb = open(svmi8file + '.27.prob', 'w')
        for row in results:
                for col in range(0, n_classes):
                    OVSProb.write("%f " % row[col])
                OVSProb.write("\n")
        OVSProb.close()

        # orifile  = "%s.svmi8" % basename
        # destfile = svmi8file
        # shutil.move("%s.svmi8" % basename, "path/to/new/destination/for/file.foo")
        return basename

    def plots(self, probFile = None, seqFile = None, s2File = None, label = None, alphabetSize = "27", verbose = False):
        """
        Generate plots illustrating the PPP prediction
        Create an image from the prediction profile
        @param probFiles: proba files
        @param seqFiles: fasta amino acids files
        @param s2Files: fasta s2 files
        @return image: "%s.probplot.png" % title
        """
        if not self.options.doPlot:
            return
        
        if not probFile:
            probFile = "%s.svmi8.27.prob" % label
        aaSeq = self.options.aaSeq 
        s2Seq = self.options.s2Seq
        if not label:
            label = self.options.label
        if seqFile != None:
            x = Fasta.fasta(seqFile)
            aaSeq = x[x.ids()[0]].s()
        if s2File != None:
            x = Fasta.fasta(s2File)
            s2Seq = x[x.ids()[0]].s()
        if s2Seq == None:
            s2Seq = aaSeq

        cmd = "%s/bin/plot %s %s %s" % (PPP_ROOT, probFile, aaSeq, label)
        if verbose:
            sys.stderr.write("%s\n" % cmd)
        os.system(cmd)

        return

def predict(obj, indices, n_classes, output):
    for i in indices:
        pos = i+2
        row = obj.svmi8[i]
        aa = obj.aaSeq[pos]
        model, scaling = obj.model_handler[aa]
        labels = obj.model_handler.getPym(aa).get_labels()
        for col in range(len(row)):
            #row[col] *= 100.0
            row[col] -= scaling[col][0]
            row[col] /= scaling[col][1]

        xi, max_idx = gen_svm_nodearray(row)

        probs = (c_double*n_classes)()
        libsvm.svm_predict_probability(model, xi, probs)
        array = []
        for col in range(len(probs)):
            array.append(probs[labels.index(col+1)])
        if not (obj.options.model2006 or obj.options.model2012):
            array = array[1:] + [array[0]]
        output.put( (i, array) )
        #output.put( (i, [float(x) for x in probs]) )
    output.put("DONE")
